#include "efi_helpers.hpp"
#include "print.hpp"
#include "alloc.hpp"

EFI_SYSTEM_TABLE* g_system_table = nullptr;

SysTableEntries walk_efi_table_entries() {
    SysTableEntries result{};
    for (size_t i = 0; i < g_system_table->NumberOfTableEntries; i++) {
        EFI_CONFIGURATION_TABLE& conf_table = g_system_table->ConfigurationTable[i];

        const char* type = "[[Unknown]]";
        if (conf_table.VendorGuid == EFI_ACPI_TABLE_GUID) {
            type = "ACPI 2.0";
            result.rsdp = static_cast<RSDP*>(conf_table.VendorTable);
        } else if (conf_table.VendorGuid == EFI_OLD_ACPI_TABLE_GUID) {
            type = "ACPI 1.0";
        } else if (conf_table.VendorGuid == EFI_SMBIOS_TABLE_GUID) {
            type = "SMBIOS";
        } else if (conf_table.VendorGuid == EFI_MEMORY_ATTRIBUTES_TABLE_GUID) {
            type = "MemoryAttributes";
        } else if (conf_table.VendorGuid == EFI_DEBUG_IMAGE_INFO_TABLE_GUID) {
            type = "DebugImageInfo";
        } else if (conf_table.VendorGuid == EFI_HOB_LIST_GUID) {
            type = "HobList";
        } else if (conf_table.VendorGuid == EFI_DXE_SERVICES_TABLE_GUID) {
            type = "DXEServices";
        } else if (conf_table.VendorGuid == EFI_MEMORY_TYPE_INFORMATION_GUID) {
            type = "MemoryTypeInformation";
        } else if (conf_table.VendorGuid == LZMA_COMPRESSED_GUID) {
            type = "LzmaCompress";
        } else if (conf_table.VendorGuid == MEM_STATUS_CODE_REC_GUID) {
            type = "MemoryStatusCodeRecord";
        } else if (conf_table.VendorGuid == SMBIOS3_TABLE_GUID) {
            type = "SMBIOS3";
        }

        print("GUID: {} -- {}\r\n", conf_table.VendorGuid, type);
    }
    return result;
}

// Gets the EFI MTFTP4 protocol and configures it. Panics if either fails.
EFI_MTFTP4_PROTOCOL* get_efi_mtftp4_protocol() {
    EFI_MTFTP4_PROTOCOL* protocol = nullptr;

    EFI_STATUS locate_handle_ret = g_system_table->BootServices->LocateProtocol(
        &EFI_MTFTP4_PROTOCOL_GUID,
        nullptr,
        reinterpret_cast<void**>(&protocol)
    );

    if (locate_handle_ret != EFI_SUCCESS || protocol == nullptr) {
        flush();
        panic(L"Couldn't find a handle for the MTFTP protocol");
    }
    print("Got handle to MTFTP protocol: 0x{x}\r\n", reinterpret_cast<uint64_t>(protocol));

    // Configure the MTFTP protocol
    EFI_MTFTP4_CONFIG_DATA config{};
    config.StationIp = {10, 0, 2, 15};
    config.ServerIp = {10, 0, 2, 2};
    config.SubnetMask = {255, 255, 255, 0};
    config.GatewayIp = {10, 0, 2, 2};

    EFI_STATUS mtftp_conf_ret = protocol->Configure(protocol, &config);
    print("MTFTP Configure ret: 0x{x}\r\n", mtftp_conf_ret);
    if (mtftp_conf_ret != EFI_SUCCESS) {
        flush();
        panic(L"Couldn't configure the MTFTP protocol");
    }

    return protocol;
}

// Uses a configured mtftp protocol to download a file
uint8_t* efi_mtftp4_protocol_get_file(EFI_MTFTP4_PROTOCOL* protocol, const char8_t* filename, OUT size_t* file_size) {
    // First we need to figure out how big the file is; lets try to download the file into a tiny buffer
    EFI_MTFTP4_TOKEN read_file_token{};
    read_file_token.Filename = filename;
    // We need to give the buffer a non null pointer
    char tmp_buffer[1];
    read_file_token.Buffer = tmp_buffer;
    read_file_token.BufferSize = sizeof(tmp_buffer);
    EFI_STATUS get_filesize_ret = protocol->ReadFile(protocol, &read_file_token);
    print("Getting the file size - Read File Ret: 0x{x}, Read File Status: 0x{x}, Buffer size: {}\r\n",
        get_filesize_ret,
        read_file_token.Status,
        read_file_token.BufferSize
    );
    if (get_filesize_ret != EFI_SUCCESS && get_filesize_ret != EFI_BUFFER_TOO_SMALL) {
        flush();
        panic(L"Couldn't get the file size of the kernel");
    }
    read_file_token.Status = 0;

    // Allocate a propper buffer for the file
    uint8_t* buffer = alloc_bytes(read_file_token.BufferSize);
    read_file_token.Buffer = buffer;

    // Download the file into the allocated buffer
    EFI_STATUS read_file_ret = protocol->ReadFile(protocol, &read_file_token);
    print("Downloading the file  - Read File Ret: 0x{x}, Read File Status: 0x{x}, Buffer size: {}\r\n",
        read_file_ret,
        read_file_token.Status,
        read_file_token.BufferSize
    );
    if (read_file_ret != EFI_SUCCESS || read_file_token.Status != EFI_SUCCESS) {
        flush();
        panic(L"Couldn't download the kernel");
    }

    *file_size = read_file_token.BufferSize;
    return buffer;
}
