#pragma once
#include "common.hpp"
#include "print.hpp"
#include "alloc.hpp"


inline void map_pa_to_va(const uint64_t page_table_phys, const uint64_t pa, const uint64_t va, bool read_only, bool executable) {
    if ((pa % 4096) != 0) {
        print("LATE PANIC: PA is not divisible by 4096!\n");
        while (true);
    }
    if (va % 4096 != 0) {
        print("LATE PANIC: VA is not divisible by 4096!\n");
        while (true);
    }

    constexpr const uint64_t get_addr_mask = 0x000f'ffff'ffff'f000ull;
    constexpr const uint64_t index_mask = 0x1ff;
    const uint64_t index4 = (va >> 39) & index_mask;
    const uint64_t index3 = (va >> 30) & index_mask;
    const uint64_t index2 = (va >> 21) & index_mask;
    const uint64_t index1 = (va >> 12) & index_mask;

    print("Mapping: VA 0x{x} -> PA 0x{x}\n", va, pa);

    uint64_t level4_entry = reinterpret_cast<uint64_t*>(page_table_phys)[index4];
    if ((level4_entry & 1) == 0) {
        // Set up a level 3 page table structure
        uint64_t new_page = get_phys_page();
        for (size_t i = 0; i < 512; i++) {
            reinterpret_cast<volatile uint64_t*>(new_page)[i] = 0;
        }
        uint64_t new_entry = new_page | (1 << 0) | (1 << 1);
        reinterpret_cast<volatile uint64_t*>(page_table_phys)[index4] = new_entry;
        level4_entry = new_entry;
    }

    const uint64_t level3_phys_addr = level4_entry & get_addr_mask;
    uint64_t level3_entry = reinterpret_cast<uint64_t*>(level3_phys_addr)[index3];
    if ((level3_entry & 1) == 0) {
        // Set up a level 2 page table structure
        uint64_t new_page = get_phys_page();
        for (size_t i = 0; i < 512; i++) {
            reinterpret_cast<volatile uint64_t*>(new_page)[i] = 0;
        }
        uint64_t new_entry = new_page | (1 << 0) | (1 << 1);
        reinterpret_cast<volatile uint64_t*>(level3_phys_addr)[index3] = new_entry;
        level3_entry = new_entry;
    }

    if ((level3_entry & (1 << 7)) != 0) {
        print("Early panic: Can't deal with the PageSize bit set in level3 page table -- unimplemented\n");
        while (true);
    }

    const uint64_t level2_phys_addr = level3_entry & get_addr_mask;
    uint64_t level2_entry = reinterpret_cast<uint64_t*>(level2_phys_addr)[index2];
    if ((level2_entry & 1) == 0) {
        // Set up a level 1 page table structure
        uint64_t new_page = get_phys_page();
        for (size_t i = 0; i < 512; i++) {
            reinterpret_cast<volatile uint64_t*>(new_page)[i] = 0;
        }
        uint64_t new_entry = new_page | (1 << 0) | (1 << 1);
        reinterpret_cast<volatile uint64_t*>(level2_phys_addr)[index2] = new_entry;
        level2_entry = new_entry;
    }

    if ((level2_entry & (1 << 7)) != 0) {
        print("Early panic: Can't deal with the PageSize bit set in level2 page table -- unimplemented\n");
        while (true);
    }

    const uint64_t level1_phys_addr = level2_entry & get_addr_mask;
    reinterpret_cast<volatile uint64_t*>(level1_phys_addr)[index1] =
        pa |
        (1 << 0) |                                          // Present
        (static_cast<uint64_t>(read_only ? 0 : 1) << 1) |   // Read/Write
        (0 << 2) |                                          // User/Supervisor
        (static_cast<uint64_t>(executable ? 0 : 1) << 63);  // NoExecute

};


struct Level4PageEntry {
    // 0 - Next level address is NOT valid; 1 - is valid
    uint64_t Present : 1;
    // 0 - Read Only; 1 - Read and Write
    uint64_t ReadWrite : 1;
    // 0 - Userspace cannot access this; 1 - Userspace can access this
    uint64_t UserSupervisor : 1;
    uint64_t PageLevelWritethrough : 1;
    uint64_t PageLevelCacheDisable : 1;
    uint64_t Accessed : 1;
    uint64_t Ignored0 : 1;
    uint64_t MustBeZero0 : 1;
    uint64_t MustBeZero1 : 1;
    uint64_t Available0 : 3;
    // Crashes the computer when accessed for some reason
    uint64_t LowerLevelPageTableAddress : 40;
    uint64_t Available1 : 11;
    uint64_t NotExecute : 1;
};

struct Level3PageEntry {
    // 0 - Next level address is NOT valid; 1 - is valid
    uint64_t Present : 1;
    // 0 - Read Only; 1 - Read and Write
    uint64_t ReadWrite : 1;
    // 0 - Userspace cannot access this; 1 - Userspace can access this
    uint64_t UserSupervisor : 1;
    uint64_t PageLevelWritethrough : 1;
    uint64_t PageLevelCacheDisable : 1;
    uint64_t Accessed : 1;
    uint64_t Ignored0 : 1;
    uint64_t PageSize : 1;
    uint64_t Ignored1 : 1;
    uint64_t Available0 : 3;
    // Crashes the computer when accessed for some reason
    uint64_t LowerLevelPageTableAddress : 40;
    uint64_t Available1 : 11;
    uint64_t NotExecute : 1;
};

using Level2PageEntry = Level3PageEntry;

struct Level1PageEntry {
    // 0 - Next level address is NOT valid; 1 - is valid
    uint64_t Present : 1;
    // 0 - Read Only; 1 - Read and Write
    uint64_t ReadWrite : 1;
    // 0 - Userspace cannot access this; 1 - Userspace can access this
    uint64_t UserSupervisor : 1;
    uint64_t PageLevelWritethrough : 1;
    uint64_t PageLevelCacheDisable : 1;
    uint64_t Accessed : 1;
    uint64_t Dirty : 1;
    uint64_t PAT : 1;
    uint64_t GlobalPage : 1;
    uint64_t Available0 : 3;
    uint64_t PhysicalPageAddress : 40;
    uint64_t Available1 : 6;
    uint64_t MemoryProtectionKey : 4;
    uint64_t NotExecute : 1;
};

inline void dump_page_table(const uint64_t page_table_phys) {
    print("PageTableDump:\n");
    const uint64_t* level4entries = reinterpret_cast<const uint64_t*>(page_table_phys);
    const Level4PageEntry* _level4entries = reinterpret_cast<const Level4PageEntry*>(page_table_phys);
    for (size_t i = 0; i < 512; i++) {
        if ((level4entries[i] & 1) == 0) {
            continue;
        }
        const uint64_t level3addr = level4entries[i] & 0x000f'ffff'ffff'f000ull;

        print(
            "Level4 Entry {}: 0x{x}, ReadWrite: 0x{x}, UserSupervisor: 0x{x}, PageLevelWritethrough: 0x{x}"
                ", PageLevelCacheDisable: 0x{x}, Accessed: 0x{x}, Available0: 0x{x}, Available1: 0x{x}"
                ", NotExecute: 0x{x}, Level3 Addr: 0x{x}\n",
            i,
            level4entries[i],
            _level4entries[i].ReadWrite,
            _level4entries[i].UserSupervisor,
            _level4entries[i].PageLevelWritethrough,
            _level4entries[i].PageLevelCacheDisable,
            _level4entries[i].Accessed,
            _level4entries[i].Available0,
            _level4entries[i].Available1,
            _level4entries[i].NotExecute,
            level3addr
        );

        const uint64_t* level3entries = reinterpret_cast<const uint64_t*>(level3addr);
        const Level3PageEntry* _level3entries = reinterpret_cast<const Level3PageEntry*>(level3addr);
        for (size_t j = 0; j < 512; j++) {
            if ((level3entries[j] & 1) == 0) {
                continue;
            }
            const uint64_t level2addr = level3entries[j] & 0x000f'ffff'ffff'f000ull;

            print(
                "\tLevel3 Entry {}: 0x{x}, ReadWrite: 0x{x}, UserSupervisor: 0x{x}, PageLevelWritethrough: 0x{x}"
                    ", PageLevelCacheDisable: 0x{x}, Accessed: 0x{x}, PageSize: 0x{x}, Available0: 0x{x}"
                    ", Available1: 0x{x}, NotExecute: 0x{x}, Level2 Addr: 0x{x}\n",
                j,
                level3entries[j],
                _level3entries[j].ReadWrite,
                _level3entries[j].UserSupervisor,
                _level3entries[j].PageLevelWritethrough,
                _level3entries[j].PageLevelCacheDisable,
                _level3entries[j].Accessed,
                _level3entries[j].PageSize,
                _level3entries[j].Available0,
                _level3entries[j].Available1,
                _level3entries[j].NotExecute,
                level2addr
            );

            // 1 GiB Page Size
            if (_level3entries[j].PageSize == 1) {
                continue;
            }

            const uint64_t* level2entries = reinterpret_cast<const uint64_t*>(level2addr);
            const Level2PageEntry* _level2entries = reinterpret_cast<const Level2PageEntry*>(level2addr);
            for (size_t k = 0; k < 512; k++) {
                if ((level2entries[k] & 1) == 0) {
                    continue;
                }
                const uint64_t level1addr = level2entries[k] & 0x000f'ffff'ffff'f000ull;

                print(
                    "\t\tLevel2 Entry {}: 0x{x}, ReadWrite: 0x{x}, UserSupervisor: 0x{x}, PageLevelWritethrough: 0x{x}"
                        ", PageLevelCacheDisable: 0x{x}, Accessed: 0x{x}, PageSize: 0x{x}, Available0: 0x{x}"
                        ", Available1: 0x{x}, NotExecute: 0x{x}",
                    k,
                    level2entries[k],
                    _level2entries[k].ReadWrite,
                    _level2entries[k].UserSupervisor,
                    _level2entries[k].PageLevelWritethrough,
                    _level2entries[k].PageLevelCacheDisable,
                    _level2entries[k].Accessed,
                    _level2entries[k].PageSize,
                    _level2entries[k].Available0,
                    _level2entries[k].Available1,
                    _level2entries[k].NotExecute
                );

                // 2 MiB Page Size
                if (_level2entries[k].PageSize == 1) {
                    print(", VA: 0x{x}\n",
                        (i << 39) | (j << 30) | (k << 21)
                    );
                    continue;
                }

                print(", Level1 Addr: 0x{x}\n", level1addr);

                const uint64_t* level1entries = reinterpret_cast<const uint64_t*>(level1addr);
                const Level1PageEntry* _level1entries = reinterpret_cast<const Level1PageEntry*>(level1addr);
                for (size_t l = 0; l < 512; l++) {
                    if ((level1entries[l] & 1) == 0) {
                        continue;
                    }
                    const uint64_t physical_page = level1entries[l] & 0x000f'ffff'ffff'f000ull;

                    print(
                        "\t\t\tLevel1 Entry {}: 0x{x}, ReadWrite: 0x{x}, UserSupervisor: 0x{x}"
                            ", PageLevelWritethrough: 0x{x}, PageLevelCacheDisable: 0x{x}, Accessed: 0x{x}"
                            ", Dirty: 0x{x}, PAT: 0x{x}, GlobalPage: 0x{x}, Available0: 0x{x}"
                            "\n\t\t\t\t\tAvailable1: 0x{x}, MemoryProtectionKey: 0x{x}, NotExecute: 0x{x}, PhysicalPageAddress: 0x{x}, VA: 0x{x}\n",
                        l,
                        level1entries[l],
                        _level1entries[l].ReadWrite,
                        _level1entries[l].UserSupervisor,
                        _level1entries[l].PageLevelWritethrough,
                        _level1entries[l].PageLevelCacheDisable,
                        _level1entries[l].Accessed,
                        _level1entries[l].Dirty,
                        _level1entries[l].PAT,
                        _level1entries[l].GlobalPage,
                        _level1entries[l].Available0,
                        _level1entries[l].Available1,
                        _level1entries[l].MemoryProtectionKey,
                        _level1entries[l].NotExecute,
                        physical_page,
                        (i << 39) | (j << 30) | (k << 21) | (l << 12)
                    );
                }
            }
        }
    }
}
