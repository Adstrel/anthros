#pragma once
#include "efi.hpp"
#include "acpi.hpp"

struct SysTableEntries {
    RSDP* rsdp;
};

extern EFI_SYSTEM_TABLE* g_system_table;

SysTableEntries walk_efi_table_entries();
EFI_MTFTP4_PROTOCOL* get_efi_mtftp4_protocol();
uint8_t* efi_mtftp4_protocol_get_file(EFI_MTFTP4_PROTOCOL* protocol, const char8_t* filename, OUT size_t* file_size);

// Should be called as soon as we enter execution in UEFI land
inline void efi_init_global_state(EFI_SYSTEM_TABLE* system_table) {
    g_system_table = system_table;
}

// Should be called as soon as successfully Exited UEFI Boot Services
inline void efi_cleanup_global_state() {
    g_system_table = nullptr;
}

[[noreturn]] inline void panic(const wchar_t* str) {
    g_system_table->ConOut->OutputString(g_system_table->ConOut, str);
    g_system_table->ConOut->OutputString(g_system_table->ConOut, L"\r\n");
    while (true);
}
