#pragma once
#include "common.hpp"


// Spec: https://learn.microsoft.com/en-us/windows/win32/debug/pe-format
namespace PE {

// COFF File Header
// Spec: https://learn.microsoft.com/en-us/windows/win32/debug/pe-format#coff-file-header-object-and-image
struct Header {
    // Spec: https://learn.microsoft.com/en-us/windows/win32/debug/pe-format#machine-types
    enum class MachineType : uint16_t {
        // Any machine type
        Unknown = 0x0,
        // Matsushita AM33
        AM33 = 0x1d3,
        // x64 (x86_64)
        AMD64 = 0x8664,
        // ARM little endian
        ARM = 0x1c0,
        // ARM64 little endian
        ARM64 = 0xaa64,
        // ARM Thumb-2 little endian
        ARMNT = 0x1c4,
        // EFI byte code
        EBC = 0xebc,
        // Intel 386 or later processors and compatible processors
        I386 = 0x14c,
        // Intel Itanium processor family
        IA64 = 0x200,
        // LoongArch 32-bit processor family
        LOONGARCH32 = 0x6232,
        // LoongArch 64-bit processor family
        LOONGARCH64 = 0x6264,
        // Mitsubishi M32R little endian
        M32R = 0x9041,
        // MIPS16
        MIPS16 = 0x266,
        // MIPS with FPU
        MIPSFPU = 0x366,
        // MIPS16 with FPU
        MIPSFPU16 = 0x466,
        // Power PC little endian
        POWERPC = 0x1f0,
        // Power PC with floating point support
        POWERPCFP = 0x1f1,
        // MIPS little endian
        R4000 = 0x166,
        // RISC-V 32-bit address space
        RISCV32 = 0x5032,
        // RISC-V 64-bit address space
        RISCV64 = 0x5064,
        // RISC-V 128-bit address space
        RISCV128 = 0x5128,
        // Hitachi SH3
        SH3 = 0x1a2,
        // Hitachi SH3 DSP
        SH3DSP = 0x1a3,
        // Hitachi SH4
        SH4 = 0x1a6,
        // Hitachi SH5
        SH5 = 0x1a8,
        // Thumb
        THUMB = 0x1c2,
        // MIPS little-endian WCE v2
        WCEMIPSV2 = 0x169
    };

    // Spec: https://learn.microsoft.com/en-us/windows/win32/debug/pe-format#characteristics
    struct Flags {
        uint16_t RELOCS_STRIPPED : 1;
        uint16_t EXECUTABLE_IMAGE : 1;
        uint16_t LINE_NUMS_STRIPPED : 1;
        uint16_t LOCAL_SYMS_STRIPPED : 1;
        uint16_t AGGRESIVE_WS_TRIM : 1;
        uint16_t LARGE_ADDRESS_AWARE : 1;
        uint16_t Reserved : 1;
        uint16_t BYTES_REVERSED_LO : 1;
        uint16_t _32BIT_MACHINE : 1;
        uint16_t DEBUG_STRIPPED : 1;
        uint16_t REMOVABLE_RUN_FROM_SWAP : 1;
        uint16_t NET_RUN_FROM_SWAP : 1;
        uint16_t SYSTEM : 1;
        uint16_t DLL : 1;
        uint16_t UP_SYSTEM_ONLY : 1;
        uint16_t BYTES_REVERSED_HI : 1;

        operator uint16_t() const noexcept {
            return *reinterpret_cast<const uint16_t*>(this);
        }
    };

    MachineType Machine;
    uint16_t NumberOfSections;
    uint32_t TimeDateStamp;
    uint32_t PointerToSymbolTable;
    uint32_t NumberOfSymbols;
    uint16_t SizeOfOptionalHeader;
    Flags Characteristics;
};

// Spec: https://learn.microsoft.com/en-us/windows/win32/debug/pe-format#optional-header-image-only
struct OptionalHeaderPE32Plus {
    struct StandardFields {
        uint16_t Magic;
        uint8_t MajorLinkerVersion;
        uint8_t MinorLinkerVersion;
        uint32_t SizeOfCode;
        uint32_t SizeOfInitializedData;
        uint32_t SizeOfUninitializedData;
        uint32_t AddressOfEntryPoint;
        uint32_t BaseOfCode;
    };

    struct WindowsSpecificFields {
        uint64_t ImageBase;
        uint32_t SectionAlignment;
        uint32_t FileAlignment;
        uint16_t MajorOperatingSystemVersion;
        uint16_t MinorOperatingSystemVersion;
        uint16_t MajorImageVersion;
        uint16_t MinorImageVersion;
        uint16_t MajorSubsystemVersion;
        uint16_t MinorSubsystemVersion;
        // Reserved; must be 0
        uint32_t Win32VersionValue;
        uint32_t SizeOfImage;
        uint32_t SizeOfHeaders;
        uint32_t Checksum;
        uint16_t Subsystem;
        uint16_t DllCharacteristics;
        uint64_t SizeOfStackReserve;
        uint64_t SizeOfStackCommit;
        uint64_t SizeOfHeapReserve;
        uint64_t SizeOfHeapCommit;
        // Reserved; must be 0
        uint32_t LoaderFlags;
        uint32_t NumberOfRvaAndSizes;
    };

    struct DataDirectory {
        uint32_t VirtualAddress;
        uint32_t Size;
    };

    StandardFields StandardFields;
    WindowsSpecificFields WindowsSpecificFields;
    DataDirectory ExportTable;
    DataDirectory ImportTable;
    DataDirectory ResouceTable;
    DataDirectory ExceptionTable;
    DataDirectory CertificateTable;
    DataDirectory BaseRelocationTable;
    DataDirectory Debug;
    // Reserved; must be 0
    DataDirectory Architecture;
    DataDirectory GlobalPtr;
    DataDirectory TLSTable;
    DataDirectory LoadConfigTable;
    DataDirectory BoundImport;
    DataDirectory IAT;
    DataDirectory DelayImportDescriptor;
    DataDirectory CLRRuntimeHeader;
    // Reserved; must be 0
    DataDirectory Reserved;
};

struct SectionHeader {
    struct Flags {
        uint32_t Reserved0 : 3;
        uint32_t TYPE_NO_PAD : 1;
        uint32_t Reserved1 : 1;
        uint32_t CNT_CODE : 1;
        uint32_t CNT_INITIALIZED_DATA : 1;
        uint32_t CNT_UNINITIALIZED_DATA : 1;
        uint32_t LNK_OTHER : 1;
        uint32_t LNK_INFO : 1;
        uint32_t Reserved2 : 1;
        uint32_t LNK_REMOVE : 1;
        uint32_t LNK_COMDAT : 1;
        uint32_t Reserved3 : 2;
        uint32_t GPREL : 1;
        uint32_t Reserved4 : 1;
        uint32_t MEM_PURGABLE : 1;
        uint32_t MEM_LOCKED : 1;
        uint32_t MEM_PRELOAD : 1;
        uint32_t ALIGN_N_BYTES : 4;
        uint32_t LNK_NRELOC_OVFL : 1;
        uint32_t MEM_DISCARDABLE : 1;
        uint32_t MEM_NOT_CACHED : 1;
        uint32_t MEM_NOT_PAGED : 1;
        uint32_t MEM_SHARED : 1;
        uint32_t MEM_EXECUTE : 1;
        uint32_t MEM_READ : 1;
        uint32_t MEM_WRITE : 1;
    };

    char8_t Name[8];
    uint32_t VirtualSize;
    uint32_t VirtualAddress;
    uint32_t SizeOfRawData;
    uint32_t PointerToRawData;
    uint32_t PointerToRelocations;
    uint32_t PointerToLinenumbers;
    uint16_t NumberOfRelocations;
    uint16_t NumberofLinenumbers;
    Flags Characteristics;
};


// Important information you need for mapping an executable PE image
struct ExecutableInfo {
    size_t needed_pages;
    const Header* coff_header;
    const OptionalHeaderPE32Plus* optional_header;
    const SectionHeader* section_table;
};

ExecutableInfo parse_executable(const uint8_t* file_buffer, const size_t filesize);

} // namespace PE
