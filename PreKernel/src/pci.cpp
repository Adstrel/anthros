#include "pci.hpp"
#include "print.hpp"

namespace PCI {

void enumerate_all_ranges(MCFG* mcfg) {
    if (mcfg == nullptr) {
        flush();
        panic(L"Couldn't find the MCFG");
    }

    const size_t mcfg_structs_number = (mcfg->header.Length - sizeof(MCFG)) / sizeof(CSBAAS);
    print("MCFG Length: {}, sizeof(MCFG): {}, Number of structs: {}\r\n",
        mcfg->header.Length,
        sizeof(MCFG),
        mcfg_structs_number
    );

    for (size_t i = 0; i < mcfg_structs_number; i++) {
        CSBAAS& pci_range = *(reinterpret_cast<CSBAAS*>(mcfg + 1) + i);
        print("MCFG range {} - Base address: 0x{x}, Segment Group: {}, Start bus num: {}, End bus num: {}\r\n",
            i,
            pci_range.base_address,
            pci_range.pci_segment_group_number,
            pci_range.start_pci,
            pci_range.end_pci
        );

        // Brute force check all combinations of busses and divices in this PCI memory range
        for (uint64_t bus = pci_range.start_pci; bus <= pci_range.end_pci; bus++) {
            for (uint64_t Device = 0; Device < 32; Device++) {
                uint64_t Function = 0;
                // 4096 byte aligned address for this combination of PCI range, bus, device, and function
                uint64_t pci_phys_addr = pci_range.base_address + (
                    ((bus - pci_range.start_pci) << 20) |
                    (Device << 15) |
                    (Function << 12)
                );
                PCI::Header* header = reinterpret_cast<PCI::Header*>(pci_phys_addr);

                if (header->VendorID == 0xffff) {
                    continue;
                }

                print("Bus {}, Device {}, PCI BUS ADDR: 0x{x}\r\n", bus, Device, pci_phys_addr);
                print("\t- VendorID: {} (0x{x}), DeviceID: {} (0x{x})\r\n", header->VendorID, header->VendorID, header->DeviceID, header->DeviceID);
                print("\t- Header Type: 0x{x}, Class code: 0x{x}, Subclass: 0x{x}, ProgIF: 0x{x}\r\n",
                    header->HeaderType,
                    header->ClassCode,
                    header->Subclass,
                    header->ProgIF
                );
                if (header->HeaderType == 0) {
                    PCI::HeaderType_0* header0 = reinterpret_cast<PCI::HeaderType_0*>(header);
                    print("\t- Subsystem Vendor ID: {} (0x{x}), Subsystem ID: {} (0x{x})\r\n",
                        header0->SubsystemVendorID,
                        header0->SubsystemVendorID,
                        header0->SubsystemID,
                        header0->SubsystemID
                    );
                }
                flush();
            }
        }
    }
}

} // namespace PCI