#pragma once
#include "efi.hpp"


#define assert(expr)

template<typename T, size_t SIZE>
consteval size_t countof(T (&)[SIZE]) {
    return SIZE;
}
