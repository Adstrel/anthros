#pragma once
#include "common.hpp"
#include "efi_helpers.hpp"


inline uint8_t* alloc_bytes(const size_t bytes) {
    void* ptr;
    const EFI_STATUS ret = g_system_table->BootServices->AllocatePool(EfiLoaderData, bytes, &ptr);
    if (ret != EFI_SUCCESS) {
        panic(L"Couldn't allocate memory!");
    }
    return static_cast<uint8_t*>(ptr);
}

extern size_t g_allocated_pages;
extern size_t g_begin_phys_addr;
extern size_t g_kernel_pages_needed;

inline void get_phys_page_init(const size_t begin_phys_addr, const size_t kernel_pages_needed) {
    g_begin_phys_addr = begin_phys_addr;
    g_kernel_pages_needed = kernel_pages_needed;
}

inline uint64_t get_phys_page() {
    const uint64_t new_page = g_begin_phys_addr + 0x1000 * (g_kernel_pages_needed + g_allocated_pages++);
    return new_page;
};
