#pragma once
#include "common.hpp"
#include "efi_helpers.hpp"


// Send a character to Bochs console
inline void dbg_push_char(const char c) {
    asm volatile("outb %0, %1" :: "a"(c), "Nd"(0xe9));
}

extern size_t g_static_buffer_index;
extern wchar_t g_static_buffer[2044];

inline void flush() {
    if (g_static_buffer_index == 0) {
        return;
    }
    g_static_buffer[g_static_buffer_index] = L'\0';
    g_system_table->ConOut->OutputString(g_system_table->ConOut, g_static_buffer);
    g_static_buffer_index = 0;
}

inline void push_char(const char c) {
    dbg_push_char(c);
    if (!g_system_table) {
        return;
    }

    if (g_static_buffer_index >= countof(g_static_buffer) - 2) {
        flush();
    }
    g_static_buffer[g_static_buffer_index++] = c;
}

inline void print_int(uint64_t value, const uint32_t base) {
    assert(base >= 2 && base <= 36);
    if (value == 0) {
        push_char(L'0');
        return;
    }

    char buffer[65] = {0};
    size_t index = countof(buffer) - 1;
    while (value != 0) {
        const uint64_t digit = value % base;
        value /= base;

        buffer[index - 1] = static_cast<char>(digit >= 10 ? 'a' + digit - 10 : '0' + digit);
        index--;
    }

    while (buffer[index] != L'\0') {
        push_char(buffer[index++]);
    }
}

inline void print_fmt(const char** fmt) {
    while (**fmt != '\0') {
        if (**fmt == '{') {
            ++(*fmt);
            if (**fmt == '\0') {
                return;
            }
            if (**fmt == '{') {
                push_char('{');
                ++(*fmt);
                continue;
            }
            while (**fmt != '\0' && **fmt != '}') {
                ++(*fmt);
            }
            if (**fmt == '}') {
                ++(*fmt);
                return;
            }
            panic(L"Error in format string!");
        }
        push_char(**fmt);
        (*fmt)++;
    }
};

inline void print_arg(const char* str) {
    print_fmt(&str);
}

inline void print_arg(const EFI_MEMORY_TYPE type) {
    const char* fmt;
    switch (type) {
    case EfiReservedMemoryType:
        fmt = "EfiReservedMemoryType";
        break;
    case EfiLoaderCode:
        fmt = "EfiLoaderCode";
        break;
    case EfiLoaderData:
        fmt = "EfiLoaderData";
        break;
    case EfiBootServicesCode:
        fmt = "EfiBootServicesCode";
        break;
    case EfiBootServicesData:
        fmt = "EfiBootServicesData";
        break;
    case EfiRuntimeServicesCode:
        fmt = "EfiRuntimeServicesCode";
        break;
    case EfiRuntimeServicesData:
        fmt = "EfiRuntimeServicesData";
        break;
    case EfiConventionalMemory:
        fmt = "EfiConventionalMemory";
        break;
    case EfiUnusableMemory:
        fmt = "EfiUnusableMemory";
        break;
    case EfiACPIReclaimMemory:
        fmt = "EfiACPIReclaimMemory";
        break;
    case EfiACPIMemoryNVS:
        fmt = "EfiACPIMemoryNVS";
        break;
    case EfiMemoryMappedIO:
        fmt = "EfiMemoryMappedIO";
        break;
    case EfiMemoryMappedIOPortSpace:
        fmt = "EfiMemoryMappedIOPortSpace";
        break;
    case EfiPalCode:
        fmt = "EfiPalCode";
        break;
    case EfiPersistentMemory:
        fmt = "EfiPersistentMemory";
        break;
    case EfiUnacceptedMemoryType:
        fmt = "EfiUnacceptedMemoryType";
        break;
    case EfiMaxMemoryType:
        fmt = "EfiMaxMemoryType";
        break;
    default:
        fmt = "Unknown";
        break;
    }
    print_fmt(&fmt);
}

template<typename T>
constexpr bool IsInteger =
    __is_same(T, signed char) ||
    __is_same(T, char) ||
    __is_same(T, unsigned char) ||
    __is_same(T, short) ||
    __is_same(T, unsigned short) ||
    __is_same(T, int) ||
    __is_same(T, unsigned int) ||
    __is_same(T, long) ||
    __is_same(T, unsigned long) ||
    __is_same(T, long long) ||
    __is_same(T, unsigned long long);

inline void print_arg(const EFI_GUID& guid) {
    const auto charint = [] (char* ptr, uint32_t value) {
        for (int i = 0; i < 8; i++) {
            uint32_t digit = value % 16;
            value /= 16;
            ptr[8 - 1 - i] = static_cast<char>(digit >= 10 ? 'a' + digit - 10 : '0' + digit);
        }
    };
    const auto pushbuff = [] (const char* ptr, size_t num) {
        for (size_t i = 0; i < num; i++) {
            push_char(ptr[i]);
        }
    };
    char buffer[8];

    charint(buffer, guid.block0);
    pushbuff(buffer, 8);
    push_char('-');
    charint(buffer, guid.block1);
    pushbuff(buffer + 4, 4);
    push_char('-');
    charint(buffer, guid.block2);
    pushbuff(buffer + 4, 4);
    push_char('-');
    charint(buffer, guid.block3[0]);
    pushbuff(buffer + 6, 2);
    charint(buffer, guid.block3[1]);
    pushbuff(buffer + 6, 2);
    push_char('-');
    charint(buffer, guid.block3[2]);
    pushbuff(buffer + 6, 2);
    charint(buffer, guid.block3[3]);
    pushbuff(buffer + 6, 2);
    charint(buffer, guid.block3[4]);
    pushbuff(buffer + 6, 2);
    charint(buffer, guid.block3[5]);
    pushbuff(buffer + 6, 2);
    charint(buffer, guid.block3[6]);
    pushbuff(buffer + 6, 2);
    charint(buffer, guid.block3[7]);
    pushbuff(buffer + 6, 2);
}

inline void print_arg(bool value) {
    const char* fmt = value ? "true" : "false";
    print_fmt(&fmt);
}

template<typename T>
void print_fmt_arg(const char** fmt, T arg) {
    print_fmt(fmt);

    // Special case for integer, bc it has a format specifier
    if constexpr (IsInteger<T>) {
        uint32_t base = 10;
        if (*(*fmt - 2) == 'x') {
            base = 16;
        } else if (*(*fmt - 2) == 'b') {
            base = 2;
        }

        print_int(arg, base);
        return;
    }

    // All other cases do not have format specifiers
    print_arg(arg);
}

template<typename... Args_t>
void print(const char* fmt, Args_t... args) {
    (print_fmt_arg(&fmt, args), ...);
    print_fmt(&fmt);
}
