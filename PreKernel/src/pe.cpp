#include "pe.hpp"
#include "print.hpp"
#include "acpi.hpp"

namespace PE {

ExecutableInfo parse_executable(const uint8_t* file_buffer, const size_t filesize) {
    if (filesize < 0x40) {
        flush();
        panic(L"Downloaded kernel binary is too small to be a PE file");
    }

    ExecutableInfo info{};

    // At file offset 0x3c is a number with the offset to the PE signature
    const uint32_t* pe_signature_offset = reinterpret_cast<const uint32_t*>(file_buffer + 0x3c);
    if (*pe_signature_offset >= filesize) {
        flush();
        panic(L"Downloaded kernel binary has invalid PE signature offset (bigger that the filesize)");
    }

    print("Kernel PE -- PE signature offset: {}\r\n", *pe_signature_offset);
    const uint32_t* pe_signature = reinterpret_cast<const uint32_t*>(file_buffer + *pe_signature_offset);
    if (*pe_signature != ascii_to_uint32_t(u8"PE\0\0")) {
        flush();
        panic(L"Downloaded kernel binary has invalid PE signature");
    }

    const PE::Header* pe_header = reinterpret_cast<const PE::Header*>(pe_signature + 1);
    info.coff_header = pe_header;
    print("Kernel PE COFF Header -- Number of sections: {}\r\n", pe_header->NumberOfSections);
    print("Kernel PE COFF Header -- Number of symbols: {}\r\n", pe_header->NumberOfSymbols);
    print("Kernel PE COFF Header -- Characteristics: 0b{b}\r\n", static_cast<uint16_t>(pe_header->Characteristics));
    print("Kernel PE COFF Header -- Size of optional header: {}\r\n", pe_header->SizeOfOptionalHeader);
    if (pe_header->SizeOfOptionalHeader < 2) {
        flush();
        panic(L"Downloaded kernel binary too small for a PE optional header signature");
    }

    const uint16_t* pe_optional_header_magic = reinterpret_cast<const uint16_t*>(pe_header + 1);
    print("Kernel PE Optinal Header Magic: 0x{x} ({})\r\n",
        *pe_optional_header_magic,
        *pe_optional_header_magic == 0x20b ? L"PE32+" :
        *pe_optional_header_magic == 0x10b ? L"PE32"  : L"Wrong header"
    );
    if (*pe_optional_header_magic != 0x20b) {
        flush();
        panic(L"Downloaded kernel binary is not in PE32+ fromat");
    }
    if (pe_header->SizeOfOptionalHeader < 240) {
        flush();
        panic(L"Downloaded kernel binary does not have the full optional header");
    }

    const PE::OptionalHeaderPE32Plus* pe_optional_header = reinterpret_cast<const PE::OptionalHeaderPE32Plus*>(pe_optional_header_magic);
    info.optional_header = pe_optional_header;
    print("Kernel PE SectionAlignment: 0x{x}\r\n", pe_optional_header->WindowsSpecificFields.SectionAlignment);
    print("Kernel PE FileAlignment: 0x{x}\r\n", pe_optional_header->WindowsSpecificFields.FileAlignment);
    print("Kernel PE Entry Point VA: 0x{x}\r\n", pe_optional_header->StandardFields.AddressOfEntryPoint);
    print("Kernel PE Base of Code: 0x{x}\r\n", pe_optional_header->StandardFields.BaseOfCode);
    print("Kernel PE Size of Code: {}\r\n", pe_optional_header->StandardFields.SizeOfCode);
    print("Kernel PE Size of Uninitialized Data: {}\r\n", pe_optional_header->StandardFields.SizeOfUninitializedData);
    print("Kernel PE Size of Initialized Data: {}\r\n", pe_optional_header->StandardFields.SizeOfInitializedData);
    print("Kernel PE Linker Version: {}.{}\r\n",
        pe_optional_header->StandardFields.MajorLinkerVersion,
        pe_optional_header->StandardFields.MinorLinkerVersion
    );
    print("Kernel PE Image Base: 0x{x}\r\n", pe_optional_header->WindowsSpecificFields.ImageBase);
    print("Kernel PE Number of RVA and Sizes: {}\r\n", pe_optional_header->WindowsSpecificFields.NumberOfRvaAndSizes);
    print("Kernel PE Subsystem: {}\r\n", pe_optional_header->WindowsSpecificFields.Subsystem);
    print("Kernel PE Win32 Version: {}\r\n", pe_optional_header->WindowsSpecificFields.Win32VersionValue);

    const PE::SectionHeader* pe_section_table = reinterpret_cast<const PE::SectionHeader*>(
        reinterpret_cast<const uint8_t*>(pe_optional_header) + pe_header->SizeOfOptionalHeader
    );
    info.section_table = pe_section_table;

    for (size_t index = 0; index < pe_header->NumberOfSections; index++) {
        const PE::SectionHeader* pe_section = pe_section_table + index;

        print("Kernel PE Section {} -- Name: \"", index + 1);
        for (size_t i = 0; i < 8; i++) {
            if (pe_section->Name[i] == '\0') {
                break;
            }
            push_char(pe_section->Name[i]);
        }
        print("\", VirtualSize: {}, VirtualAddress: 0x{x}\r\n", pe_section->VirtualSize, pe_section->VirtualAddress);
        info.needed_pages += pe_section->VirtualSize / 4096 + (pe_section->VirtualSize % 4096 == 0 ? 0 : 1);
    }
    print("The Kernel will need to have {} page{} of memory to be fully loaded.\r\n",
        info.needed_pages,
        info.needed_pages != 1 ? "s" : ""
    );

    return info;
}

} // namespace PE
