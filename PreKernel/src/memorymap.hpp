#pragma once
#include "efi.hpp"
#include "print.hpp"
#include "alloc.hpp"


struct MemoryMap {
    size_t buffer_byte_size{0};
    size_t buffer_byte_capacity{0};
    EFI_MEMORY_DESCRIPTOR* buffer{nullptr};
    size_t key{0};
    size_t descriptor_size{0};
    uint32_t descriptor_version{0};

    // Frees the old buffer if one was already allocated
    inline void Update() noexcept {
        EFI_STATUS res;
        while (true) {
            buffer_byte_size = buffer_byte_capacity;
            res = g_system_table->BootServices->GetMemoryMap(
                &buffer_byte_size,
                buffer,
                &key,
                &descriptor_size,
                &descriptor_version
            );

            if (res == EFI_SUCCESS) {
                break;
            }

            if (res != EFI_BUFFER_TOO_SMALL) {
                print("Couldn't get a memory map; return value: 0x{x}\n", res);
                panic(L"Couldn't get a memory map");
            }

            // Try allocating a bigger buffer
            buffer_byte_capacity = buffer_byte_size + 1024;
            if (buffer) {
                g_system_table->BootServices->FreePool(buffer);
            }
            buffer = reinterpret_cast<EFI_MEMORY_DESCRIPTOR*>(alloc_bytes(buffer_byte_capacity));
        }
    }

    inline static MemoryMap GetNew() noexcept {
        MemoryMap result{};
        result.Update();
        return result;
    }

    // Freeing of the underlying buffer is explicit, because we don't stay in UEFI land forever and
    // we might actually want to do a memory leak (so we can forward the buffer to the Kernel)
    inline void FreeBuffer() noexcept {
        if (buffer != nullptr) {
            g_system_table->BootServices->FreePool(buffer);
            *this = MemoryMap{};
        }
    }

    class Iterator {
        EFI_MEMORY_DESCRIPTOR* ptr;
        const size_t descriptor_size;

    public:
        inline Iterator(EFI_MEMORY_DESCRIPTOR* ptr, size_t descriptor_size)
            : ptr(ptr), descriptor_size(descriptor_size) {}

        inline Iterator& operator++() noexcept {
            ptr = reinterpret_cast<EFI_MEMORY_DESCRIPTOR*>(
                reinterpret_cast<uint8_t*>(ptr) + descriptor_size
            );
            return *this;
        }

        inline EFI_MEMORY_DESCRIPTOR& operator*() noexcept {
            return *ptr;
        }

        inline const EFI_MEMORY_DESCRIPTOR& operator*() const noexcept {
            return *ptr;
        }

        inline bool operator!=(const Iterator& other) const noexcept {
            return ptr != other.ptr;
        }
    };

    inline Iterator begin() noexcept {
        return {
            buffer,
            descriptor_size
        };
    }

    inline Iterator end() noexcept {
        return {
            reinterpret_cast<EFI_MEMORY_DESCRIPTOR*>(
                reinterpret_cast<uint8_t*>(buffer) + buffer_byte_size
            ),
            descriptor_size
        };
    }

    class ConstIterator {
        const EFI_MEMORY_DESCRIPTOR* ptr;
        const size_t descriptor_size;

    public:
        inline ConstIterator(const EFI_MEMORY_DESCRIPTOR* ptr, size_t descriptor_size)
            : ptr(ptr), descriptor_size(descriptor_size) {}

        inline ConstIterator& operator++() noexcept {
            ptr = reinterpret_cast<const EFI_MEMORY_DESCRIPTOR*>(
                reinterpret_cast<const uint8_t*>(ptr) + descriptor_size
            );
            return *this;
        }

        inline const EFI_MEMORY_DESCRIPTOR& operator*() const noexcept {
            return *ptr;
        }

        inline bool operator!=(const ConstIterator& other) const noexcept {
            return ptr != other.ptr;
        }
    };

    inline ConstIterator begin() const noexcept {
        return {
            buffer,
            descriptor_size
        };
    }

    inline ConstIterator end() const noexcept {
        return {
            reinterpret_cast<const EFI_MEMORY_DESCRIPTOR*>(
                reinterpret_cast<const uint8_t*>(buffer) + buffer_byte_size
            ),
            descriptor_size
        };
    }
};

inline void dump_memory_map(const MemoryMap& mem_map) {
    for (size_t i = 0; const auto& mem_region : mem_map) {
        print("[{}]:\tType({}), PhysicalStart(0x{x}), VirtualStart(0x{x}), NumberOfPages({}), Attributes(0x{x})\r\n",
            i++,
            mem_region.Type,
            mem_region.PhysicalStart,
            mem_region.VirtualStart,
            mem_region.NumberOfPages,
            mem_region.Attribute
        );
    }
}
