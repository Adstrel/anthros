#include "acpi.hpp"
#include "print.hpp"

void walk_rsdp(const RSDP* rsdp, OUT XSDT** xsdt) {
    print("RSDP signature is correct: {}\r\n", rsdp->Signature == ascii_to_uint64_t(u8"RSD PTR "));
    print("OEMID: \"");
    for (char8_t c : rsdp->OEMID) {
        push_char(c);
    }
    print("\"\r\n");
    print("RSDP ACPI Revision: {}\r\n", rsdp->Revision);
    print("RSDP Length: {}, sizeof(RSDP): {}\r\n", rsdp->Length, sizeof(RSDP));
    print("RSDT physical address: 0x{x}\r\n", rsdp->RsdtAddress);
    print("XSDT physical address: 0x{x}\r\n", rsdp->XsdtAddress);

    *xsdt = reinterpret_cast<XSDT*>(rsdp->XsdtAddress);
}

void walk_xsdt(XSDT* xsdt, OUT FADT** fadt, OUT MCFG** mcfg) {
    const size_t n_entries = (xsdt->header.Length - sizeof(XSDT)) / 8;
    print("XSDT Length: {}, sizeof(XSDT): {}, Number of entries: {}\r\n",
        xsdt->header.Length,
        sizeof(XSDT),
        n_entries
    );

    const uint64_t* entries = reinterpret_cast<uint64_t*>(xsdt + 1);
    for (size_t i = 0; i < n_entries; i++) {
        uint64_t table_phys_addr = entries[i];

        uint32_t table_signature = *reinterpret_cast<uint32_t*>(table_phys_addr);
        if (table_signature == ascii_to_uint32_t(u8"FACP")) {
            *fadt = reinterpret_cast<FADT*>(table_phys_addr);
        } else if (table_signature == ascii_to_uint32_t(u8"MCFG")) {
            *mcfg = reinterpret_cast<MCFG*>(table_phys_addr);
        }

        char table_signature_str[5];
        for (size_t i = 0; i < 5; i++) {
            table_signature_str[i] = static_cast<char>(0xff & table_signature);
            table_signature >>= 8;
        }
        table_signature_str[4] = '\0';

        print("XSDT Entry Address: {x}, Signature: \"{}\"\r\n", table_phys_addr, table_signature_str);
    }
}

void walk_fadt(FADT* fadt, OUT FACS** facs, OUT DSDT** dsdt) {
    if (fadt == nullptr) {
        flush();
        panic(L"Couldn't find the FADT");
    }

    print("FADT Stuff:\r\n");
    print("FADT length: {}, sizeof(FADT): {}\r\n", fadt->header.Length, sizeof(FADT));
    print("Version: Major({}), Minor({}), Errata({})\r\n",
        fadt->header.Revision,
        fadt->FADT_Minor_Version & 0xf,
        (fadt->FADT_Minor_Version & 0xf0) >> 4
    );
    print("ACPI_ENABLE: {}, ACPI_DISABLE: {}\r\n", fadt->ACPI_ENABLE, fadt->ACPI_DISABLE);
    print("FIRMWARE_CTRL: {x}\r\n", fadt->FIRMWARE_CTRL);
    print("X_DSDR: {x}\r\n", fadt->X_DSDT);
    print("Reduced ACPI: {}\r\n", static_cast<bool>(fadt->Flags.HW_REDUCED_ACPI));
    print("Preferred PM Profile: {x}\r\n", fadt->Preferred_PM_Profile);

    const size_t X_FACS_offset = reinterpret_cast<uint8_t*>(&fadt->X_FIRMWARE_CTRL) - reinterpret_cast<uint8_t*>(fadt);
    if (X_FACS_offset + sizeof(fadt->X_FIRMWARE_CTRL) <= fadt->header.Length && fadt->X_FIRMWARE_CTRL != 0) {
        *facs = reinterpret_cast<FACS*>(fadt->X_FIRMWARE_CTRL);
    } else if (fadt->FIRMWARE_CTRL != 0) {
        *facs = reinterpret_cast<FACS*>(static_cast<uint64_t>(fadt->FIRMWARE_CTRL));
    }

    const size_t X_DSDT_offset = reinterpret_cast<uint8_t*>(&fadt->X_DSDT) - reinterpret_cast<uint8_t*>(fadt);
    if (X_DSDT_offset + sizeof(fadt->X_DSDT) <= fadt->header.Length && fadt->X_DSDT != 0) {
        *dsdt = reinterpret_cast<DSDT*>(fadt->X_DSDT);
    } else if (fadt->DSDT != 0) {
        *dsdt = reinterpret_cast<DSDT*>(static_cast<uint64_t>(fadt->DSDT));
    }
}

AcpiTables walk_acpi(RSDP* rsdp) {
    if (rsdp == nullptr) {
        flush();
        panic(L"RSDP structure was not found");
    }

    AcpiTables tables{};
    tables.rsdp = rsdp;

    walk_rsdp(tables.rsdp, &tables.xsdt);
    walk_xsdt(tables.xsdt, &tables.fadt, &tables.mcfg);
    walk_fadt(tables.fadt, &tables.facs, &tables.dsdt);

    // FACS and DSDT should be in all supported systems
    if (tables.facs == nullptr) {
        flush();
        panic(L"Couldn't find the FACS pointer in the FADT");
    }
    if (tables.dsdt == nullptr) {
        flush();
        panic(L"Couldn't find the DSDT pointer in the FADT");
    }

    print("FACS Length: {}, sizeof(FACS): {}\r\n", tables.facs->Length, sizeof(FACS));
    print("DSDT Length: {}\r\n", tables.dsdt->header.Length);

    return tables;
}
