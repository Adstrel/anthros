#pragma once
#include <stdint.h>
#include <stddef.h>


#define EFIAPI __attribute__((ms_abi))
#define IN
#define OUT

using EFI_STATUS = uint64_t;
using EFI_HANDLE = void*;
using EFI_EVENT = void*;
using EFI_PHYSICAL_ADDRESS = uint64_t;
using EFI_VIRTUAL_ADDRESS = uint64_t;

struct EFI_TABLE_HEADER {
    uint64_t Signature;
    uint32_t Revision;
    uint32_t HeaderSize;
    uint32_t CRC32;
    uint32_t Reserved;
};

// Temporary definitions
using EFI_SIMPLE_TEXT_INPUT_PROTOCOL = void;
struct EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL;
using EFI_RUNTIME_SERVICES = void;

typedef EFI_STATUS (EFIAPI* EFI_TEXT_STRING) (
    EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL *This,
    const wchar_t* String
);

struct EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL {
    size_t Reset;
    EFI_TEXT_STRING OutputString;
    size_t TestString;
    size_t QueryMode;
    size_t SetMode;
    size_t SetAttribute;
    size_t ClearScreen;
    size_t SetCursorPosition;
    size_t EnableCursor;
    void* Mode;
};

enum class EFI_ALLOCATE_TYPE {
    AllocateAnyPages,
    AllocateMaxAddress,
    AllocateAddress,
    MaxAllocateType
};
using enum EFI_ALLOCATE_TYPE;

enum class EFI_MEMORY_TYPE : uint32_t {
    EfiReservedMemoryType,
    EfiLoaderCode,
    EfiLoaderData,
    EfiBootServicesCode,
    EfiBootServicesData,
    EfiRuntimeServicesCode,
    EfiRuntimeServicesData,
    EfiConventionalMemory,
    EfiUnusableMemory,
    EfiACPIReclaimMemory,
    EfiACPIMemoryNVS,
    EfiMemoryMappedIO,
    EfiMemoryMappedIOPortSpace,
    EfiPalCode,
    EfiPersistentMemory,
    EfiUnacceptedMemoryType,
    EfiMaxMemoryType
};
using enum EFI_MEMORY_TYPE;


struct EFI_MEMORY_DESCRIPTOR {
    EFI_MEMORY_TYPE Type;
    EFI_PHYSICAL_ADDRESS PhysicalStart;
    EFI_VIRTUAL_ADDRESS VirtualStart;
    uint64_t NumberOfPages;
    uint64_t Attribute;
};

struct EFI_GUID {
    uint32_t block0;
    uint16_t block1;
    uint16_t block2;
    uint8_t  block3[8];

    bool operator==(const EFI_GUID& other) const = default;
};

// Spec: https://uefi.org/sites/default/files/resources/UEFI_Spec_2_10_Aug29.pdf#subsection.7.2.1
typedef EFI_STATUS (EFIAPI* EFI_ALLOCATE_PAGES) (
    IN EFI_ALLOCATE_TYPE Type,
    IN EFI_MEMORY_TYPE MemoryType,
    IN size_t Pages,
    IN OUT EFI_PHYSICAL_ADDRESS* Memory
);

// Spec: https://uefi.org/sites/default/files/resources/UEFI_Spec_2_10_Aug29.pdf#subsection.7.2.2
typedef EFI_STATUS (EFIAPI* EFI_FREE_PAGES) (
    IN EFI_PHYSICAL_ADDRESS Memory,
    IN size_t Pages
);

// Spec: https://uefi.org/sites/default/files/resources/UEFI_Spec_2_10_Aug29.pdf#subsection.7.2.3
typedef EFI_STATUS (EFIAPI* EFI_GET_MEMORY_MAP) (
    IN OUT size_t* MemoryMapSize,
    OUT EFI_MEMORY_DESCRIPTOR* MemoryMap,
    OUT size_t* MapKey,
    OUT size_t* DescriptorSize,
    OUT uint32_t* DescriptorVersion
);

// Spec: https://uefi.org/sites/default/files/resources/UEFI_Spec_2_10_Aug29.pdf#subsection.7.2.4
typedef EFI_STATUS (EFIAPI* EFI_ALLOCATE_POOL) (
    IN EFI_MEMORY_TYPE PoolType,
    IN size_t Size,
    OUT void** Buffer
);

// Spec: https://uefi.org/sites/default/files/resources/UEFI_Spec_2_10_Aug29.pdf#subsection.7.2.5
typedef EFI_STATUS (EFIAPI* EFI_FREE_POOL) (
    IN void* Buffer
);

// Spec: https://uefi.org/sites/default/files/resources/UEFI_Spec_2_10_Aug29.pdf#subsection.7.3.7
typedef EFI_STATUS (EFIAPI* EFI_HANDLE_PROTOCOL) (
    IN EFI_HANDLE Handle,
    IN const EFI_GUID* Protocol,
    OUT void** Interface
);

// Spec: https://uefi.org/sites/default/files/resources/UEFI_Spec_2_10_Aug29.pdf#subsection.7.3.9
typedef EFI_STATUS (EFIAPI* EFI_OPEN_PROTOCOL) (
    IN EFI_HANDLE Handle,
    IN const EFI_GUID* Protocol,
    OUT void** Interface,
    IN EFI_HANDLE AgentHandle,
    IN EFI_HANDLE ControllerHandle,
    IN uint32_t Attributes
);

// Argument list contains pairs of protocol GUIDs and protocol interfaces
// Spec: https://uefi.org/sites/default/files/resources/UEFI_Spec_2_10_Aug29.pdf#subsection.7.3.17
typedef EFI_STATUS (EFIAPI* EFI_INSTALL_MULTIPLE_PROTOCOL_INTERFACES) (
    IN OUT EFI_HANDLE* Handle,
    ...
);

// Spec: https://uefi.org/sites/default/files/resources/UEFI_Spec_2_10_Aug29.pdf#subsection.7.3.16
typedef EFI_STATUS (EFIAPI* EFI_LOCATE_PROTOCOL) (
    IN const EFI_GUID* Protocol,
    IN void* Registration,
    OUT void** Interface
);

enum class EFI_LOCATE_SEARCH_TYPE {
    AllHandles,
    ByRegisterNotify,
    ByProtocol
};

typedef EFI_STATUS (EFIAPI* EFI_LOCATE_HANDLE) (
    IN EFI_LOCATE_SEARCH_TYPE SearchType,
    IN EFI_GUID* Protocol,
    IN void* SearchKey,
    IN OUT size_t* BufferSize,
    OUT EFI_HANDLE* Buffer
);

typedef EFI_STATUS (EFIAPI* EFI_EXIT_BOOT_SERVICES) (
    IN EFI_HANDLE ImageHandle,
    IN size_t MapKey
);

struct EFI_BOOT_SERVICES {
    EFI_TABLE_HEADER Hdr;

    size_t RaiseTPL;
    size_t RestoreTPL;

    EFI_ALLOCATE_PAGES AllocatePages;
    EFI_FREE_PAGES FreePages;
    EFI_GET_MEMORY_MAP GetMemoryMap;
    EFI_ALLOCATE_POOL AllocatePool;
    EFI_FREE_POOL FreePool;

    size_t CreateEvent;
    size_t SetTimer;
    size_t WaitForEvent;
    size_t SignalEvent;
    size_t CloseEvent;
    size_t CheckEvent;

    size_t InstallProtocolInterface;
    size_t ReinstallProtocolInterface;
    size_t UninstallProtocolInterface;
    EFI_HANDLE_PROTOCOL HandleProtocol;
    size_t Reserved;
    size_t RegisterProtocolNotify;
    EFI_LOCATE_HANDLE LocateHandle;
    size_t LocateDevicePath;
    size_t InstallConfigurationTable;

    size_t LoadImage;
    size_t StartImage;
    size_t Exit;
    size_t UnloadImage;
    EFI_EXIT_BOOT_SERVICES ExitBootServices;

    size_t GetNextMonotonicCount;
    size_t Stall;
    size_t SetWatchdogTimer;

    size_t ConnectController;
    size_t DisconnectController;

    EFI_OPEN_PROTOCOL OpenProtocol;
    size_t CloseProtocol;
    size_t OpenProtocolInformation;

    size_t ProtocolsPerHandle;
    size_t LocateHandleBuffer;
    EFI_LOCATE_PROTOCOL LocateProtocol;
    EFI_INSTALL_MULTIPLE_PROTOCOL_INTERFACES InstallMultipleProtocolInterfaces;
    size_t UninstallMultipleProtocolInterfaces;

    size_t CalculateCrc32;

    size_t CopyMem;
    size_t SetMem;
    size_t CreateEventEx;
};

struct EFI_CONFIGURATION_TABLE {
    EFI_GUID VendorGuid;
    void* VendorTable;
};

struct EFI_SYSTEM_TABLE {
    EFI_TABLE_HEADER Hdr;
    wchar_t* FirmwareVendor;
    uint32_t FirmwareRevision;

    EFI_HANDLE ConsoleInHandle;
    EFI_SIMPLE_TEXT_INPUT_PROTOCOL* ConIn;

    EFI_HANDLE ConsoleOutHandle;
    EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL* ConOut;

    EFI_HANDLE StandardErrorHandle;
    EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL* StdErr;

    EFI_RUNTIME_SERVICES* RuntimeServices;
    EFI_BOOT_SERVICES* BootServices;

    size_t NumberOfTableEntries;
    EFI_CONFIGURATION_TABLE* ConfigurationTable;
};

constexpr EFI_STATUS EFI_SUCCESS = 0;
constexpr EFI_STATUS MakeEfiError(const uint64_t value) {
    return (1ull << 63ull) | value;
}
constexpr EFI_STATUS EFI_INVALID_PARAMETER = MakeEfiError(2);
constexpr EFI_STATUS EFI_BUFFER_TOO_SMALL  = MakeEfiError(5);

// Spec: https://uefi.org/sites/default/files/resources/UEFI_Spec_2_10_Aug29.pdf#subsubsection.4.6.1.1
constexpr EFI_GUID EFI_ACPI_TABLE_GUID = {
    0x8868e871, 0xe4f1, 0x11d3,
    { 0xbc,0x22,0x00,0x80,0xc7,0x3c,0x88,0x81 }
};
// Spec: https://uefi.org/sites/default/files/resources/UEFI_Spec_2_10_Aug29.pdf#subsubsection.4.6.1.1
constexpr EFI_GUID EFI_OLD_ACPI_TABLE_GUID = {
    0xeb9d2d30,0x2d88,0x11d3,
    { 0x9a,0x16,0x00,0x90,0x27,0x3f,0xc1,0x4d }
};
// Spec: https://uefi.org/sites/default/files/resources/UEFI_Spec_2_10_Aug29.pdf#subsubsection.4.6.1.1
constexpr EFI_GUID EFI_SMBIOS_TABLE_GUID = {
    0xeb9d2d31,0x2d88,0x11d3,
    { 0x9a,0x16,0x00,0x90,0x27,0x3f,0xc1,0x4d }
};
// Spec: https://uefi.org/sites/default/files/resources/UEFI_Spec_2_10_Aug29.pdf#subsection.4.6.4
constexpr EFI_GUID EFI_MEMORY_ATTRIBUTES_TABLE_GUID = {
    0xdcfa911d, 0x26eb, 0x469f,
    { 0xa2, 0x20, 0x38, 0xb7, 0xdc, 0x46, 0x12, 0x20 }
};
// Spec: https://uefi.org/sites/default/files/resources/UEFI_Spec_2_10_Aug29.pdf#subsection.18.4.3
constexpr EFI_GUID EFI_DEBUG_IMAGE_INFO_TABLE_GUID = {
    0x49152E77,0x1ADA,0x4764,
    { 0xB7,0xA2,0x7A,0xFE,0xFE,0xD9,0x5E,0x8B }
};
// Spec: https://github.com/tianocore/edk/blob/master/Foundation/Framework/Guid/Hob/Hob.h
constexpr EFI_GUID EFI_HOB_LIST_GUID = {
    0x7739f24c, 0x93d7, 0x11d4,
    { 0x9a, 0x3a, 0x0, 0x90, 0x27, 0x3f, 0xc1, 0x4d }
};
// Spec: https://android.googlesource.com/device/linaro/hikey/+/82c8071/uefi/linaro-edk2/EdkCompatibilityPkg/Foundation/Framework/Guid/DxeServices/DxeServices.h
constexpr EFI_GUID EFI_DXE_SERVICES_TABLE_GUID = {
    0x5ad34ba, 0x6f02, 0x4214,
    { 0x95, 0x2e, 0x4d, 0xa0, 0x39, 0x8e, 0x2b, 0xb9 }
};
// Spec: https://github.com/tianocore/edk2/blob/master/MdeModulePkg/Include/Guid/MemoryTypeInformation.h
constexpr EFI_GUID EFI_MEMORY_TYPE_INFORMATION_GUID = {
    0x4c19049f,0x4137,0x4dd3,
    { 0x9c,0x10,0x8b,0x97,0xa8,0x3f,0xfd,0xfa }
};

// This GUID seems to be defined in the EDK2-archive git repository
constexpr EFI_GUID LZMA_COMPRESSED_GUID = {
    0xee4e5898,0x3914,0x4259,
    { 0x9d,0x6e,0xdc,0x7b,0xd7,0x94,0x03,0xcf }
};

// The u-boot project seems to know this GUID, but I don't know much else
constexpr EFI_GUID MEM_STATUS_CODE_REC_GUID = {
    0x060cc026,0x4c0d,0x4dda,
    { 0x8f,0x41,0x59,0x5f,0xef,0x00,0xa5,0x02 }
};

constexpr EFI_GUID SMBIOS3_TABLE_GUID = {
    0xf2fd1544,0x9794,0x4a2c,
    { 0x99,0x2e,0xe5,0xbb,0xcf,0x20,0xe3,0x94 }
};

// Spec: https://uefi.org/sites/default/files/resources/UEFI_Spec_2_10_Aug29.pdf#subsection.30.3.2
constexpr EFI_GUID EFI_MTFTP4_PROTOCOL_GUID = {
    0x78247c57,0x63db,0x4708,
    { 0x99,0xc2,0xa8,0xb4,0xa9,0xa6,0x1f,0x6b }
};

struct EFI_MTFTP4_PROTOCOL;
struct EFI_MTFTP4_TOKEN;
struct EFI_IPv4_ADDRESS {
    uint8_t buffer[4];
};
struct EFI_MTFTP4_OVERRIDE_DATA {
    EFI_IPv4_ADDRESS GatewayIp;
    EFI_IPv4_ADDRESS ServerIp;
    uint16_t ServerPort;
    uint16_t TryCount;
    uint16_t TimeoutValue;
};
struct EFI_MTFTP4_OPTION {
    uint8_t* OptionStr;
    uint8_t* ValueStr;
};
struct EFI_MTFTP4_REQ_HEADER {
    uint16_t OpCode;
    uint8_t Filename[1];
} __attribute__((packed));
struct EFI_MTFTP4_OACK_HEADER {
    uint16_t OpCode;
    uint8_t Data[1];
} __attribute__((packed));
struct EFI_MTFTP4_DATA_HEADER {
    uint16_t OpCode;
    uint16_t Block;
    uint8_t Data[1];
} __attribute__((packed));
struct EFI_MTFTP4_ACK_HEADER {
    uint16_t OpCode;
    uint16_t Block[1];
} __attribute__((packed));
// Reserved; should not be used
struct EFI_MTFTP4_DATA8_HEADER {
    uint16_t OpCode;
    uint64_t Block;
    uint8_t Data[1];
} __attribute__((packed));
// Reserved; should not be used
struct EFI_MTFTP4_ACK8_HEADER {
    uint16_t OpCode;
    uint64_t Block[1];
} __attribute__((packed));
struct EFI_MTFTP4_ERROR_HEADER {
    uint16_t OpCode;
    uint16_t ErrorCode;
    uint8_t ErrorMessage[1];
} __attribute__((packed));
union EFI_MTFTP4_PACKET {
    uint16_t OpCode;
    EFI_MTFTP4_REQ_HEADER Rrq, Wrq;
    EFI_MTFTP4_OACK_HEADER Oack;
    EFI_MTFTP4_DATA_HEADER Data;
    EFI_MTFTP4_ACK_HEADER Ack;
    // Reserved; should not be used
    EFI_MTFTP4_DATA8_HEADER Data8;
    // Reserved; should not be used
    EFI_MTFTP4_ACK8_HEADER Ack8;
    EFI_MTFTP4_ERROR_HEADER Error;
} __attribute__((packed));
typedef EFI_STATUS (EFIAPI* EFI_MTFTP4_CHECK_PACKET) (
    IN EFI_MTFTP4_PROTOCOL* This,
    IN EFI_MTFTP4_TOKEN* Token,
    IN uint16_t PacketLen,
    IN EFI_MTFTP4_PACKET* Packet
);
typedef EFI_STATUS (EFIAPI* EFI_MTFTP4_TIMEOUT_CALLBACK) (
    IN EFI_MTFTP4_PROTOCOL* This,
    IN EFI_MTFTP4_TOKEN* Token
);
typedef EFI_STATUS (EFIAPI* EFI_MTFTP4_PACKET_NEEDED) (
    IN EFI_MTFTP4_PROTOCOL* This,
    IN EFI_MTFTP4_TOKEN* Token,
    IN OUT uint16_t* Length,
    OUT void** Buffer
);
struct EFI_MTFTP4_TOKEN {
    EFI_STATUS Status;
    EFI_EVENT Event;
    EFI_MTFTP4_OVERRIDE_DATA* OverrideData;
    const char8_t* Filename;
    const char8_t* ModeStr;
    uint32_t OptionCount;
    EFI_MTFTP4_OPTION* OptionList;
    uint64_t BufferSize;
    void* Buffer;
    void* Context;
    EFI_MTFTP4_CHECK_PACKET CheckPacket;
    EFI_MTFTP4_TIMEOUT_CALLBACK TimeoutCallback;
    EFI_MTFTP4_PACKET_NEEDED PackedNeeded;
};
typedef EFI_STATUS (EFIAPI* EFI_MTFTP4_READ_FILE) (
    IN EFI_MTFTP4_PROTOCOL* This,
    IN EFI_MTFTP4_TOKEN* Token
);
struct EFI_MTFTP4_CONFIG_DATA {
    bool UseDefaultSetting;
    EFI_IPv4_ADDRESS StationIp;
    EFI_IPv4_ADDRESS SubnetMask;
    uint16_t LocalPort;
    EFI_IPv4_ADDRESS GatewayIp;
    EFI_IPv4_ADDRESS ServerIp;
    uint16_t InitialServerPort;
    uint16_t TryCount;
    uint16_t TimeoutValue;
};
typedef EFI_STATUS (EFIAPI* EFI_MTFTP4_CONFIGURE) (
    IN EFI_MTFTP4_PROTOCOL* This,
    IN EFI_MTFTP4_CONFIG_DATA* MtftpConfigData
);
struct EFI_MTFTP4_MODE_DATA {
    EFI_MTFTP4_CONFIG_DATA ConfigData;
    uint8_t SupportedOptionCount;
    uint8_t** SupportedOptions;
    uint8_t UnsupportedOptionCount;
    uint8_t** UnsupportedOptions;
};
typedef EFI_STATUS (EFIAPI* EFI_MTFTP4_GET_MODE_DATA) (
    IN EFI_MTFTP4_PROTOCOL* This,
    OUT EFI_MTFTP4_MODE_DATA* ModeData
);
typedef EFI_STATUS (EFIAPI* EFI_MTFTP4_GET_INFO) (
    IN EFI_MTFTP4_PROTOCOL* This,
    IN EFI_MTFTP4_OVERRIDE_DATA* OverrideData,
    IN const char8_t* Filename,
    IN const char8_t* ModeStr,
    IN uint8_t OptionCount,
    IN EFI_MTFTP4_OPTION* OptionList,
    OUT uint32_t* PacketLength,
    OUT EFI_MTFTP4_PACKET** Packet
);
struct EFI_MTFTP4_PROTOCOL {
    EFI_MTFTP4_GET_MODE_DATA GetModeData;
    EFI_MTFTP4_CONFIGURE Configure;
    EFI_MTFTP4_GET_INFO GetInfo;
    size_t ParseOptions;
    EFI_MTFTP4_READ_FILE ReadFile;
    size_t WriteFile;
    size_t ReadDirectory;
    size_t Poll;
};

constexpr EFI_GUID EFI_GRAPHICS_OUTPUT_PROTOCOL_GUID = {
    0x9042a9de,0x23dc,0x4a38,
    { 0x96,0xfb,0x7a,0xde,0xd0,0x80,0x51,0x6a }
};
struct EFI_GRAPHICS_OUTPUT_PROTOCOL {
    size_t QueryMode;
    size_t SetMode;
    size_t Blt;
    size_t Mode;
};
