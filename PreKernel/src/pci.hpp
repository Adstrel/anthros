#pragma once
#include "common.hpp"
#include "acpi.hpp"


// PCI specifications are not freely distributed, so this osdev article
//     will have to do as reference: https://wiki.osdev.org/PCI
namespace PCI {

struct Header {
    const uint16_t VendorID;
    const uint16_t DeviceID;

    uint16_t Command;
    uint16_t Status;

    const uint8_t RevisionID;
    // Programming Interface Byte
    const uint8_t ProgIF;
    const uint8_t Subclass;
    const uint8_t ClassCode;

    uint8_t CacheLineSize;
    uint8_t LatencyTimer;
    const uint8_t HeaderType : 7;
    const uint8_t MultipleFunctions : 1;
    uint8_t BIST;
} __attribute__((packed));

struct HeaderType_0 {
    Header common;
    uint32_t BAR0;
    uint32_t BAR1;
    uint32_t BAR2;
    uint32_t BAR3;
    uint32_t BAR4;
    uint32_t BAR5;
    uint32_t CardbusCisPointer;
    const uint16_t SubsystemVendorID;
    const uint16_t SubsystemID;
    uint32_t ExpansionRomBaseAddress;
    uint8_t CapabilitiesPointer;
    const uint8_t Reserved[7];
    uint8_t InterruptLine;
    uint8_t InterruptPIN;
    uint8_t MinGrant;
    uint8_t MaxLatency;
} __attribute__((packed));

void enumerate_all_ranges(MCFG* mcfg);

} // namespace PCI
