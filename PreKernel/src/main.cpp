#include "efi.hpp"
#include "common.hpp"
#include "alloc.hpp"
#include "print.hpp"
#include "acpi.hpp"
#include "pci.hpp"
#include "pe.hpp"
#include "paging.hpp"
#include "memorymap.hpp"
#include "efi_helpers.hpp"


extern "C" EFI_STATUS EFIAPI efi_main(EFI_HANDLE image_handle, EFI_SYSTEM_TABLE* system_table) {
    // The system_table structure holds function pointers to EFI services; we put it in a
    //     global variable so we don't need to pass the pointer everywhere
    efi_init_global_state(system_table);
    print("Hello, World!\r\n");

    // We don't need an early memory map, but I want to print out the entries to the screen
    auto memory_map = MemoryMap::GetNew();
    print("Early memory map needed buffer size: {} bytes\r\n", memory_map.buffer_byte_size);
    // dump_memory_map(memory_map);

    // Walk the Configuration Table, dump it, and find important pointers we need
    auto efi_entries = walk_efi_table_entries();

    // Print out info from the found ACPI structures
    auto acpi_tables = walk_acpi(efi_entries.rsdp);

    // Use info from MCFG to enumerate PCI ranges
    PCI::enumerate_all_ranges(acpi_tables.mcfg);

    // We now want to download the kernel binary, for that we need to fetch the MTFTP protocol handle
    EFI_MTFTP4_PROTOCOL* protocol = get_efi_mtftp4_protocol();

    // Download the kernel and put it into an allocated buffer
    size_t kernel_filesize = 0;
    uint8_t* kernel_file_buffer = efi_mtftp4_protocol_get_file(protocol, u8"kernel.exe", &kernel_filesize);

    // Parse the downloaded kernel as a PE executable image
    auto kernel_file_info = PE::parse_executable(kernel_file_buffer, kernel_filesize);

    print("\r\nWill now attempt to exit EFI Boot services, map the kernel, and jump into it. Whish me luck!\r\n");
    flush();

    EFI_STATUS exit_efi_ret;
    do {
        memory_map.Update();

        // ExitBootServices can fail if the UEFI MemoryMap has changed since we last got it,
        //     which can happen inside calls to UEFI services, or even interrupts
        exit_efi_ret = system_table->BootServices->ExitBootServices(image_handle, memory_map.key);
    } while (exit_efi_ret != EFI_SUCCESS);
    efi_cleanup_global_state();

    // Lets map our kernel + 1 page of stack + 32 free bootstrap pages into one big block
    const size_t needed_block_pages = kernel_file_info.needed_pages + 1 + 32;
    print("Need {} free page{}.\n", needed_block_pages, needed_block_pages != 1 ? "s" : "");

    // Find a block of pages big enough in the given memory map
    size_t begin_phys_addr = 0;
    bool found = false;
    for (const auto& mem_desc : memory_map) {
        if (mem_desc.Type != EFI_MEMORY_TYPE::EfiConventionalMemory) {
            print("Skipping {} free page{} of wrong type at address 0x{x}\n",
                mem_desc.NumberOfPages,
                mem_desc.NumberOfPages != 1 ? "s" : "",
                mem_desc.PhysicalStart
            );
            continue;
        }

        if (mem_desc.NumberOfPages < needed_block_pages) {
            print("Skipping {} free page{} at address 0x{x}\n",
                mem_desc.NumberOfPages,
                mem_desc.NumberOfPages != 1 ? "s" : "",
                mem_desc.PhysicalStart
            );
            continue;
        }

        print("Found block of {} free page{} at address 0x{x}!\n",
            mem_desc.NumberOfPages,
            mem_desc.NumberOfPages != 1 ? "s" : "",
            mem_desc.PhysicalStart
        );
        begin_phys_addr = mem_desc.PhysicalStart;
        found = true;
        break;
    }

    if (!found) {
        print("Early panic: Couldn't find a block of memory big enough to map the kernel!");
        while (true);
    }

    // Get address of current page table
    // TODO: I'm not sure if we can relly on the Kernel's virtual address space to be empty in our borrowed page table.
    //       We might need to create a blank one for the Kernel.
    //       This also means properly tracking the memory map we pass on, the GDT, IDT, and all other borrowed structures and data.
    size_t page_table_phys;
    {
        size_t cr3;
        asm ("mov %%cr3, %0" : "=r"(cr3));
        page_table_phys = cr3 & (~0xfff);
    }

    get_phys_page_init(begin_phys_addr, kernel_file_info.needed_pages + 1 /* The kernel stack page */);

    // Copy the level 4 page table, bc the old one might be in read only memory
    size_t new_page_table = get_phys_page();
    for (size_t i = 0; i < 512; i++) {
        reinterpret_cast<uint64_t*>(new_page_table)[i] = reinterpret_cast<uint64_t*>(page_table_phys)[i];
    }
    asm volatile("mov %0, %%cr3" : : "r"(new_page_table));
    page_table_phys = new_page_table;

    // Copy all the sections
    volatile uint8_t* mm_data = reinterpret_cast<volatile uint8_t*>(begin_phys_addr);
    size_t mm_offset = 0;
    for (size_t section = 0; section < kernel_file_info.coff_header->NumberOfSections; section++) {
        const PE::SectionHeader* pe_section = kernel_file_info.section_table + section;

        // PE has only 32-bit virtual addresses, but we want to put our kernel into the higher half
        //     of the virtual address space
        const uint64_t base_section_va = pe_section->VirtualAddress + 0xffff'ffff'0000'0000;

        // The supplied `SizeOfRawData` and `VirtualSize` need to be padded to a `SectionAlignment` boundary.
        // Exact value of `SectionAlignment` is specified in the optional header, but we just presume it is 4096.
        const size_t full_virtual_size =
            pe_section->VirtualSize % 0x1000 == 0 ?
            pe_section->VirtualSize :
            pe_section->VirtualSize + (0x1000 - pe_section->VirtualSize % 0x1000);

        // Map pages for this section
        for (size_t offset = 0; offset < full_virtual_size; offset += 0x1000) {
            map_pa_to_va(
                page_table_phys,
                begin_phys_addr + mm_offset + offset,
                base_section_va + offset,
                !pe_section->Characteristics.MEM_WRITE,
                pe_section->Characteristics.MEM_EXECUTE
            );

            // Flush the TLB
            // TODO: Do we need to flush each page one by one? Can we maybe flush a range of pages at once?
            //       Or maybe just writing to cr3 would be better (i.e. flush all pages)
            asm volatile("invlpg (%0)" : : "r"(base_section_va + offset));
        }

        const uint8_t* src_data = kernel_file_buffer + pe_section->PointerToRawData;
        size_t section_offset = 0;

        // Copy initialized data
        for (; section_offset < pe_section->SizeOfRawData; section_offset++) {
            mm_data[mm_offset + section_offset] = src_data[section_offset];
        }

        // Zero initialize the rest of the section
        for (;section_offset < full_virtual_size; section_offset++) {
            mm_data[mm_offset + section_offset] = 0;
        }

        mm_offset += section_offset;
    }

    // Map the kernel stack at a predetermined address (it is just one page long)
    constexpr const uint64_t kernel_stack_va = 0xffff'ffff'7fff'8000;
    map_pa_to_va(
        page_table_phys,
        begin_phys_addr + 4096 * kernel_file_info.needed_pages, // We reserved this page for the stack earlier
        kernel_stack_va,
        false,
        false
    );
    // Flush the TLB
    asm volatile("invlpg (%0)" : : "r"(kernel_stack_va));

    // We just copied code, and we will want to jump into it next.
    // This is considered self-modifying code, so we need to execute any serializing instruction before we try jumping into it.
    // TODO: Is this actually needed? Can we guarantee not needing this? I don't think so; I think this has to stay
    asm volatile("cpuid" : : : "eax", "ebx", "ecx", "edx");

    const uint64_t higher_half_entry_point = 0xffff'ffff'0000'0000 +
        kernel_file_info.optional_header->StandardFields.AddressOfEntryPoint;

    print("RSDP: 0x{x}\n", reinterpret_cast<uint64_t>(efi_entries.rsdp));
    print("Jumping to address: 0x{x}\n", higher_half_entry_point);
    print("EFI entry point addr: 0x{x}\n", reinterpret_cast<uint64_t>(efi_main));

    const register uint64_t rdi asm("rdi") = reinterpret_cast<uint64_t>(efi_entries.rsdp);
    const register uint64_t rsi asm("rsi") = reinterpret_cast<uint64_t>(memory_map.buffer);
    const register uint64_t rdx asm("rdx") = memory_map.buffer_byte_size / memory_map.descriptor_size;
    const register uint64_t rcx asm("rcx") = memory_map.descriptor_size;
    const register uint64_t r8  asm("r8")  = begin_phys_addr;
    const register uint64_t r9  asm("r9")  = g_allocated_pages;
    asm volatile (
        "mov %[kernel_stack_top], %%rsp;"
        "mov %[kernel_stack_top], %%rbp;"
        "pushq $0;" // Return address
        "jmp *%[entry_point];"
        : :
        // Virtual address of Kernel Stack top
        [kernel_stack_top]"n"(kernel_stack_va + 0x1000),
        // Entry point address
        [entry_point]"r"(higher_half_entry_point),
        // Entry point arguments
        [arg0]"r"(rdi),
        [arg1]"r"(rsi),
        [arg2]"r"(rdx),
        [arg3]"r"(rcx),
        [arg4]"r"(r8),
        [arg5]"r"(r9)
    );
    __builtin_unreachable();
}

