#pragma once
#include "common.hpp"


constexpr uint32_t ascii_to_uint32_t(const char8_t (&str)[5]) {
    // Last byte is '\0'
    return (static_cast<uint32_t>(str[0]) << (0 * 8)) |
           (static_cast<uint32_t>(str[1]) << (1 * 8)) |
           (static_cast<uint32_t>(str[2]) << (2 * 8)) |
           (static_cast<uint32_t>(str[3]) << (3 * 8));
}

constexpr uint64_t ascii_to_uint64_t(const char8_t (&str)[9]) {
    // Last byte is '\0'
    return (static_cast<uint64_t>(str[0]) << (0 * 8)) |
           (static_cast<uint64_t>(str[1]) << (1 * 8)) |
           (static_cast<uint64_t>(str[2]) << (2 * 8)) |
           (static_cast<uint64_t>(str[3]) << (3 * 8)) |
           (static_cast<uint64_t>(str[4]) << (4 * 8)) |
           (static_cast<uint64_t>(str[5]) << (5 * 8)) |
           (static_cast<uint64_t>(str[6]) << (6 * 8)) |
           (static_cast<uint64_t>(str[7]) << (7 * 8));
}

// Root System Description Pointer Structure
// Spec: https://uefi.org/sites/default/files/resources/ACPI_Spec_6_5_Aug29.pdf#subsubsection.5.2.5.3
struct RSDP {
    // Should equal string "RSD PTR "
    uint64_t Signature;
    // The sum of the first 20 bytes of this structure should equal to zero
    uint8_t Checksum;
    // OEM-supplied string that identifies the OEM
    char8_t OEMID[6];
    // If Revision is less than 2, this structure is only valid up to the RsdtAddress field
    uint8_t Revision;
    // 32-bit physical address of the RSDT
    uint32_t RsdtAddress;
    // Length of this entire table structure
    uint32_t Length;
    // 64-bit physical address of the XSDT
    uint64_t XsdtAddress;
    // Checksum for the entire table, including both checksum fields
    uint8_t ExtendedChecksum;
    uint8_t Reserved[3];
} __attribute__((packed));

// ACPI System Descriptor Table Header
struct SDTHeader {
    uint32_t Signature;
    uint32_t Length;
    uint8_t Revision;
    // From the beginning of the table, the next "Length" bytes should sum to 0
    uint8_t Checksum;
    char8_t OEMID[6];
    uint64_t OEMTableID;
    uint32_t OEMRevision;
    uint32_t CreatorID;
    uint32_t CreatorRevision;
} __attribute__((packed));

// Root System Description Table
// Right after the header there is a list of uint32_t Entries
// (how many depends on the value of header.Length)
// Spec: https://uefi.org/sites/default/files/resources/ACPI_Spec_6_5_Aug29.pdf#subsection.5.2.7
struct RSDT {
    SDTHeader header;
} __attribute__((packed));

// Extended System Description Table
// Right after the header there is a list of uint64_t Entries
// (how many depends on the value of header.Length)
// Spec: https://uefi.org/sites/default/files/resources/ACPI_Spec_6_5_Aug29.pdf#subsection.5.2.8
struct XSDT {
    SDTHeader header;
} __attribute__((packed));

struct InterruptControllerStructure {
    enum class Type : uint8_t {
        ProcessorLocalAPIC = 0,
        IO_APIC = 1,
        InterruptSourceOverride = 2,
        NonmaskableInterruptSource = 3,
        LocalApicNMI = 4,
        LocalApicAddressOverride = 5,
        IO_SAPIC = 6,
        LocalSAPIC = 7,
        PlatformInterruptSources = 8,
        ProcessorLocal_x2APIC = 9,
        Local_x2APIC_NMI = 0xa,
        GIC_CPU_Interface = 0xb,
        GIC_Distributor = 0xc,
        GIC_MSI_Frame = 0xd,
        GIC_Redistributor = 0xe,
        GIC_InterruptTranslationService = 0xf,
        MultiprocessorWakeup = 0x10,
        CoreProgrammableInterruptController = 0x11,
        LegacyIOProgrammableInterruptController = 0x12,
        HyperTransportProgrammableInterruptController = 0x13,
        ExtendIOProgrammableInterruprController = 0x14,
        MSIProgrammableInterruptController = 0x15,
        BridgeIOProgrammableInterruptController = 0x16,
        LowPinCountProgrammableInterruptController = 0x17
    } type;
    uint8_t Length;
    union Structure {
        // Total Length equals 8
        struct ProcessorLocalApic {
            uint8_t ACPI_ProcessorUID;
            uint8_t APIC_ID;
            struct Flags {
                uint8_t Enabled : 1;
                uint8_t OnlineCapable : 1;
                uint32_t Reserved : 30;
            } Flags;
        } __attribute__((packed)) ProcessorLocalApic;
        // Total Length equals 16
        struct ProcesorLocalx2Apic {
            uint8_t Reserved[2];
            uint32_t X2APIC_ID;
            struct Flags {
                uint8_t Enabled : 1;
                uint8_t OnlineCapable : 1;
                uint32_t Reserved : 30;
            } Flags;
            uint32_t ACPI_ProcessorUID;
        } __attribute__((packed)) ProcessorLocalx2Apic;
    } __attribute__((packed)) structure;
} __attribute__((packed));

// Multiple APIC Description Table
// header.Signature should equal "APIC"
// Right after the defined structure, there is a list of InterruptControllerStructures
// (how many depends on the value of header.Length)
// Spec: https://uefi.org/sites/default/files/resources/ACPI_Spec_6_5_Aug29.pdf#subsection.5.2.12
struct MADT {
    SDTHeader header;
    // 32-bit physical address at which each processor can access its local interrupt controller
    uint32_t LocalInterruptControllerAddress;
    struct Flags {
        uint8_t  PCAT_COMPAT : 1;
        uint32_t Reserved    : 31;
    } Flags;
} __attribute__((packed));

struct GenericAddressRegister {
    uint8_t AddressSpace;
    uint8_t BitWidth;
    uint8_t BitOffset;
    uint8_t AccessSize;
    uint64_t Address;
} __attribute__((packed));

// header.Signature should equal "FACP"
// Spec: https://uefi.org/sites/default/files/resources/ACPI_Spec_6_5_Aug29.pdf#subsection.5.2.9
struct FADT {
    SDTHeader header;
    // 32-bit physical address of the FACS structure
    uint32_t FIRMWARE_CTRL;
    // 32-bit physical address of the DSDT structure
    uint32_t DSDT;
    uint8_t Reserved0;
    uint8_t Preferred_PM_Profile;
    uint16_t SCI_INT;
    uint32_t SMI_CMD;
    uint8_t ACPI_ENABLE;
    uint8_t ACPI_DISABLE;
    uint8_t S4BIOS_REQ;
    uint8_t PSTATE_CNT;
    uint32_t PM1a_EVT_BLK;
    uint32_t PM1b_EVT_BLK;
    uint32_t PM1a_CNT_BLK;
    uint32_t PM1b_CNT_BLK;
    uint32_t PM2_CNT_BLK;
    uint32_t PM_TMR_BLK;
    uint32_t GPE0_BLK;
    uint32_t GPE1_BLK;
    uint8_t PM1_EVT_LEN;
    uint8_t PM1_CNT_LEN;
    uint8_t PM2_CNT_LEN;
    uint8_t PM_TMR_LEN;
    uint8_t GPE0_BLK_LEN;
    uint8_t GPE1_BLK_LEN;
    uint8_t GPE1_BASE;
    uint8_t CST_CNT;
    uint16_t P_LVL2_LAT;
    uint16_t P_LVL3_LAT;
    uint16_t FLUSH_SIZE;
    uint16_t FLUSH_STRIDE;
    uint8_t DUTY_OFFSET;
    uint8_t DUTY_WIDTH;
    uint8_t DAY_ALRM;
    uint8_t MON_ALRM;
    uint8_t CENTURY;
    uint16_t IAPC_BOOT_ARCH;
    // Must be 0
    uint8_t Reserved1;
    struct Flags {
        uint8_t WBINVD                      : 1;
        uint8_t WBINVD_FLUSH                : 1;
        uint8_t PROC_C1                     : 1;
        uint8_t P_LVL2_UP                   : 1;
        uint8_t PWD_BUTTON                  : 1;
        uint8_t SLP_BUTTON                  : 1;
        uint8_t FIX_RTC                     : 1;
        uint8_t RTC_S4                      : 1;
        uint8_t TMR_VAL_EXT                 : 1;
        uint8_t DCK_CAP                     : 1;
        uint8_t RESET_REG_SUP               : 1;
        uint8_t SEALED_CASE                 : 1;
        uint8_t HEADLESS                    : 1;
        uint8_t CPU_SW_SLP                  : 1;
        uint8_t PCI_EXP_WAK                 : 1;
        uint8_t USE_PLATFORM_CLOCK          : 1;
        uint8_t S4_RTC_STS_VALID            : 1;
        uint8_t REMOTE_POWER_ON_CAPABLE     : 1;
        uint8_t FORCE_APIC_CLUSTER_MODEL    : 1;
        uint8_t FORCE_APIC_PHYSICAL_DESTINATION_MODE : 1;
        uint8_t HW_REDUCED_ACPI             : 1;
        uint8_t LOW_POWER_S0_IDLE_CAPABLE   : 1;
        uint8_t PERSISTENT_CPU_CACHES       : 2;
        uint8_t Reserved : 8;
    } Flags;
    GenericAddressRegister RESET_REG;
    uint8_t RESET_VALUE;
    uint16_t ARM_BOOT_ARCH;
    uint8_t FADT_Minor_Version;
    uint64_t X_FIRMWARE_CTRL;
    uint64_t X_DSDT;
    GenericAddressRegister X_PM1a_EVT_BLK;
    GenericAddressRegister X_PM1b_EVT_BLK;
    GenericAddressRegister X_PM1a_CNT_BLK;
    GenericAddressRegister X_PM1b_CNT_BLK;
    GenericAddressRegister X_PM2_CNT_BLK;
    GenericAddressRegister X_PM_TMR_BLK;
    GenericAddressRegister X_GPE0_BLK;
    GenericAddressRegister X_GPE1_BLK;
    GenericAddressRegister SLEEP_CONTROL_REG;
    GenericAddressRegister SLEEP_STATUS_REG;
    char8_t HypervisorVendorIdentity[8];
} __attribute__((packed));

// Firmware ACPI Control Structure
// Signature should be "FACS"
// Spec: https://uefi.org/sites/default/files/resources/ACPI_Spec_6_5_Aug29.pdf#subsection.5.2.10
struct FACS {
    uint32_t Signature;
    uint32_t Length;
    uint32_t HardwareSignature;
    uint32_t FirmwareWakingVector;
    uint32_t GlobalLock;
    struct Flags {
        uint8_t S4BIOS_F : 1;
        uint8_t _64BIT_WAKE_SUPPORTED_F : 1;
        uint32_t Reserved : 30;
    } flags;
    uint64_t X_FirmwareWakingVector;
    uint8_t Version;
    uint8_t Reserved0[3];
    struct OSPMFlags {
        uint8_t _64BIT_WAKE_F : 1;
        uint32_t Reserved : 31;
    } OSPMFlags;
    uint8_t Reserved1[24];
} __attribute__((packed));

// Differentiated System Description Table
// Signature should be "DSDT"
// There is AML bytecode after the header. You can figure out how much from header.Length
// Spec: https://uefi.org/sites/default/files/resources/ACPI_Spec_6_5_Aug29.pdf#subsubsection.5.2.11.1
struct DSDT {
    SDTHeader header;
} __attribute__((packed));

// Configuration Space Base Address Allocation Structure
// Relevant to the "MCFG" ACPI table
struct CSBAAS {
    // Base Address of Enhanced Configuration Mechanism
    uint64_t base_address;
    uint16_t pci_segment_group_number;
    // Start PCI Bus number decoded by this host bridge
    uint8_t start_pci;
    // End PCI Bus number decoded by this host bridge
    uint8_t end_pci;
    uint32_t Reserved;
} __attribute__((packed));

// No idea what the name means
// After the reserved bytes there is a list of CSBAASs. You can get their count from header.Length
// Signature should be "MCFG"
// Not spec but has to do bc spec is not freely accessible: https://wiki.osdev.org/PCI_Express#Enhanced_Configuration_Mechanism
struct MCFG {
    SDTHeader header;
    uint8_t Reserved[8];
} __attribute__((packed));


struct AcpiTables {
    RSDP* rsdp;
    XSDT* xsdt;
    FADT* fadt;
    MCFG* mcfg;
    FACS* facs;
    DSDT* dsdt;
};


void walk_rsdp(const RSDP* rsdp, OUT XSDT** xsdt);
void walk_xsdt(XSDT* xsdt, OUT FADT** fadt, OUT MCFG** mcfg);
void walk_fadt(FADT* fadt, OUT FACS** facs, OUT DSDT** dsdt);
AcpiTables walk_acpi(RSDP* rsdp);
