#pragma once
#include "common.hpp"

namespace ACPI {

// Root System Description Pointer Structure
// Spec: https://uefi.org/sites/default/files/resources/ACPI_Spec_6_5_Aug29.pdf#subsubsection.5.2.5.3
struct RSDP {
    // Should equal string "RSD PTR "
    u64 Signature;
    // The sum of the first 20 bytes of this structure should equal to zero
    u8 Checksum;
    // OEM-supplied string that identifies the OEM
    u8 OEMID[6];
    // If Revision is less than 2, this structure is only valid up to the RsdtAddress field
    u8 Revision;
    // 32-bit physical address of the RSDT
    u32 RsdtAddress;
    // Length of this entire table structure
    u32 Length;
    // 64-bit physical address of the XSDT
    u64 XsdtAddress;
    // Checksum for the entire table, including both checksum fields
    u8 ExtendedChecksum;
    u8 Reserved[3];
} __attribute__((packed));

// ACPI System Descriptor Table Header
struct SDTHeader {
    u32 Signature;
    u32 Length;
    u8 Revision;
    // From the beginning of the table, the next "Length" bytes should sum to 0
    u8 Checksum;
    u8 OEMID[6];
    u64 OEMTableID;
    u32 OEMRevision;
    u32 CreatorID;
    u32 CreatorRevision;
} __attribute__((packed));

// Root System Description Table
// Right after the header there is a list of uint32_t Entries
// (how many depends on the value of header.Length)
// Spec: https://uefi.org/sites/default/files/resources/ACPI_Spec_6_5_Aug29.pdf#subsection.5.2.7
struct RSDT {
    SDTHeader header;
} __attribute__((packed));

// Extended System Description Table
// Right after the header there is a list of uint64_t Entries
// (how many depends on the value of header.Length)
// Spec: https://uefi.org/sites/default/files/resources/ACPI_Spec_6_5_Aug29.pdf#subsection.5.2.8
struct XSDT {
    SDTHeader header;
} __attribute__((packed));

struct InterruptControllerStructure {
    enum class Type : u8 {
        ProcessorLocalAPIC = 0,
        IO_APIC = 1,
        InterruptSourceOverride = 2,
        NonmaskableInterruptSource = 3,
        LocalApicNMI = 4,
        LocalApicAddressOverride = 5,
        IO_SAPIC = 6,
        LocalSAPIC = 7,
        PlatformInterruptSources = 8,
        ProcessorLocal_x2APIC = 9,
        Local_x2APIC_NMI = 0xa,
        GIC_CPU_Interface = 0xb,
        GIC_Distributor = 0xc,
        GIC_MSI_Frame = 0xd,
        GIC_Redistributor = 0xe,
        GIC_InterruptTranslationService = 0xf,
        MultiprocessorWakeup = 0x10,
        CoreProgrammableInterruptController = 0x11,
        LegacyIOProgrammableInterruptController = 0x12,
        HyperTransportProgrammableInterruptController = 0x13,
        ExtendIOProgrammableInterruprController = 0x14,
        MSIProgrammableInterruptController = 0x15,
        BridgeIOProgrammableInterruptController = 0x16,
        LowPinCountProgrammableInterruptController = 0x17
    } type;
    u8 Length;

    union Structure {
        // Total Length equals 8
        struct ProcessorLocalApic {
            u8 ACPI_ProcessorUID;
            u8 APIC_ID;
            struct Flags {
                u8 Enabled : 1;
                u8 OnlineCapable : 1;
                u32 Reserved : 30;
            } Flags;
        } __attribute__((packed)) ProcessorLocalApic;

        // Total Length equals 16
        struct ProcesorLocalx2Apic {
            u8 Reserved[2];
            u32 X2APIC_ID;
            struct Flags {
                u8 Enabled : 1;
                u8 OnlineCapable : 1;
                u32 Reserved : 30;
            } Flags;
            u32 ACPI_ProcessorUID;
        } __attribute__((packed)) ProcessorLocalx2Apic;

        // Total Length equals 12
        struct IO_APIC {
            u8 IO_APIC_ID;
            u8 Reserved;
            u32 IO_APIC_Address;
            u32 GlobalSystemInterruptBase;
        } __attribute__((packed)) IO_APIC;

        // Total Length equals 10
        struct InterruptSourceOverride {
            u8 Bus;
            u8 Source;
            u32 GlobalSystemInterrupt;
            struct Flags {
                u8 Polarity : 2;
                u8 TriggerMode : 2;
                u16 Reserved : 12;
            } Flags;
        } __attribute__((packed)) InterruptSourceOverride;

        // Total Length equals 6
        struct LocalApicNMI {
            u8 ACPI_ProcessorUID;
            struct Flags {
                u8 Polarity : 2;
                u8 TriggerMode : 2;
                u16 Reserved : 12;
            } Flags;
            u8 LocalAPIC_Lint_n;
        } __attribute__((packed)) LocalApicNMI;
    } __attribute__((packed)) structure;
} __attribute__((packed));

// Multiple APIC Description Table
// header.Signature should equal "APIC"
// Right after the defined structure, there is a list of InterruptControllerStructures
// (how many depends on the value of header.Length)
// Spec: https://uefi.org/sites/default/files/resources/ACPI_Spec_6_5_Aug29.pdf#subsection.5.2.12
struct MADT {
    SDTHeader header;
    // 32-bit physical address at which each processor can access its local interrupt controller
    u32 LocalInterruptControllerAddress;
    struct Flags {
        u8  PCAT_COMPAT : 1;
        u32 Reserved    : 31;
    } Flags;
} __attribute__((packed));

struct GenericAddressRegister {
    u8 AddressSpace;
    u8 BitWidth;
    u8 BitOffset;
    u8 AccessSize;
    u64 Address;
} __attribute__((packed));

// header.Signature should equal "FACP"
// Spec: https://uefi.org/sites/default/files/resources/ACPI_Spec_6_5_Aug29.pdf#subsection.5.2.9
struct FADT {
    SDTHeader header;
    // 32-bit physical address of the FACS structure
    u32 FIRMWARE_CTRL;
    // 32-bit physical address of the DSDT structure
    u32 DSDT;
    u8 Reserved0;
    u8 Preferred_PM_Profile;
    u16 SCI_INT;
    u32 SMI_CMD;
    u8 ACPI_ENABLE;
    u8 ACPI_DISABLE;
    u8 S4BIOS_REQ;
    u8 PSTATE_CNT;
    u32 PM1a_EVT_BLK;
    u32 PM1b_EVT_BLK;
    u32 PM1a_CNT_BLK;
    u32 PM1b_CNT_BLK;
    u32 PM2_CNT_BLK;
    u32 PM_TMR_BLK;
    u32 GPE0_BLK;
    u32 GPE1_BLK;
    u8 PM1_EVT_LEN;
    u8 PM1_CNT_LEN;
    u8 PM2_CNT_LEN;
    u8 PM_TMR_LEN;
    u8 GPE0_BLK_LEN;
    u8 GPE1_BLK_LEN;
    u8 GPE1_BASE;
    u8 CST_CNT;
    u16 P_LVL2_LAT;
    u16 P_LVL3_LAT;
    u16 FLUSH_SIZE;
    u16 FLUSH_STRIDE;
    u8 DUTY_OFFSET;
    u8 DUTY_WIDTH;
    u8 DAY_ALRM;
    u8 MON_ALRM;
    u8 CENTURY;
    u16 IAPC_BOOT_ARCH;
    // Must be 0
    u8 Reserved1;
    struct Flags {
        u8 WBINVD                      : 1;
        u8 WBINVD_FLUSH                : 1;
        u8 PROC_C1                     : 1;
        u8 P_LVL2_UP                   : 1;
        u8 PWD_BUTTON                  : 1;
        u8 SLP_BUTTON                  : 1;
        u8 FIX_RTC                     : 1;
        u8 RTC_S4                      : 1;
        u8 TMR_VAL_EXT                 : 1;
        u8 DCK_CAP                     : 1;
        u8 RESET_REG_SUP               : 1;
        u8 SEALED_CASE                 : 1;
        u8 HEADLESS                    : 1;
        u8 CPU_SW_SLP                  : 1;
        u8 PCI_EXP_WAK                 : 1;
        u8 USE_PLATFORM_CLOCK          : 1;
        u8 S4_RTC_STS_VALID            : 1;
        u8 REMOTE_POWER_ON_CAPABLE     : 1;
        u8 FORCE_APIC_CLUSTER_MODEL    : 1;
        u8 FORCE_APIC_PHYSICAL_DESTINATION_MODE : 1;
        u8 HW_REDUCED_ACPI             : 1;
        u8 LOW_POWER_S0_IDLE_CAPABLE   : 1;
        u8 PERSISTENT_CPU_CACHES       : 2;
        u8 Reserved : 8;
    } Flags;
    GenericAddressRegister RESET_REG;
    u8 RESET_VALUE;
    u16 ARM_BOOT_ARCH;
    u8 FADT_Minor_Version;
    u64 X_FIRMWARE_CTRL;
    u64 X_DSDT;
    GenericAddressRegister X_PM1a_EVT_BLK;
    GenericAddressRegister X_PM1b_EVT_BLK;
    GenericAddressRegister X_PM1a_CNT_BLK;
    GenericAddressRegister X_PM1b_CNT_BLK;
    GenericAddressRegister X_PM2_CNT_BLK;
    GenericAddressRegister X_PM_TMR_BLK;
    GenericAddressRegister X_GPE0_BLK;
    GenericAddressRegister X_GPE1_BLK;
    GenericAddressRegister SLEEP_CONTROL_REG;
    GenericAddressRegister SLEEP_STATUS_REG;
    char8_t HypervisorVendorIdentity[8];
} __attribute__((packed));

// Firmware ACPI Control Structure
// Signature should be "FACS"
// Spec: https://uefi.org/sites/default/files/resources/ACPI_Spec_6_5_Aug29.pdf#subsection.5.2.10
struct FACS {
    u32 Signature;
    u32 Length;
    u32 HardwareSignature;
    u32 FirmwareWakingVector;
    u32 GlobalLock;
    struct Flags {
        u8 S4BIOS_F : 1;
        u8 _64BIT_WAKE_SUPPORTED_F : 1;
        u32 Reserved : 30;
    } flags;
    u64 X_FirmwareWakingVector;
    u8 Version;
    u8 Reserved0[3];
    struct OSPMFlags {
        u8 _64BIT_WAKE_F : 1;
        u32 Reserved : 31;
    } OSPMFlags;
    u8 Reserved1[24];
} __attribute__((packed));

// Differentiated System Description Table
// Signature should be "DSDT"
// There is AML bytecode after the header. You can figure out how much from header.Length
// Spec: https://uefi.org/sites/default/files/resources/ACPI_Spec_6_5_Aug29.pdf#subsubsection.5.2.11.1
struct DSDT {
    SDTHeader header;
} __attribute__((packed));

// Configuration Space Base Address Allocation Structure
// Relevant to the "MCFG" ACPI table
struct CSBAAS {
    // Base Address of Enhanced Configuration Mechanism
    u64 base_address;
    u16 pci_segment_group_number;
    // Start PCI Bus number decoded by this host bridge
    u8 start_pci;
    // End PCI Bus number decoded by this host bridge
    u8 end_pci;
    u32 Reserved;
} __attribute__((packed));

// No idea what the name means
// After the reserved bytes there is a list of CSBAAS structs. You can get their count from header.Length
// Signature should be "MCFG"
// Not spec but has to do bc spec is not freely accessible: https://wiki.osdev.org/PCI_Express#Enhanced_Configuration_Mechanism
struct MCFG {
    SDTHeader header;
    u8 Reserved[8];
} __attribute__((packed));

} // namespace ACPI
