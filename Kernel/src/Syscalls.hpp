#pragma once
#include "common.hpp"
#include "APIC.hpp"


namespace Syscalls {

// Prints out the APIC timer value
inline GENERAL_REGS u64 sys$test() {
    APIC::print_timer_count();
    return 0;
}

inline GENERAL_REGS u64 sys$timerval() {
    return APIC::get_timer_count();
}

// Returns the return value in rax; all other registers are saved and unchanged
// TODO: no_caller_saved_registers does not fit here -- it is the oposite of preserving register values
inline GENERAL_REGS __attribute__((no_caller_saved_registers)) u64 cpp_entry(
    u64 arg0,
    u64 arg1,
    u64 arg2,
    u64 arg3,
    u64 arg4,
    u64 arg5
) {
    // TODO: Delete this warning silencer later
    []<typename... Args>(Args... args){(((void) args), ...);}(arg0, arg1, arg2, arg3, arg4, arg5);

    switch (arg0) {
    case 0:
        return sys$test();
    case 1:
        return sys$timerval();
    default:
        // Unknown syscall
        return ~0ull;
    }
}

// This is the syscall entry point
inline GENERAL_REGS __attribute__((naked)) void asm_entry() {
    asm (
        // TODO: Move arguments to the right registers
        // TODO: I guess we should switch to a kernel stack?
        //       Are there other resources we should be careful about?
        "call %P0;"
        "sysretq;"
        ::
        "i"(cpp_entry)
    );
}

} // namespace Syscalls
