#include "Schedueler.hpp"
#include "paging.hpp"
#include "PageAllocator.hpp"
#include "x86.hpp"
#include "Syscalls.hpp"


namespace Schedueler {

TSS cpu0_tss;

// Right now just tries to get to userspace
NORETURN void work() {
    // Page table phys address of the new process
    const u64 new_page_table_phys = PageAllocator::get_page();

    // Get physical address of our current page table
    const u64 level4_phys_addr = read_cr3();

    // Zero the first half of the new page table
    memset(reinterpret_cast<u8*>(new_page_table_phys), 0, 4096 / 2);

    // Copy the second half of the new page table from our current one
    memcpy(reinterpret_cast<u8*>(new_page_table_phys) + 4096 / 2, reinterpret_cast<u8*>(level4_phys_addr) + 4096 / 2, 4096 / 2);

    // Map a new page into lower half and copy a simple user program there
    const u64 program_page = PageAllocator::get_page_zeroed();
    map_pa_to_va(new_page_table_phys, program_page, 0x80'000, IsWritable::No, IsExecutable::Yes, IsUsermode::Yes);

    // Userspace Stack
    map_pa_to_va(new_page_table_phys, PageAllocator::get_page_zeroed(), 0x81'000, IsWritable::Yes, IsExecutable::No, IsUsermode::Yes);

    constexpr static u8 program[] = {
        /* 80000 */ 0x55,                                       // push   %rbp
        /* 80001 */ 0xbf, 0x01, 0x00, 0x00, 0x00,               // mov    $0x1,%edi
        /* 80006 */ 0x48, 0x89, 0xe5,                           // mov    %rsp,%rbp
        /* 80009 */ 0x41, 0x57,                                 // push   %r15
        /* 8000b */ 0x41, 0x56,                                 // push   %r14
        /* 8000d */ 0x41, 0x55,                                 // push   %r13
        /* 8000f */ 0x41, 0x54,                                 // push   %r12
        /* 80011 */ 0x53,                                       // push   %rbx
        /* 80012 */ 0x0f, 0x05,                                 // syscall
        /* 80014 */ 0x45, 0x31, 0xf6,                           // xor    %r14d,%r14d
        /* 80017 */ 0x48, 0x89, 0xc3,                           // mov    %rax,%rbx
        /* 8001a */ 0x41, 0xbd, 0x88, 0x13, 0x00, 0x00,         // mov    $0x1388,%r13d
        /* 80020 */ 0x41, 0xbf, 0x01, 0x00, 0x00, 0x00,         // mov    $0x1,%r15d
        /* 80026 */ 0x4c, 0x89, 0xf7,                           // mov    %r14,%rdi
        /* 80029 */ 0x0f, 0x05,                                 // syscall
        /* 8002b */ 0x31, 0xd2,                                 // xor    %edx,%edx
        /* 8002d */ 0x48, 0x89, 0xd8,                           // mov    %rbx,%rax
        /* 80030 */ 0x4c, 0x8d, 0xa3, 0x88, 0x13, 0x00, 0x00,   // lea    0x1388(%rbx),%r12
        /* 80037 */ 0x49, 0xf7, 0xf5,                           // div    %r13
        /* 8003a */ 0x49, 0x29, 0xd4,                           // sub    %rdx,%r12
        /* 8003d */ 0x4c, 0x89, 0xff,                           // mov    %r15,%rdi
        /* 80040 */ 0x0f, 0x05,                                 // syscall
        /* 80042 */ 0x48, 0x89, 0xc3,                           // mov    %rax,%rbx
        /* 80045 */ 0x4c, 0x39, 0xe0,                           // cmp    %r12,%rax
        /* 80048 */ 0x72, 0xf3,                                 // jb     8003d <__start+0x3d>
        /* 8004a */ 0xeb, 0xda,                                 // jmp    80026 <__start+0x26>
    };

    // Copy the program into the new address space
    memcpy(reinterpret_cast<u8*>(program_page), program, sizeof(program));

    // Set up the TSS to hold the kernel stack address that will be loaded after
    // we recieve an interrupt while in ring 3
    constexpr u64 kernel_stack_va = 0xffff'ffff'7fff'9000;
    cpu0_tss.rsp0_low  = 0xffff'ffff & kernel_stack_va;
    cpu0_tss.rsp0_high = kernel_stack_va >> 32;

    // The TSS Descriptor was already set up in GDT::init(), but we now need to tell the CPU about it
    {
        constexpr u16 tmp = 0x28;
        asm volatile ("ltr %0" :: "m"(tmp));
    }

    // Enable the syscall and sysret instructions
    WriteMsr(Msr::STAR{
        .SYSCALL_TARGET_EIP = 0,
        .SYSCALL_CS_SS = 0x8,
        .SYSRET_CS_SS = 0x10
    });
    WriteMsr(Msr::LSTAR{ .TARGET_RIP_64 = reinterpret_cast<u64>(Syscalls::asm_entry) });
    WriteMsr(Msr::CSTAR{ .TARGET_RIP_32 = 0 });
    WriteMsr(Msr::SFMASK{ .SYSCALL_FLAG_MASK = ~static_cast<u32>(0x202) });

    Msr::EFER msr_efer = ReadMsr<Msr::EFER>();
    msr_efer.SCE = 1;
    WriteMsr(msr_efer);

    print("Jumping to userspace, whish me luck!\n");

    asm volatile (
        "mov %0, %%cr3;"     // New Page Table
        "pushq $(0x18 + 3);" // Stack Segment Selector + DPL 3
        "pushq $0x82000;"    // Userspace Stack Pointer
        "pushq $0x202;"      // Userspace rFLAGS (Interrupts enabled: 0x202, Interrupts disabled: 0x002)
        "pushq $(0x20 + 3);" // Code Segment Selector + DPL 3
        "pushq $0x80000;"    // Address to jump to
        "iretq;"             // Jump to Userspace
        : : "r"(new_page_table_phys)
    );
    __builtin_unreachable();
}

} // namespace Schedueler
