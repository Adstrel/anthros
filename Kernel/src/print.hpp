#pragma once
#include "common.hpp"
#include "ACPI/Definitions.hpp"

ALWAYS_INLINE GENERAL_REGS void out_dbg_char(const char c) {
    asm volatile("outb %0, %1" :: "a"(c), "Nd"(0xe9));
}

inline void out_dbg(const char* str) {
    while (*str != '\0') {
        out_dbg_char(*str++);
    }
}

inline void out_int(u64 value, const u64 base) {
    if (value == 0) {
        out_dbg_char('0');
        return;
    }

    char buffer[65];
    buffer[64] = '\0';
    usize index = 64;

    while (value != 0) {
        const u64 digit = value % base;
        value /= base;

        buffer[--index] = static_cast<char>(digit >= 10 ? 'a' + digit - 10 : '0' + digit);
    }

    out_dbg(buffer + index);
}

inline void print_arg(const char** fmt, u64 value) {
    // Skip the '{'
    (*fmt)++;

    struct IntegerFormat {
        u64 base = 10;
        enum class Case { Lower, Upper } _case = Case::Lower;
        bool print_prefix = false;
    } format;

    while (**fmt != '}') {
        switch (*(*fmt)++) {
        case 'b':
            format.base = 2;
            break;
        case 'o':
            format.base = 8;
            break;
        case 'd':
            format.base = 10;
            break;
        case 'x':
            format.base = 16;
            break;
        case 'X':
            format.base = 16;
            format._case = IntegerFormat::Case::Upper;
            break;
        case 'h':
            format.print_prefix = true;
            break;
        default:
            break;
        }
    }

    // Skip the '}'
    (*fmt)++;

    if (format.print_prefix) {
        out_dbg_char('0');
        switch (format.base) {
        case 2:
            out_dbg_char('b');
            break;
        case 8:
            out_dbg_char('o');
            break;
        case 10:
            out_dbg_char('d');
            break;
        case 16:
            out_dbg_char('x');
            break;
        default:
            break;
        }
    }

    out_int(value, format.base);
}

inline void print_arg(const char** fmt, const char* str) {
    // Skip '{' and '}'
    (*fmt) += 2;
    out_dbg(str);
}

inline void print_arg(const char** fmt, ACPI::InterruptControllerStructure::Type type) {
    // Skip '{' and '}'
    (*fmt) += 2;
    switch (type) {
    case ACPI::InterruptControllerStructure::Type::ProcessorLocalAPIC:
        out_dbg("ProcessorLocalAPIC");
        break;
    case ACPI::InterruptControllerStructure::Type::IO_APIC:
        out_dbg("IO_APIC");
        break;
    case ACPI::InterruptControllerStructure::Type::InterruptSourceOverride:
        out_dbg("InterruptSourceOverride");
        break;
    case ACPI::InterruptControllerStructure::Type::NonmaskableInterruptSource:
        out_dbg("NonmaskableInterruptSource");
        break;
    case ACPI::InterruptControllerStructure::Type::LocalApicNMI:
        out_dbg("LocalApicNMI");
        break;
    case ACPI::InterruptControllerStructure::Type::LocalApicAddressOverride:
        out_dbg("LocalApicAddressOverride");
        break;
    case ACPI::InterruptControllerStructure::Type::IO_SAPIC:
        out_dbg("IO_SAPIC");
        break;
    case ACPI::InterruptControllerStructure::Type::LocalSAPIC:
        out_dbg("LocalSAPIC");
        break;
    case ACPI::InterruptControllerStructure::Type::PlatformInterruptSources:
        out_dbg("PlatformInterruptSources");
        break;
    case ACPI::InterruptControllerStructure::Type::ProcessorLocal_x2APIC:
        out_dbg("ProcessorLocal_x2APIC");
        break;
    case ACPI::InterruptControllerStructure::Type::Local_x2APIC_NMI:
        out_dbg("Local_x2APIC_NMI");
        break;
    case ACPI::InterruptControllerStructure::Type::GIC_CPU_Interface:
        out_dbg("GIC_CPU_Interface");
        break;
    case ACPI::InterruptControllerStructure::Type::GIC_Distributor:
        out_dbg("GIC_Distributor");
        break;
    case ACPI::InterruptControllerStructure::Type::GIC_MSI_Frame:
        out_dbg("GIC_MSI_Frame");
        break;
    case ACPI::InterruptControllerStructure::Type::GIC_Redistributor:
        out_dbg("GIC_Redistributor");
        break;
    case ACPI::InterruptControllerStructure::Type::GIC_InterruptTranslationService:
        out_dbg("GIC_InterruptTranslationService");
        break;
    case ACPI::InterruptControllerStructure::Type::MultiprocessorWakeup:
        out_dbg("MultiprocessorWakeup");
        break;
    case ACPI::InterruptControllerStructure::Type::CoreProgrammableInterruptController:
        out_dbg("CoreProgrammableInterruptController");
        break;
    case ACPI::InterruptControllerStructure::Type::LegacyIOProgrammableInterruptController:
        out_dbg("LegacyIOProgrammableInterruptController");
        break;
    case ACPI::InterruptControllerStructure::Type::HyperTransportProgrammableInterruptController:
        out_dbg("HyperTransportProgrammableInterruptController");
        break;
    case ACPI::InterruptControllerStructure::Type::ExtendIOProgrammableInterruprController:
        out_dbg("ExtendIOProgrammableInterruprController");
        break;
    case ACPI::InterruptControllerStructure::Type::MSIProgrammableInterruptController:
        out_dbg("MSIProgrammableInterruptController");
        break;
    case ACPI::InterruptControllerStructure::Type::BridgeIOProgrammableInterruptController:
        out_dbg("BridgeIOProgrammableInterruptController");
        break;
    case ACPI::InterruptControllerStructure::Type::LowPinCountProgrammableInterruptController:
        out_dbg("LowPinCountProgrammableInterruptController");
        break;
    default:
        out_dbg("[[Unknown]]");
    }
}

inline void print_fmt(const char** fmt) {
    while (**fmt != '{') {
        if (**fmt == '\0') {
            out_dbg("\nPANIC: Missing format specifier!\n");
            while (true);
        }
        out_dbg_char(*(*fmt)++);
    }
}

template<typename... Args_t>
inline void print(const char* fmt, Args_t... args) {
    ((print_fmt(&fmt), print_arg(&fmt, args)), ...);
    out_dbg(fmt);
}

template<typename... Args_t>
inline void println(const char* fmt, Args_t... args) {
    print(fmt, args...);
    out_dbg_char('\n');
}

NORETURN inline void panic(const char* msg, const char* func = "") {
    if (func) {
        print("[PANIC]: Crash in \"{}\"\n", func);
    }
    print("[PANIC]: {}\n", msg);
    while (true);
}
