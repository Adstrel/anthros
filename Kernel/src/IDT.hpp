#pragma once
#include "cpp/Common.hpp"


#define ISR __attribute__((interrupt, target("general-regs-only")))

struct interrupt_frame {
    u64 rip;
    u64 cs;
    u64 rflags;
    u64 rsp;
    u64 ss;
};

namespace IDT {

void init();

} // namespace IDT
