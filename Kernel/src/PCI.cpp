#include "PCI.hpp"
#include "Devices/qemu_virtual_video_controller.hpp"
#include "Devices/rtl8139.hpp"
#include "print.hpp"

namespace PCI {

// TODO: Major refactor needed
const char* pci_vendor_id_to_string(u16 vendor_id) {
    switch (vendor_id) {
    case 0x10ec: return "Realtek Semiconductor Co., Ltd.";
    case 0x1234: return "QEMU/Bochs";
    case 0x1af4: return "Red Hat, Inc.";
    case 0x1b36: return "Red Hat, Inc.";
    case 0x8086: return "Intel Corporation";
    default:
        return nullptr;
    }
}

// TODO: Major refactor needed
const char* pci_device_to_string(u16 vendor_id, u16 device_id) {
    switch (vendor_id) {
    case 0x10ec:
        switch (device_id) {
        case 0x8139: return "RTL-8100/8101L/8139 PCI Fast Ethernet Adapter";
        case 0x8168: return "RTL8111/8168/8211/8411 PCI Express Gigabit Ethernet Controller";
        }
        break;
    case 0x1234:
        switch (device_id) {
        case 0x1111: return "Virtual Video Controller";
        case 0x11e8: return "QEMU EDU device";
        }
        break;
    case 0x1af4:
        switch (device_id) {
        case 0x1041: return "Virtio 1.0 network device";
        case 0x1044: return "Virtio 1.0 RNG";
        case 0x1050: return "Virtio 1.0 GPU";
        case 0x105a: return "Virtio file system";
        }
        break;
    case 0x1b36:
        switch (device_id) {
        case 0x0001: return "QEMU PCI-PCI bridge";
        case 0x0002: return "QEMU PCI 16550A Adapter";
        case 0x0003: return "QEMU PCI Dual-port 16550A Adapter";
        case 0x0004: return "QEMU PCI Quad-port 16550A Adapter";
        case 0x0005: return "QEMU PCI Test Device";
        case 0x000d: return "QEMU XHCI Host Controller";
        case 0x0010: return "QEMU NVM Express Controller";
        }
        break;
    case 0x8086:
        switch (device_id) {
        case 0x29c0: return "82G33/G31/P35/P31 Express DRAM Controller";
        case 0x100e: return "82540EM Gigabit Ethernet Controller";
        case 0x2918: return "82801IB (ICH9) LPC Interface Controller";
        }
        break;
    }
    return nullptr;
}

// TODO: Write an actual driver
static void dummy_driver_82540EM_Gigabit_Ethernet_Controller(u64 base_addr, usize bus_index, usize device_index) {
    usize function_index = 0;
    u64 pci_config_physical_base_address = base_addr + (
        (bus_index << 20) |
        (device_index << 15) |
        (function_index << 12)
    );

    CommonHeader* common_header = reinterpret_cast<CommonHeader*>(pci_config_physical_base_address);
    const char* device_string = pci_device_to_string(0x8086, 0x100e);

    if (!device_string) {
        // Should not be reachable
        // TODO: Refactor everything, so we don't have to have this dummy check here
        return;
    }

    bool is_common_header_correct =
        common_header->VendorID == 0x8086 &&
        common_header->DeviceID == 0x100e &&
        common_header->HeaderType == 0 &&
        common_header->ClassCode == 2 &&
        common_header->Subclass == 0 &&
        common_header->ProgIF == 0;

    if (!is_common_header_correct) {
        print("Incorrect PCI common header for device \"{}\"!\n", device_string);
        return;
    }

    HeaderType_0* header = reinterpret_cast<HeaderType_0*>(common_header);

    print("Status: {bh}\n", header->common.Status);
    print("Command: {bh}\n", header->common.Command);
    print("Revision ID: {xh}\n", header->common.RevisionID);
    print("Cache line size: {xh}\n", header->common.CacheLineSize);
    print("BIST: {xh}\n", header->common.BIST);
    print("Capabilities Pointer: {xh}\n", header->CapabilitiesPointer);

    print("BAR0: {xh}\n", header->BAR0);
    print("BAR1: {xh}\n", header->BAR1);
    print("BAR2: {xh}\n", header->BAR2);
    print("BAR3: {xh}\n", header->BAR3);
    print("BAR4: {xh}\n", header->BAR4);
    print("BAR5: {xh}\n", header->BAR5);

    print("PCI device \"{}\" successfully initialized!\n", device_string);
}

// Does not appear to be a usefull device inside QEMU. Can we do anything with this? We might have to just ignore this.
static void dummy_driver_82801IB_ICH9_LPC_Interface_Controller(u64 base_addr, usize bus_index, usize device_index) {
    usize function_index = 0;
    u64 pci_config_physical_base_address = base_addr + (
        (bus_index << 20) |
        (device_index << 15) |
        (function_index << 12)
    );

    CommonHeader* common_header = reinterpret_cast<CommonHeader*>(pci_config_physical_base_address);
    const char* device_string = pci_device_to_string(0x8086, 0x2918);

    if (!device_string) {
        // Should not be reachable
        // TODO: Refactor everything, so we don't have to have this dummy check here
        return;
    }

    bool is_common_header_correct =
        common_header->VendorID == 0x8086 &&
        common_header->DeviceID == 0x2918 &&
        common_header->HeaderType == 0 &&
        common_header->ClassCode == 6 &&
        common_header->Subclass == 1 &&
        common_header->ProgIF == 0;

    if (!is_common_header_correct) {
        print("Incorrect PCI common header for device \"{}\"!\n", device_string);
        return;
    }

    HeaderType_0* header = reinterpret_cast<HeaderType_0*>(common_header);

    print("Status: {bh}\n", header->common.Status);
    print("Command: {bh}\n", header->common.Command);
    print("Revision ID: {xh}\n", header->common.RevisionID);
    print("Cache line size: {xh}\n", header->common.CacheLineSize);
    print("BIST: {xh}\n", header->common.BIST);
    print("Capabilities Pointer: {xh}\n", header->CapabilitiesPointer);

    print("BAR0: {xh}\n", header->BAR0);
    print("BAR1: {xh}\n", header->BAR1);
    print("BAR2: {xh}\n", header->BAR2);
    print("BAR3: {xh}\n", header->BAR3);
    print("BAR4: {xh}\n", header->BAR4);
    print("BAR5: {xh}\n", header->BAR5);

    print("PCI device \"{}\" successfully initialized!\n", device_string);
}

static void call_native_pci_device_driver(u16 vendor_id, u16 device_id, u64 base_addr, usize bus_index, usize device_index) {
    switch (vendor_id) {
    case 0x10ec:
        switch (device_id) {
        case 0x8139:
            Device::rtl8139::init(base_addr, bus_index, device_index);
            break;
        }
        break;
    case 0x1234:
        switch (device_id) {
        case 0x1111:
            QemuVirtVideoController::init(base_addr, bus_index, device_index);
            break;
        }
        break;
    case 0x8086:
        switch (device_id) {
        case 0x29c0:
            // This device does not need a driver, since it does not have any functionality that we can use
            break;
        case 0x100e:
            dummy_driver_82540EM_Gigabit_Ethernet_Controller(base_addr, bus_index, device_index);
            break;
        case 0x2918:
            dummy_driver_82801IB_ICH9_LPC_Interface_Controller(base_addr, bus_index, device_index);
            break;
        }
        break;
    }
}

static void enumerate_pci_bus(const ACPI::CSBAAS* csbaas) {
    for (usize bus_index = csbaas->start_pci; bus_index <= csbaas->end_pci; bus_index++) {
        for (usize device_index = 0; device_index < 32; device_index++) {
            // All devices have to implement function 0, so we use it to check if the device is present
            usize function_index = 0;

            // TODO: Are you supposed to subtract the start_pci index for the bus index?
            // TODO: Are all values here within valid ranges?
            u64 pcibus_physical_address = csbaas->base_address + (
                ((bus_index - csbaas->start_pci) << 20) |
                (device_index << 15) |
                (function_index << 12)
            );

            CommonHeader* header = reinterpret_cast<CommonHeader*>(pcibus_physical_address);

            if (header->VendorID == 0xffff) {
                continue;
            }

            print("Bus {}, Device {}, PCI BUS ADDR: {xh}\n", bus_index, device_index, pcibus_physical_address);
            const char* vendor_string = pci_vendor_id_to_string(header->VendorID);
            const char* device_string = pci_device_to_string(header->VendorID, header->DeviceID);
            if (vendor_string && device_string) {
                print("\t- Vendor: \"{}\", Device: \"{}\"\n", vendor_string, device_string);
            }
            print("\t- VendorID: {} ({xh}), DeviceID: {} ({xh})\n", header->VendorID, header->VendorID, header->DeviceID, header->DeviceID);
            print("\t- Header Type: {xh}, Class code: {xh}, Subclass: {xh}, ProgIF: {xh}\n",
                header->HeaderType,
                header->ClassCode,
                header->Subclass,
                header->ProgIF
            );
            if (header->HeaderType == 0) {
                PCI::HeaderType_0* header0 = reinterpret_cast<PCI::HeaderType_0*>(header);
                print("\t- Subsystem Vendor ID: {} ({xh}), Subsystem ID: {} ({xh})\n",
                    header0->SubsystemVendorID,
                    header0->SubsystemVendorID,
                    header0->SubsystemID,
                    header0->SubsystemID
                );
            }

            call_native_pci_device_driver(
                header->VendorID,
                header->DeviceID,
                csbaas->base_address,
                bus_index - csbaas->start_pci,
                device_index
            );
        }
    }
}

void register_mcfg_table(u64 mcfg_physical_addr) {
    const ACPI::MCFG* mcfg_table = reinterpret_cast<const ACPI::MCFG*>(mcfg_physical_addr);

    usize csbaas_size = (mcfg_table->header.Length - sizeof(ACPI::MCFG)) / sizeof(ACPI::CSBAAS);

    // TODO: Check that the table is valid
    const ACPI::CSBAAS* csbaas_list = reinterpret_cast<const ACPI::CSBAAS*>(mcfg_table + 1);

    for (usize i = 0; i < csbaas_size; i++) {
        print("MCFG entry {}: BaseAddr: {xh}, PCI_Sgmnt_grp_nmbr: {}, StartPCI: {}, EndPCI: {}\n",
            i,
            csbaas_list[i].base_address,
            csbaas_list[i].pci_segment_group_number,
            csbaas_list[i].start_pci,
            csbaas_list[i].end_pci
        );
        enumerate_pci_bus(csbaas_list + i);
    }
}

} // namespace PCI
