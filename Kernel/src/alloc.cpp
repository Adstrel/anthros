#include "alloc.hpp"
#include "paging.hpp"


// TODO: Rewrite the allocator to work even for freeing
static constexpr const u64 begin_va = 0xffff'9000'0000'0000;
static constexpr const u64 max_size = 0x0000'1000'0000'0000;
static u64 g_allocated_bytes = 0;

void kfree(void* ptr) {
    (void) ptr;
}

void* kalloc(const usize needed_size, const usize alignment) {
    // TODO: Refactor this mess... wth even is this
    const u64 alloc_offset = alignment - (g_allocated_bytes % alignment == 0 ? alignment : g_allocated_bytes % alignment);
    const u64 alloc_size = needed_size + alloc_offset;

    const u64 old_end = begin_va + g_allocated_bytes;
    g_allocated_bytes += alloc_size;

    // Map new pages into memory we will now be using
    const u64 map_pages_count =
        (old_end + alloc_size) / 4096
        - old_end / 4096
        + (old_end % 4096 == 0 ? 1 : 0)                 // If we begun on a page boundary
        - ((old_end + alloc_size) % 4096 == 0 ? 1 : 0); // If we end on a page boundary
    if (map_pages_count) {
        const u64 map_range_begin = old_end - (old_end % 4096) + (old_end % 4096 != 0 ? 4096 : 0);
        alloc_and_map_memory(
            read_cr3(),
            map_range_begin,
            map_pages_count,
            IsWritable::Yes,
            IsExecutable::No,
            IsUsermode::No
        );
    }

    return reinterpret_cast<void*>(old_end + alloc_offset);
}
