#include "PageAllocator.hpp"
#include "paging.hpp"
#include "x86.hpp"


extern "C" void* kernel_addr_start[];
extern "C" void* kernel_addr_end[];

enum class MemoryType : u32 {
    EfiReservedMemoryType       = 0,
    EfiLoaderCode               = 1,
    EfiLoaderData               = 2,
    EfiBootServicesCode         = 3,
    EfiBootServicesData         = 4,
    EfiRuntimeServicesCode      = 5,
    EfiRuntimeServicesData      = 6,
    EfiConventionalMemory       = 7,
    EfiUnusableMemory           = 8,
    EfiACPIReclaimMemory        = 9,
    EfiACPIMemoryNVS            = 10,
    EfiMemoryMappedIO           = 11,
    EfiMemoryMappedIOPortSpace  = 12,
    EfiPalCode                  = 13,
    EfiPersistentMemory         = 14,
    EfiUnacceptedMemoryType     = 15,
    EfiMaxMemoryType            = 16,
};

struct MemMapElement {
    MemoryType Type;
    u64 PhysicalAddress;
    u64 VirtualAddress;
    u64 NumberOfPages;
    u64 Attribute;
};

namespace PageAllocator {

struct PageBitmap {
    // Nth bit represents page with physical address 'n * 4096'
    u64* buffer;

    // Size of the buffer in 'u64's
    usize size;

    // During allocation, we walk the buffer from start to end, this is our cached index
    usize next_index;
};

static PageBitmap g_page_bitmap{};
static usize g_total_free_pages = 0;
static usize g_total_valid_pages = 0;

u64 get_page() {
    usize next_index = g_page_bitmap.next_index;
    while (g_page_bitmap.buffer[next_index] == 0) {
        next_index = (next_index + 1) % g_page_bitmap.size;
        if (next_index == g_page_bitmap.next_index) {
            print("PANIC: Out of physical memory!\n");
            while (true);
        }
    }
    g_page_bitmap.next_index = next_index;

    const u64 bitmap = g_page_bitmap.buffer[next_index];

    // TODO: Hide the inbuilt funciton in a wrapper and put it inside "Maths.hpp"
    // int __builtin_ffsll(long long):
    // Returns one plus the index of the least significant 1-bit of x, or if x is zero, returns zero.
    const u64 bit = __builtin_ffsll(bitmap) - 1;

    const u64 final_address = next_index * (4096 * 8 * sizeof(u64)) + bit * 4096;
    g_page_bitmap.buffer[next_index] &= ~(1ull << bit);
    g_total_free_pages--;

    return final_address;
}

u64 get_page_zeroed() {
    const u64 final_address = get_page();
    memset(reinterpret_cast<u8*>(final_address), 0, 4096);
    return final_address;
}

void free_page(const u64 phys_page) {
    const u64 index = (phys_page / 4096) / (8 * sizeof(u64));
    const u64 bit = (phys_page / 4096) % (8 * sizeof(u64));

    if (index >= g_page_bitmap.size) {
        print("PANIC: Tried to free a page outside of our managed memory range ({xh})!\n", phys_page);
        while (true);
    }

    if ((g_page_bitmap.buffer[index] & (1ull << bit)) != 0) {
        print("PANIC: Double free of a physical page {xh}!\n", phys_page);
        while (true);
    }

    g_page_bitmap.buffer[index] |= 1ull << bit;
    g_total_free_pages++;
}

void init(
    const u64 efi_mem_map_phys,
    const u64 efi_mem_map_count,
    const u64 efi_mem_map_element_size,
    const u64 kernel_base_phys,
    const u64 used_phys_page_count
) {
    // The start virtual address of the Page Allocator bitmap buffer
    constexpr const u64 buffer_begin_va = 0xffff'8000'0000'0000ull;

    const u64 kernel_size = reinterpret_cast<u64>(kernel_addr_end) - reinterpret_cast<u64>(kernel_addr_start);
    // TODO: Why does it have to be +1? The stack should be part of 'used_phys_page_count'!
    //       Triple faults later when the +1 is not here. We later try to copy the root page table and we suddenly don't have write permission to the stack.
    const u64 first_next_free_page = kernel_base_phys + kernel_size + (used_phys_page_count + 1) * 4096;

    g_page_bitmap = {
        // Lets use an identity mapped physical page as the buffer temporairly and swap it for a different virtual address later
        .buffer = reinterpret_cast<u64*>(first_next_free_page),
        .size = 4096 / sizeof(u64),
        .next_index = 0
    };
    memset(g_page_bitmap.buffer, 0, 4096);

    // 1) Find the highest addressable physical page
    // 2) Setup the first page of the Page Allocator Bitmap
    u64 highest_phys_address = 0;
    for (usize i = 0; i < efi_mem_map_count; i++) {
        const MemMapElement* element = reinterpret_cast<const MemMapElement*>(efi_mem_map_phys + i * efi_mem_map_element_size);

        switch (element->Type) {
        case MemoryType::EfiLoaderData:
            // The memory map is in memory of this type, we need to skip it for now
            g_total_valid_pages += element->NumberOfPages;
            continue;
        case MemoryType::EfiACPIReclaimMemory:
            // We will be able to use this memory later, after we parse the ACPI tables
            // TODO: Copy the memory ranges of this type for later; the current memory map
            //   is in memory of type EfiLoaderData, which will be freed later
            g_total_valid_pages += element->NumberOfPages;
            continue;
        case MemoryType::EfiLoaderCode:
        case MemoryType::EfiBootServicesCode:
        case MemoryType::EfiBootServicesData:
        case MemoryType::EfiConventionalMemory:
        case MemoryType::EfiPersistentMemory:
            // We can use memory of these types right now
            g_total_valid_pages += element->NumberOfPages;
            break;
        case MemoryType::EfiRuntimeServicesCode:
        case MemoryType::EfiRuntimeServicesData:
        case MemoryType::EfiACPIMemoryNVS:
            // This memory is valid, but used by the firmware
            g_total_valid_pages += element->NumberOfPages;
            continue;
        default:
            // All other memory types are unusable and we don't need to track them
            continue;
        }

        // Highest page of this memory range
        const u64 high_bound = element->PhysicalAddress + 4096 * (element->NumberOfPages - 1);
        if (highest_phys_address < high_bound) {
            highest_phys_address = high_bound;
        }

        // If the pages are in index range from 0 to 4096 * 8, we can mark them in the first page of the bitmap buffer
        if (element->PhysicalAddress / 4096 < 4096 * 8) {
            // We need to reserve the very first page; we need it to boot up other cores
            if (element->PhysicalAddress == 0) {
                continue;
            }

            // TODO: Rewrite this to do less loops?
            for (usize phys_page = element->PhysicalAddress; phys_page < element->PhysicalAddress + 4096 * (element->NumberOfPages - 1); phys_page += 4096) {
                if (phys_page / 4096 >= 4096 * 8) {
                    break;
                }
                if (phys_page >= kernel_base_phys && phys_page < first_next_free_page + 4096) {
                    continue;
                }
                free_page(phys_page);
            }
        }
    }

    // We have no reason to create our own page table, lets reuse the one setup by our bootloader.
    // We just need to make sure not to touch the structures for lower half of memory, as page
    //   tables for those ranges might be in ROM, which we cannot alter.
    // TODO: Yea, but they might also be in memory marked "EfiLoaderData" which we are about to deallocate now.
    //       We should just create our own page table from scratch to avoid any danger here!
    // TODO: Does the ACPI spec say where exactly the page table is going to be stored?
    //       We might have to do a runtime check to avoid colisions in the first part of this function.
    const u64 page_table_phys = read_cr3();

    // Map the buffer of the page allocator bitmap to our desired virtual address.
    // If there were no free pages in the first ~134 MiB of physical memory (which cannot happen,
    //   as there has to be valid memory below 1 MiB), we will panic here.
    map_pa_to_va(page_table_phys, first_next_free_page, buffer_begin_va, IsWritable::Yes, IsExecutable::No, IsUsermode::No);
    g_page_bitmap.buffer = reinterpret_cast<u64*>(buffer_begin_va);

    // TODO: Rework computation of the buffer size; I'm not sure we're doing it correctly right now
    const u64 full_buffer_size = (highest_phys_address + 4096*8) / (4096 * 8) + 4096;
    for (u64 page = 4096; page < full_buffer_size; page += 4096) {
        const u64 va = buffer_begin_va + page;
        map_pa_to_va(page_table_phys, get_page_zeroed(), va, IsWritable::Yes, IsExecutable::No, IsUsermode::No);
        // Flush the tlb
        asm volatile ("invlpg (%0)" : : "r"(va));
    }

    g_page_bitmap.size = full_buffer_size / sizeof(u64);

    // Set up the rest of the Page Allocator Bitmap (we only setup the first 4096 of the buffer so far)
    for (usize i = 0; i < efi_mem_map_count; i++) {
        const MemMapElement* element = reinterpret_cast<const MemMapElement*>(efi_mem_map_phys + i * efi_mem_map_element_size);

        switch (element->Type) {
        case MemoryType::EfiLoaderData:
            // We skipped all memory of this type before, so we can't double free here
            for (usize phys_page = element->PhysicalAddress; phys_page < element->PhysicalAddress + 4096 * (element->NumberOfPages - 1); phys_page += 4096) {
                free_page(phys_page);
            }
            continue;
        case MemoryType::EfiLoaderCode:
        case MemoryType::EfiBootServicesCode:
        case MemoryType::EfiBootServicesData:
        case MemoryType::EfiConventionalMemory:
        case MemoryType::EfiPersistentMemory:
            break;
        default:
            // All other memory types are unusable and we don't need to track them
            continue;
        }

        // If the pages are in index range from 0 to 4096 * 8, we can mark them in the first page of the bitmap buffer
        if ((element->PhysicalAddress + (element->NumberOfPages - 1) * 4096) / 4096 >= 4096 * 8) {
            for (usize phys_page = element->PhysicalAddress; phys_page < element->PhysicalAddress + 4096 * (element->NumberOfPages - 1); phys_page += 4096) {
                if (phys_page / 4096 < 4096 * 8) {
                    // We freed memory from this low range already
                    // And also allocated from this range, so we can't just free it again, we might be using it!
                    continue;
                }
                if (phys_page >= kernel_base_phys && phys_page < first_next_free_page + 4096) {
                    // We already use this memory
                    continue;
                }
                free_page(phys_page);
            }
        }
    }

    print("Initialized the Bitmap Page Allocator!\n");
    print("Total free pages:  {}\n", g_total_free_pages);
    print("Total valid pages: {}\n", g_total_valid_pages);
}

} // namespace PageAllocator
