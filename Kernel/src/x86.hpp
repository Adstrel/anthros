#pragma once
#include "common.hpp"


namespace Msr {

// APIC Base Address Register
struct APIC_BAR {
    static constexpr u32 GetBaseAddress() { return 0x0000'001b; }

    // Reserved
    // Bits: 0-7, Access Type: MBZ
    u64 reserved_0_7 : 8 = 0;

    // Boot Strap CPU Core
    // Bit: 8, Access Type: RO
    u64 BSC : 1;

    // Reserved
    // Bit: 9, Access Type: MBZ
    u64 reserved_9 : 1 = 0;

    // x2APIC Mode Enable
    // Bit: 10, Access Type: R/W
    u64 EXTD : 1;

    // APIC Enable
    // Bit: 11, Access Type: R/W
    u64 AE : 1;

    // APIC Base Address
    // Bits: 12-51, Access Type: R/W
    u64 ABA : 40;

    // Reserved
    // Bits: 52-63, Access Type: MBZ
    u64 reserved_52_64 : 12 = 0;
};

// Extended Feature Enable Register
struct EFER {
    static constexpr u32 GetBaseAddress() { return 0xc000'0080; }

    // System Call Extension
    // Bit: 0, Access Type: R/W
    u64 SCE : 1;

    // Reserved
    // Bits: 1-7, Access Type: RAZ
    u64 reserved_1_7 : 7 = 0;

    // Long Mode Enable
    // Bit: 8, Access Type: R/W
    u64 LME : 1;

    // Reserved
    // Bit: 9, Access Type: MBZ
    u64 reserved_9 : 1 = 0;

    // Long Mode Active
    // Bit: 10, Access Type: R/W
    u64 LMA : 1;

    // No-Execute Enable
    // Bit: 11, Access Type: R/W
    u64 NXE : 1;

    // Secure Virtual Machine Enable
    // Bit: 12, Access Type: R/W
    u64 SVME : 1;

    // Long Mode Segment Limit Enable
    // Bit: 13, Access Type: R/W
    u64 LMSLE : 1;

    // Fast FXSAVE/FXRSTOR
    // Bit: 14, Access Type: R/W
    u64 FFXSR : 1;

    // Translation Cache Extension
    // Bit: 15, Access Type: R/W
    u64 TCE : 1;

    // Reserved
    // Bit: 16, Access Type: MBZ
    u64 reserved_16 : 1 = 0;

    // Enable MCOMMIT Instruction
    // Bit: 17, Access Type: R/W
    u64 MCOMMIT : 1;

    // Interruptible WBINVD/WBNOINVD enable
    // Bit: 18, Access Type: R/W
    u64 INTWB : 1;

    // Reserved
    // Bit: 19, Access Type: MBZ
    u64 reserved_19 : 1 = 0;

    // Upper Address Ignore Enable
    // Bit: 20, Access Type: R/W
    u64 UAIE : 1;

    // Automatic IBRS Enable
    // Bit: 21, Access Type: R/W
    u64 AIBRSE : 1;

    // Reserved
    // Bits: 22-63, Access Type: MBZ
    u64 reserved_22_63 : 42 = 0;
};

struct STAR {
    static constexpr u32 GetBaseAddress() { return 0xc000'0081; }

    // 32-bit SYSCALL Target EIP
    // Bits: 0-31, Access Type: R/W
    u64 SYSCALL_TARGET_EIP : 32;

    // SYSCALL CS and SS
    // Bits: 32-47, Access Type: R/W
    u64 SYSCALL_CS_SS : 16;

    // SYSRET CS and SS
    // Bits: 48-63, Access Type: R/W
    u64 SYSRET_CS_SS : 16;
};

struct LSTAR {
    static constexpr u32 GetBaseAddress() { return 0xc000'0082; }

    // Target RIP for 64-Bit-Mode Calling Software
    // Bits: 0-63, Access Type: R/W
    u64 TARGET_RIP_64 : 64;
};

struct CSTAR {
    static constexpr u32 GetBaseAddress() { return 0xc000'0083; }

    // Target RIP for Compatibility-Mode Calling Software
    // Bits: 0-63, Access Type: R/W
    u64 TARGET_RIP_32 : 64;
};

struct SFMASK {
    static constexpr u32 GetBaseAddress() { return 0xc000'0084; }

    // SYSCALL Flag Mask
    // Bits: 0-31, Access Type: R/W
    u64 SYSCALL_FLAG_MASK : 32;

    // Reserved
    // Bits: 32-63, Access Type: RAZ
    u64 reserved_32_63 : 32 = 0;
};

} // namespace Msr

template<u32 ADDR> ALWAYS_INLINE u64 ReadRawMsr() {
    u32 edx, eax;
    asm ("rdmsr" : "=d"(edx), "=a"(eax) : "c"(ADDR));
    return (static_cast<u64>(edx) << 32) | eax;
}

template<u32 ADDR> ALWAYS_INLINE void WriteRawMsr(u64 data) {
    asm volatile ("wrmsr" : : "d"(static_cast<u32>(data >> 32)), "a"(static_cast<u32>(data)), "c"(ADDR));
}

template<typename Msr_t>
requires (sizeof(Msr_t) == sizeof(u64))
Msr_t ReadMsr();

template<typename Msr_t>
requires (sizeof(Msr_t) == sizeof(u64))
void WriteMsr(Msr_t msr_data);

#define DEFINE_MSR(msr_type)                                                \
    template<> ALWAYS_INLINE msr_type ReadMsr<msr_type>() {                 \
        union U { msr_type as_bitfield; u64 as_number; };                   \
        U test{};                                                           \
        test.as_number = ReadRawMsr<msr_type::GetBaseAddress()>();          \
        return test.as_bitfield;                                            \
    }                                                                       \
                                                                            \
    template<> ALWAYS_INLINE void WriteMsr<msr_type>(msr_type msr_data) {   \
        union U { msr_type as_bitfield; u64 as_number; };                   \
        U test{};                                                           \
        test.as_bitfield = msr_data;                                        \
        WriteRawMsr<msr_type::GetBaseAddress()>(test.as_number);            \
    }

DEFINE_MSR(Msr::APIC_BAR);
DEFINE_MSR(Msr::EFER);
DEFINE_MSR(Msr::STAR);
DEFINE_MSR(Msr::LSTAR);
DEFINE_MSR(Msr::CSTAR);
DEFINE_MSR(Msr::SFMASK);

struct CpuIdRes {
    u32 eax;
    u32 ebx;
    u32 ecx;
    u32 edx;
};

ALWAYS_INLINE CpuIdRes cpuid(const u32 function) {
    CpuIdRes result;
    asm ("cpuid" : "=a"(result.eax), "=b"(result.ebx), "=c"(result.ecx), "=d"(result.edx) : "a"(function));
    return result;
}

ALWAYS_INLINE CpuIdRes cpuid(const u32 function, const u32 arg) {
    CpuIdRes result;
    asm ("cpuid" : "=a"(result.eax), "=b"(result.ebx), "=c"(result.ecx), "=d"(result.edx) : "a"(function), "c"(arg));
    return result;
}

ALWAYS_INLINE u64 read_cr3() {
    u64 level4_phys_addr;
    asm ("mov %%cr3, %0" : "=r"(level4_phys_addr));
    level4_phys_addr = level4_phys_addr & (~0xfffull);
    return level4_phys_addr;
}
