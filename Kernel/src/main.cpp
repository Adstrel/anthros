#include "common.hpp"
#include "print.hpp"
#include "GDT.hpp"
#include "PageAllocator.hpp"
#include "ACPI.hpp"
#include "IDT.hpp"
#include "APIC.hpp"
#include "Schedueler.hpp"
#include "alloc.hpp"
#include "PCI.hpp"


extern "C" NORETURN void kmain(
        const u64 rsdp_phys,
        const u64 efi_mem_map_phys,
        const u64 efi_mem_map_count,
        const u64 efi_mem_map_element_size,
        const u64 kernel_base_phys,
        const u64 used_phys_pages_count) {
    print("WE ARE IN!\n{x}\n", rsdp_phys);

    // Swap UEFI's segments for ours
    GDT::init();
    // Set up our Interrupt and Exception handlerers
    IDT::init();
    asm volatile ("sti");

    // Initialize Physical Memory Allocation
    PageAllocator::init(
        efi_mem_map_phys,
        efi_mem_map_count,
        efi_mem_map_element_size,
        kernel_base_phys,
        used_phys_pages_count
    );

    // Early parsing of important ACPI info
    auto acpi_tables = ACPI::parse(rsdp_phys);

    // Init LAPIC Interrupts
    APIC::init();

    // Enumerate PCI busses and initialize found devices if we have a builtin kernel driver
    PCI::register_mcfg_table(acpi_tables.mcfg_phys);

    // TODO: Implement the rest of the kernel bootstrap routine
    // - Boot up other cores on the system (before enumerating PCI devices)
    // - Panic if we don't have a network card device (after enumerating PCI devices)
    // - Download a file with tFTP to be used as the init process
    // - Map the file and start the schedueler

    // TODO: Other Kernel/OS features
    // - Graphics stack with VGA backend
    // - IOMMU for PCI device separation

    // TODO: Delete this, but only after the kernel has other uscases of runtime allocation
    for (usize i = 0; i < 5; i++) {
        void* mem = kalloc(1);
        print("Allocated memory: {xh}\n", reinterpret_cast<u64>(mem));
        kfree(mem);
    }

    // Leave the Early Kernel and launch the Schedueler
    // TODO: Flesh out the runtime kernel with more syscalls and stuff
    Schedueler::work();
}

