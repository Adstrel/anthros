#pragma once
#include "Schedueler.hpp"


namespace GDT {

struct Entry {
    u64 SegmentLimitLower : 16;
    u64 BaseAddressLower : 24;
    u64 Accessed : 1;
    u64 R_W : 1;
    u64 C_E : 1;
    u64 Type : 2;
    u64 DPL : 2;
    u64 Present : 1;
    u64 SegmentLimitHigher : 4;
    u64 AVL : 1;
    u64 Long : 1;
    u64 D : 1;
    u64 G : 1;
    u64 BaseAddressHigher : 8;
};

// Can't be read only, bc the processor will write the 'Accessed' bit!
constinit Entry GDT[] = {
    // Null descriptor
    Entry{},
    // Supervisor Code descriptor
    Entry{
        .SegmentLimitLower = 0xffff,
        .BaseAddressLower = 0x0,
        .Accessed = 0,
        .R_W = 1,
        .C_E = 0,
        .Type = 0b11,
        .DPL = 0b00,
        .Present = 1,
        .SegmentLimitHigher = 0xf,
        .AVL = 0,
        .Long = 1,
        .D = 0,
        .G = 0,
        .BaseAddressHigher = 0x0
    },
    // Supervisor Data descriptor
    Entry{
        .SegmentLimitLower = 0xffff,
        .BaseAddressLower = 0x0,
        .Accessed = 0,
        .R_W = 1,
        .C_E = 0,
        .Type = 0b10,
        .DPL = 0b00,
        .Present = 1,
        .SegmentLimitHigher = 0xf,
        .AVL = 0,
        .Long = 0,
        .D = 0,
        .G = 0,
        .BaseAddressHigher = 0x0
    },
    // Userspace Data descriptor
    Entry{
        .SegmentLimitLower = 0xffff,
        .BaseAddressLower = 0x0,
        .Accessed = 0,
        .R_W = 1,
        .C_E = 0,
        .Type = 0b10,
        .DPL = 0b11,
        .Present = 1,
        .SegmentLimitHigher = 0xf,
        .AVL = 0,
        .Long = 0,
        .D = 0,
        .G = 0,
        .BaseAddressHigher = 0x0
    },
    // Userspace Code descriptor
    Entry{
        .SegmentLimitLower = 0xffff,
        .BaseAddressLower = 0x0,
        .Accessed = 0,
        .R_W = 1,
        .C_E = 0,
        .Type = 0b11,
        .DPL = 0b11,
        .Present = 1,
        .SegmentLimitHigher = 0xf,
        .AVL = 0,
        .Long = 1,
        .D = 0,
        .G = 0,
        .BaseAddressHigher = 0x0
    },
    // System Segment (TSS) Descriptor lower part
    Entry{

    },
    // System Segment (TSS) Descriptor higher part
    Entry{

    }
};

struct GDTR {
    u16 Limit;
    const Entry* Base;
} __attribute__((packed));

constexpr const __attribute__((aligned(0x10))) GDTR gdtr{
    .Limit = sizeof(GDT) - 1,
    .Base = GDT
};

inline void init() {
    // Set up the TSS descriptor
    // TODO: Have it be set up at compile time
    GDT[countof(GDT) - 2] = {
        .SegmentLimitLower = sizeof(Schedueler::TSS) - 1,
        .BaseAddressLower = reinterpret_cast<u64>(&Schedueler::cpu0_tss) & 0xff'ffff,
        .Accessed = 1,
        .R_W = 0,
        .C_E = 0,
        .Type = 0b01,
        .DPL = 0b11,
        .Present = 1,
        .SegmentLimitHigher = 0,
        .AVL = 0,
        .Long = 0,
        .D = 0,
        .G = 0,
        .BaseAddressHigher = (reinterpret_cast<u64>(&Schedueler::cpu0_tss) >> 24) & 0xff
    };
    GDT[countof(GDT) - 1] = {
        .SegmentLimitLower = (reinterpret_cast<u64>(&Schedueler::cpu0_tss) >> 32) & 0xffff,
        .BaseAddressLower = static_cast<u32>(reinterpret_cast<u64>(&Schedueler::cpu0_tss) >> 48) & 0xffff,
        // The rest are all zeroes
        .Accessed = 0,
        .R_W = 0,
        .C_E = 0,
        .Type = 0,
        .DPL = 0,
        .Present = 0,
        .SegmentLimitHigher = 0,
        .AVL = 0,
        .Long = 0,
        .D = 0,
        .G = 0,
        .BaseAddressHigher = 0
    };

    // Swap the GDT for the one our Kernel controls
    asm volatile ("lgdt (%0)" : : "r"(&gdtr));

    // Set the 'cs' (code segment) register to the second descriptor in our GDT.
    // To set the 'cs' register we have to do a far return
    asm volatile (
        "push  $8;"  // Code segment selector we want to load (8 bytes from start of GDT)
        "pushq $1f;" // Address of a label to return to
        "retfq;"
        "1:"
    );

    // Set the data segment registers to the Data Descriptor
    asm volatile ("mov %0, %%ds" : : "r"(sizeof(u64) * 2));
    asm volatile ("mov %0, %%es" : : "r"(sizeof(u64) * 2));
    asm volatile ("mov %0, %%fs" : : "r"(sizeof(u64) * 2));
    asm volatile ("mov %0, %%gs" : : "r"(sizeof(u64) * 2));
    asm volatile ("mov %0, %%ss" : : "r"(sizeof(u64) * 2));
}

} // namespace GDT
