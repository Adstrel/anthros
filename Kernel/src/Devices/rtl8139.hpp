#pragma once
#include "common.hpp"

namespace Device::rtl8139 {

void init(u64 base_addr, usize bus_index, usize device_index);

} // namespace Device::rtl8139
