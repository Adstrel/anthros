#include "qemu_virtual_video_controller.hpp"
#include "PCI.hpp"
#include "print.hpp"


namespace QemuVirtVideoController {

struct MMIO_regs {
    u8 edid_data_blob[0x400];

    u16 vga_io_ports[0x10];

    u8 rsrvd1[0x100 - 0x20];

    u16 bochs_dspi_interface[0xb];

    u8 rsrvd2[0x100 - 0x16];

    u32 qemu_extended_reg_region_size;
    u32 qemu_extended_reg_framebuffer_endianness;
};

static_assert(offsetof(MMIO_regs, edid_data_blob) == 0x0);
static_assert(sizeof(MMIO_regs::edid_data_blob) == 0x400);

static_assert(offsetof(MMIO_regs, vga_io_ports) == 0x400);
static_assert(sizeof(MMIO_regs::vga_io_ports) == 0x20);

static_assert(offsetof(MMIO_regs, bochs_dspi_interface) == 0x500);
static_assert(sizeof(MMIO_regs::bochs_dspi_interface) == 0x16);

static_assert(offsetof(MMIO_regs, qemu_extended_reg_region_size) == 0x600);
static_assert(sizeof(MMIO_regs::qemu_extended_reg_region_size) == 0x4);

static_assert(offsetof(MMIO_regs, qemu_extended_reg_framebuffer_endianness) == 0x604);
static_assert(sizeof(MMIO_regs::qemu_extended_reg_framebuffer_endianness) == 0x4);

u16* g_framebuffer;
MMIO_regs* g_mmio_regs;

void init(u64 base_addr, usize bus_index, usize device_index) {
    const usize function_index = 0;
    const u64 pci_config_physical_base_address = base_addr + (
        (bus_index << 20) |
        (device_index << 15) |
        (function_index << 12)
    );

    PCI::CommonHeader* common_header = reinterpret_cast<PCI::CommonHeader*>(pci_config_physical_base_address);
    const char* device_string = PCI::pci_device_to_string(0x1234, 0x1111);

    if (!device_string) {
        // Should not be reachable
        // TODO: Refactor everything, so we don't have to have this dummy check here
        return;
    }

    const bool is_common_header_correct =
        common_header->VendorID == 0x1234 &&
        common_header->DeviceID == 0x1111 &&
        common_header->HeaderType == 0 &&
        common_header->ClassCode == 3 &&
        common_header->Subclass == 0 &&
        common_header->ProgIF == 0 &&
        common_header->RevisionID >= 2;

    if (!is_common_header_correct) {
        print("\t+ Incorrect PCI common header for device \"{}\"!\n", device_string);
        return;
    }

    PCI::HeaderType_0* header = reinterpret_cast<PCI::HeaderType_0*>(common_header);

    // BAR0 has physical address of the frame buffer (16 MiB in size by default)
    // TODO: Check the actual size at runtime
    u64 framebuffer_phys_bar = header->BAR0;
    if ((framebuffer_phys_bar & 1) != 0) {
        print("\t+ Incorrect BAR0 for device \"{}\"!\n", device_string);
        return;
    }

    // BAR2 has physical address of MMIO registers (4096 bytes in size)
    u64 mmio_regs_phys_bar = header->BAR2;
    if ((mmio_regs_phys_bar & 1) != 0) {
        print("\t+ Incorrect BAR2 for device \"{}\"!\n", device_string);
        return;
    }

    // TODO: Memory map these addresses to higher half of the address range
    g_framebuffer = reinterpret_cast<u16*>(framebuffer_phys_bar & ~static_cast<u64>(0xf));
    g_mmio_regs = reinterpret_cast<MMIO_regs*>(mmio_regs_phys_bar & ~static_cast<u64>(0xf));

    // Write into the frame buffer (looks like OVMF has set up some kind of color mode, not text mode)
    // TODO: Write an actual useful VGA driver? Or semi-vga I guess, since we use the QEMU virtual controller
    // TODO: We should also have a vanilla VGA driver as well, since that is what most hardware supports
    for (usize i = 0; i < 0x8000; i++) {
        g_framebuffer[i] = 0xff;
    }

    print("\t+ PCI device \"{}\" successfully initialized!\n", device_string);
}

} // namespace QemuVirtVideoController
