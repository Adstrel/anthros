#pragma once
#include "common.hpp"


namespace QemuVirtVideoController {

void init(u64 base_addr, usize bus_index, usize device_index);

} // namespace QemuVirtVideoController
