#include "rtl8139.hpp"
#include "print.hpp"
#include "PCI.hpp"
#include "paging.hpp"
#include "x86.hpp"
#include "PageAllocator.hpp"

// https://theretroweb.com/chip/documentation/rtl8100c-l-6418bc0447a42694897038.pdf
namespace Device::rtl8139 {

struct Regs {
    // [00:00] ID Register 0
    u8 IDR0;

    // [01:01] ID Register 1
    u8 IDR1;

    // [02:02] ID Register 2
    u8 IDR2;

    // [03:03] ID Register 3
    u8 IDR3;

    // [04:04] ID Register 4
    u8 IDR4;

    // [05:05] ID Register 5
    u8 IDR5;
    u8 _rsrvd0[2];

    // [08:08] Multicast Address Register 0
    u8 MAR0;

    // [09:09] Multicast Address Register 1
    u8 MAR1;

    // [0A:0A] Multicast Address Register 2
    u8 MAR2;

    // [0B:0B] Multicast Address Register 3
    u8 MAR3;

    // [0C:0C] Multicast Address Register 4
    u8 MAR4;

    // [0D:0D] Multicast Address Register 5
    u8 MAR5;

    // [0E:0E] Multicast Address Register 6
    u8 MAR6;

    // [0F:0F] Multicast Address Register 7
    u8 MAR7;

    // [10:13] Transmit Status of Descriptor 0
    u32 TSD0;

    // [14:17] Transmit Status of Descriptor 1
    u32 TSD1;

    // [18:1B] Transmit Status of Descriptor 2
    u32 TSD2;

    // [1C:1F] Transmit Status of Descriptor 3
    u32 TSD3;

    // [20:23] Transmit Start Address of Descriptor 0
    u32 TSAD0;

    // [24:27] Transmit Start Address of Descriptor 1
    u32 TSAD1;

    // [28:2B] Transmit Start Address of Descriptor 2
    u32 TSAD2;

    // [2C:2F] Transmit Start Address of Descriptor 3
    u32 TSAD3;

    // [30:33] Receive (Rx) Buffer Start Address
    u32 RBSTART;

    // [34:35] Early Receive (Rx) Byte Count Register
    u16 ERBCR;

    // [36:36] Early Rx Status Register
    u8 ERSR;

    // [37:37] Command Register
    u8 CR;

    // [38:39] Current Address of Packet Read
    u16 CAPR;

    // [3A:3B] Current Buffer Address
    u16 CBR;

    // [3C:3D] Interrupt Mask Register
    u16 IMR;

    // [3E:3F] Interrupt Status Register
    u16 ISR;

    // [40:43] Transmit (Tx) Configuration Register
    u32 TCR;

    // [44:47] Receive (Rx) Configuration Register
    u32 RCR;

    // [48:4B] Timer Count Register
    u32 TCTR;

    // [4C:4F] Missed Packet Counter
    u32 MPC;

    // [50:50] 93C46 Command Register
    u8 CR9346;

    // [51:51] Configuration Register 0
    u8 CONFIG0;

    // [52:52] Configuration Register 1
    u8 CONFIG1;
    u8 _rsrvd1[1];

    // [54:57] Timer Interrupt Register
    u32 TimerInt;

    // [58:58] Media Status Register
    u8 MSR;

    // [59:59] Configuration register 3
    u8 CONFIG3;

    // [5A:5A] Configuration register 4
    u8 CONFIG4;
    u8 _rsrvd2[1];

    // [5C:5D] Multiple Interrupt Select
    u16 MULINT;

    // [5E:5E] PCI Revision ID
    u8 RERID;
    u8 _rsrvd3[1];

    // [60:61] Transmit Status of All Descriptors
    u16 TSAD;

    // [62:63] Basic Mode Control Register
    u16 BMCR;

    // [64:65] Basic Mode Status Register
    u16 BMSR;

    // [66:67] Auto-Negotiation Advertisement Register
    u16 ANAR;

    // [68:69] Auto-Negotiation Link Partner Register
    u16 ANLPAR;

    // [6A:6B] Auto-Negotiation Expansion Register
    u16 ANER;

    // [6C:6D] Disconnect Counter
    u16 DIS;

    // [6E:6F] False Carrier Sense Counter
    u16 FCSC;

    // [70:71] N-way Test Register
    u16 NWAYTR;

    // [72:73] RX_ER Counter
    u16 REC;

    // [74:75] CS Configuration Register
    u16 CSCR;
    u16 _rsrvd4;

    // [78:7B] PHY Parameter 1
    u32 PHY1_PARM;

    // [7C:7F] Twister Parameter
    u32 TW_PARM;

    // [80:80] PHY Parameter 2
    u8 PHY2_PARM;
    u8 _rsrvd5[3];

    // [84:84] Power Management CRC register 0 for wakeup frame 0
    u8 CRC0;

    // [85:85] Power Management CRC register 0 for wakeup frame 1
    u8 CRC1;

    // [86:86] Power Management CRC register 0 for wakeup frame 2
    u8 CRC2;

    // [87:87] Power Management CRC register 0 for wakeup frame 3
    u8 CRC3;

    // [88:88] Power Management CRC register 0 for wakeup frame 4
    u8 CRC4;

    // [89:89] Power Management CRC register 0 for wakeup frame 5
    u8 CRC5;

    // [8A:8A] Power Management CRC register 0 for wakeup frame 6
    u8 CRC6;

    // [8B:8B] Power Management CRC register 0 for wakeup frame 7
    u8 CRC7;

    // [8C:93] Power Management Wakeup frame 0 (64-bit)
    u64 Wakeup0;

    // [94:9B] Power Management Wakeup frame 1 (64-bit)
    u64 Wakeup1;

    // [9C:A3] Power Management Wakeup frame 2 (64-bit)
    u64 Wakeup2;

    // [A4:AB] Power Management Wakeup frame 3 (64-bit)
    u64 Wakeup3;

    // [AC:B3] Power Management Wakeup frame 4 (64-bit)
    u64 Wakeup4;

    // [B4:BB] Power Management Wakeup frame 5 (64-bit)
    u64 Wakeup5;

    // [BC:C3] Power Management Wakeup frame 6 (64-bit)
    u64 Wakeup6;

    // [C4:CB] Power Management Wakeup frame 7 (64-bit)
    u64 Wakeup7;

    // [CC:CC] LSB of the mask byte of wakeup frame 0 within offset 12 to 75
    u8 LSBCRC0;

    // [CD:CD] LSB of the mask byte of wakeup frame 1 within offset 12 to 75
    u8 LSBCRC1;

    // [CE:CE] LSB of the mask byte of wakeup frame 2 within offset 12 to 75
    u8 LSBCRC2;

    // [CF:CF] LSB of the mask byte of wakeup frame 3 within offset 12 to 75
    u8 LSBCRC3;

    // [D0:D0] LSB of the mask byte of wakeup frame 4 within offset 12 to 75
    u8 LSBCRC4;

    // [D1:D1] LSB of the mask byte of wakeup frame 5 within offset 12 to 75
    u8 LSBCRC5;

    // [D2:D2] LSB of the mask byte of wakeup frame 6 within offset 12 to 75
    u8 LSBCRC6;

    // [D3:D3] LSB of the mask byte of wakeup frame 7 within offset 12 to 75
    u8 LSBCRC7;
    u8 _rsrvd6[4];

    // [D8:D8] Configuration register 5
    u8 Config5;
    u8 _rsrvd7[39];
} PACKED;

static_assert(offsetof(Regs, MAR0) == 0x08);
static_assert(offsetof(Regs, TSD0) == 0x10);
static_assert(offsetof(Regs, CR) == 0x37);
static_assert(offsetof(Regs, TCR) == 0x40);

static_assert(offsetof(Regs, CONFIG0) == 0x51);
static_assert(offsetof(Regs, RERID) == 0x5E);
static_assert(offsetof(Regs, ANAR) == 0x66);
static_assert(offsetof(Regs, PHY1_PARM) == 0x78);

static_assert(offsetof(Regs, CRC0) == 0x84);
static_assert(offsetof(Regs, CRC7) == 0x8B);
static_assert(offsetof(Regs, Wakeup0) == 0x8C);
static_assert(offsetof(Regs, Config5) == 0xD8);

static_assert(sizeof(Regs) == 0x100);

// TODO: Make a general kernel subsystem for allocating VM MMIO
constexpr usize vm_addr_mmio  = 0xffff'8010'0000'0000;
constexpr usize vm_addr_rxbuf = 0xffff'8010'0000'1000;

static u32 rx_buf_phys_addr;

// TODO: This is PCI generic stuff so make this accessible for other PCI devices too
struct CapabilityHeader {
    u8 id;
    u8 next_offset;
};

struct CapabilityMsi {
    CapabilityHeader header;
    u16 msg_control;
    u32 msg_addr_low;
    u32 msg_addr_high;
    u16 msg_data;
    u16 rsrvd;
    u32 mask;
    u32 pending;
};

void init(u64 base_addr, usize bus_index, usize device_index) {
    println("[rtl8139]: Beginning init");

    const usize function_index = 0;
    const u64 pci_config_physical_base_address = base_addr + (
        (bus_index << 20) |
        (device_index << 15) |
        (function_index << 12)
    );

    // TODO:
    // - Figure out where the BARs are mapped to
    // - Walk the PCI capability list

    volatile PCI::HeaderType_0* header0 = reinterpret_cast<PCI::HeaderType_0*>(pci_config_physical_base_address);
    println("[rtl8139]: PCI Command = {xh}", header0->common.Command);
    println("[rtl8139]: PCI Status = {xh}", header0->common.Status);
    println("[rtl8139]: PCI BAR0 = {xh}", header0->BAR0);
    println("[rtl8139]: PCI BAR1 = {xh}", header0->BAR1);
    println("");

    header0->common.Command = 0x0006u;
    header0->BAR0 = 0;
    println("[rtl8139]: PCI Command = {xh}", header0->common.Command);
    println("[rtl8139]: PCI Status = {xh}", header0->common.Status);
    println("[rtl8139]: PCI BAR0 = {xh}", header0->BAR0);
    println("[rtl8139]: PCI BAR1 = {xh}", header0->BAR1);
    println("");

    // Lets just hope the BIOS configured the physical MMIO address well enough for us
    // TODO: Maybe handle low bits of BARs correctly?
    //       Even if we already expect them to be zero, then we should at least
    //       diagnose these things separately.
    if (header0->BAR1 % 0x1000 != 0) {
        println("[rtl8139]: "
            "Cannot initialize the RTL8139 network device, "
            "because the BIOS didn't page align its MMIO "
            "registers during PCI configuration"
        );
        return;
    }

    // TODO: At the moment we have a hardcoded VM address specifically for this device's MMIO,
    //       but that should probably be allocated from some VM manager.
    // TODO: We should manage page tables better.
    //       We cannot rely on them being flat mapped -- this might not be true even
    //       during Kernel initialization, not even talking after it.
    u64 page_table_addr = read_cr3();
    map_pa_to_va(page_table_addr, header0->BAR1, vm_addr_mmio, IsWritable::Yes, IsExecutable::No, IsUsermode::No);

    // Flush the tlb
    asm volatile ("invlpg (%0)" : : "r"(vm_addr_mmio) : "memory");

    // ALRIGHT, the registers should be mapped to a reasonable virtual address now...
    println("[rtl8139]: Mapped the device MMIO regs to VA 0xffff'8010'0000'0000");

    // Lets read some stuff out now
    volatile Regs* mmio = reinterpret_cast<volatile Regs*>(vm_addr_mmio);

    println("[rtl8139]: IDR0 = {xh}", mmio->IDR0);
    println("[rtl8139]: IDR1 = {xh}", mmio->IDR1);
    println("[rtl8139]: IDR2 = {xh}", mmio->IDR2);
    println("[rtl8139]: IDR3 = {xh}", mmio->IDR3);
    println("[rtl8139]: IDR4 = {xh}", mmio->IDR4);
    println("[rtl8139]: IDR5 = {xh}", mmio->IDR5);
    println("[rtl8139]: PCI Revision ID = {xh}", mmio->RERID);
    println("[rtl8139]: Transmit Configuration Register = {xh}", mmio->TCR);
    println("[rtl8139]: Command Register = {xh}", mmio->CR);

    println("[rtl8139]: CONFIG0 = {xh}", mmio->CONFIG0);
    println("[rtl8139]: CONFIG1 = {xh}", mmio->CONFIG1);

    // Power on the device
    mmio->CONFIG1 = (1 << 4);

    // Reset the device
    mmio->CR = (1u << 4);

    // Wait until the device is done resetting itself
    usize count = 0;
    while ((mmio->CR & (1u << 4)) != 0) {
        count++;
    }
    println("[rtl8139]: Re-initialized with wait count {}!", count);

    // Allocate 3 pages of continuous physical memory under 4 GiB
    // TODO: Handle all of this correctly
    u64 first_page = PageAllocator::get_page();;
    u64 second_page = PageAllocator::get_page();
    u64 third_page = PageAllocator::get_page();

    const bool valid_pages =
        first_page <= 0xFFFF'FFFFul &&
        second_page <= 0xFFFF'FFFFul &&
        third_page <= 0xFFFF'FFFFul &&
        second_page == first_page + 0x1000 &&
        third_page == second_page + 0x1000;
    if (!valid_pages) {
        panic("Cannot initialize the rtl8139 device!");
    }

    map_pa_to_va(page_table_addr, first_page, vm_addr_rxbuf, IsWritable::Yes, IsExecutable::No, IsUsermode::No);
    asm volatile ("invlpg (%0)" : : "r"(vm_addr_rxbuf) : "memory");

    // Configure the Rx ring buffer
    rx_buf_phys_addr = static_cast<u32>(first_page);
    mmio->RBSTART = static_cast<u32>(first_page);

    // Configure the Rx operation
    mmio->RCR = 0x0000'008fu;

    // Enable Rx and Tx operation
    mmio->CR = 0x0C;

    // Walk the configuration space capabiliry list and look for the MSI capability (0x05)
    const bool has_capability_list = (header0->common.Status & (1 << 4)) != 0;
    if (!has_capability_list) {
        println("[rtl8139]: Device was expected to have a capability list, but does not have it!");

        // TODO: We end up over here. I guess this device does not support MSIs?
        return;
    }

    const u8 cap_off = header0->CapabilitiesPointer & static_cast<u8>(~0x3u);
    println("[rtl8139]: Capability pointer = {xh}", cap_off);
    if (cap_off == 0) {
        return;
    }

    volatile CapabilityHeader* pCap = reinterpret_cast<volatile CapabilityHeader*>(pci_config_physical_base_address + cap_off);
    println("Cap ID: {xh}, Cap next offset: {xh}", pCap->id, pCap->next_offset);

    // Wait until we receive a packet
//    u32 status = 0;
//    while ((status = mmio->ISR) == 0);
//    println("[rtl8139]: Status = {xh}", status);
//    if ((status & 1) != 0) {
//        println("[rtl8139]: Current Address of Packet Read = {xh}", mmio->CAPR);
//        const u16 len = mmio->CBR;
//        println("[rtl8139]: Current Buffer Address = {xh}", len);
//        const u16 rx_status = *reinterpret_cast<volatile const u16*>(vm_addr_rxbuf);
//        println("[rtl8139]: Recieve Status = {xh}", rx_status);
//
//        for (usize i = 0; i < len; i++) {
//            print("{x} ", reinterpret_cast<volatile const u8*>(vm_addr_rxbuf)[i]);
//            print(i % 8 == 7 ? "\n" : " ");
//        }
//        println("");
//    }

    // TODO:
    // - Walk the PCI capability list and enable MSIs for this device
    // - Figure out how to use MSIs in general
    // https://wiki.osdev.org/RTL8139
    // https://wiki.osdev.org/Network_Stack
    // https://wiki.osdev.org/PCI
    // https://wiki.osdev.org/APIC

    // TODO:
    // - Reset the device
    // - Create Rx FIFO
    // - Create Tx FIFO
    // - Try to get a DHCP lease
    // - Try to send an ARP request
    // - Try to download a file from the QEMU TFTP server

    // Example of a packet that QEMU seems to keep sending:
    // 01  00  72  00  33  33  00  00
    // 00  01  52  56  00  00  00  02
    // 86  dd  60  00  00  00  00  38
    // 3a  ff  fe  80  00  00  00  00
    // 00  00  00  00  00  00  00  00
    // 00  02  ff  02  00  00  00  00
    // 00  00  00  00  00  00  00  00
    // 00  01  86  00  15  5e  40  00
    // 07  08  00  00  00  00  00  00
    // 00  00  01  01  52  56  00  00
    // 00  02  03  04  40  c0  00  01
    // 51  80  00  00  38  40  00  00
    // 00  00  fe  c0  00  00  00  00
    // 00  00  00  00  00  00  00  00
    // 00  00  8c  f2  6c  74  00  00
}

} // namespace Device::rtl8139
