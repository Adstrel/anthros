#include "ACPI.hpp"
#include "print.hpp"
#include "APIC.hpp"


namespace ACPI {

static void check_rsdp(const RSDP* rsdp) {
    if (rsdp->Signature != ascii_to_uint64_t(u8"RSD PTR ")) {
        print("PANIC: ACPI RSDP structure has wrong signature!\n");
        while (true);
    }

    u8 checksum = 0;
    for (usize i = 0; i < 20; i++) {
        checksum += reinterpret_cast<const u8*>(rsdp)[i];
    }
    if (checksum != 0) {
        print("PANIC: ACPI RSDP structure has wrong checksum! Is {}, but should be 0!\n", checksum);
        while (true);
    }

    u8 ex_checksum = 0;
    for (usize i = 0; i < rsdp->Length; i++) {
        ex_checksum += reinterpret_cast<const u8*>(rsdp)[i];
    }
    if (ex_checksum != 0) {
        print("PANIC: ACPI RSDP structure has wrong Extended Checksum! Is {}, but should be 0!\n", ex_checksum);
        while (true);
    }

    if (rsdp->Length < sizeof(RSDP)) {
        print("PANIC: ACPI RSDP structure is older than supported! Length is {}, but should be {}!\n", rsdp->Length, sizeof(RSDP));
        while (true);
    }
}

static void check_xsdt(const XSDT* xsdt) {
    if (xsdt->header.Signature != ascii_to_uint32_t(u8"XSDT")) {
        print("PANIC: ACPI XSDT structure has wrong signature!\n");
        while (true);
    }

    u8 checksum = 0;
    for (usize i = 0; i < xsdt->header.Length; i++) {
        checksum += reinterpret_cast<const u8*>(xsdt)[i];
    }
    if (checksum != 0) {
        print("PANIC: ACPI XSDT structure has wrong checksum!\n");
        while (true);
    }
}

ImportantTables parse(const u64 rsdp_phys) {
    ImportantTables tables{};

    tables.rsdp_phys = rsdp_phys;
    RSDP* rsdp = reinterpret_cast<RSDP*>(rsdp_phys);
    check_rsdp(rsdp);

    tables.xsdt_phys = rsdp->XsdtAddress;
    XSDT* xsdt = reinterpret_cast<XSDT*>(rsdp->XsdtAddress);
    check_xsdt(xsdt);

    {
        u64* entry = reinterpret_cast<u64*>(xsdt + 1);
        print("XSDT addr: {xh}, First Entry addr: {xh}, Length: {}\n", reinterpret_cast<u64>(xsdt), reinterpret_cast<u64>(entry), xsdt->header.Length);
        while (reinterpret_cast<u8*>(entry) < reinterpret_cast<u8*>(xsdt) + xsdt->header.Length) {
            SDTHeader* table = reinterpret_cast<SDTHeader*>(*entry);
            print("XSDT Entry Signature: \"");
            u32 signature = table->Signature;
            for (usize i = 0; i < 4; i++) {
                out_dbg_char(static_cast<char>(signature & 0xff));
                signature >>= 8;
            }
            print("\"\n");

            switch (table->Signature) {
            case ascii_to_uint32_t(u8"FACP"):
                tables.facp_phys = *entry;
                break;
            case ascii_to_uint32_t(u8"APIC"):
                tables.madt_phys = *entry;
                APIC::parse_madt(*entry);
                break;
            case ascii_to_uint32_t(u8"MCFG"):
                tables.mcfg_phys = *entry;
                break;
            case ascii_to_uint32_t(u8"HPET"):
            case ascii_to_uint32_t(u8"WAET"):
            case ascii_to_uint32_t(u8"BGRT"):
                // Safe to ignore
                // TODO: Cache info from these tables (+ Unknown tables), so we can display their data after we boot up (just for fun/introspection)
                break;
            default: {
                print("Unknown XSDT Entry with signature \"");
                u32 signature = table->Signature;
                for (usize i = 0; i < 4; i++) {
                    out_dbg_char(static_cast<char>(signature & 0xff));
                    signature >>= 8;
                }
                print("\"!\n");
            }}

            entry++;
        }
    }

    if (!tables.FoundAll()) {
        print("PANIC: Did not find all important ACPI tables!\n");
        while (true);
    }

    return tables;
}

} // namespace ACPI
