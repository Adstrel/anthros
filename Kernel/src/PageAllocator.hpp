#pragma once
#include "common.hpp"


namespace PageAllocator {

u64 get_page();
u64 get_page_zeroed();
void free_page(const u64 phys_page);

void init(
    const u64 efi_mem_map_phys,
    const u64 efi_mem_map_count,
    const u64 efi_mem_map_element_size,
    const u64 kernel_base_phys,
    const u64 used_phys_page_count
);

} // namespace PageAllocator
