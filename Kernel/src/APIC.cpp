#include "APIC.hpp"
#include "IDT.hpp"
#include "x86.hpp"
#include "paging.hpp"


namespace APIC {

enum class Reg : u64 {
    APIC_ID_Register = 0x20, // DONE
    APIC_Version_Register = 0x30, // DONE
    Task_Priority_Register = 0x80,
    Arbitration_Priority_Register = 0x90,
    Processor_Priority_Register = 0xa0,
    End_of_Interrupt_Register = 0xb0,
    Remote_Read_Register = 0xc0,
    Logical_Destination_Register = 0xd0,
    Destination_Format_Register = 0xe0,
    Spurious_Interrupt_Vector_Register = 0xf0,
    InService_Register_0 = 0x100,
    InService_Register_1 = 0x110,
    InService_Register_2 = 0x120,
    InService_Register_3 = 0x130,
    InService_Register_4 = 0x140,
    InService_Register_5 = 0x150,
    InService_Register_6 = 0x160,
    InService_Register_7 = 0x170,
    Trigger_Mode_Register_0 = 0x180,
    Trigger_Mode_Register_1 = 0x190,
    Trigger_Mode_Register_2 = 0x1a0,
    Trigger_Mode_Register_3 = 0x1b0,
    Trigger_Mode_Register_4 = 0x1c0,
    Trigger_Mode_Register_5 = 0x1d0,
    Trigger_Mode_Register_6 = 0x1e0,
    Trigger_Mode_Register_7 = 0x1f0,
    Interrupt_Request_Register_0 = 0x200,
    Interrupt_Request_Register_1 = 0x210,
    Interrupt_Request_Register_2 = 0x220,
    Interrupt_Request_Register_3 = 0x230,
    Interrupt_Request_Register_4 = 0x240,
    Interrupt_Request_Register_5 = 0x250,
    Interrupt_Request_Register_6 = 0x260,
    Interrupt_Request_Register_7 = 0x270,
    Error_Status_Register = 0x280,
    Interrupt_Command_Register_Low = 0x300,
    Interrupt_Command_Register_High = 0x310,
    Timer_Local_Vector_Table_Entry = 0x320,
    Termal_Local_Vector_Table_Entry = 0x330,
    Performance_Counter_Local_Vector_Table_Entry = 0x340,
    Local_Interrupt_0_Vector_Table_Entry = 0x350,
    Local_Interrupt_1_Vector_Table_Entry = 0x360,
    Error_Vector_Table_Entry = 0x370,
    Timer_Initial_Count_Register = 0x380,
    Timer_Current_Count_Register = 0x390,
    Timer_Divide_Configuration_Register = 0x3e0,
    Extended_APIC_Feature_Register = 0x400, // DONE
    Extended_APIC_Control_Register = 0x410, // DONE
    Specific_End_of_Interrupt_Register = 0x420,
    Interrupt_Enable_Register_0 = 0x480,
    Interrupt_Enable_Register_1 = 0x490,
    Interrupt_Enable_Register_2 = 0x4a0,
    Interrupt_Enable_Register_3 = 0x4b0,
    Interrupt_Enable_Register_4 = 0x4c0,
    Interrupt_Enable_Register_5 = 0x4d0,
    Interrupt_Enable_Register_6 = 0x4e0,
    Interrupt_Enable_Register_7 = 0x4f0,
    Extended_Interrupt_0_Local_Vector_Table_Register = 0x500,
    Extended_Interrupt_1_Local_Vector_Table_Register = 0x510,
    Extended_Interrupt_2_Local_Vector_Table_Register = 0x520,
    Extended_Interrupt_3_Local_Vector_Table_Register = 0x530,
};

namespace _Reg {

// APIC ID Register
struct AIR {
    static constexpr u32 GetOffset() { return 0x20; }

    // Reserved
    // Bits: 0-23, Access Type: MBZ
    u32 reserved_0_23 : 24 = 0;

    // APIC ID
    // Bits: 24-31, Access Type: R/W
    u32 AID : 8;
};

// APIC Version Register
struct AVR {
    static constexpr u32 GetOffset() { return 0x30; }

    // Version
    // Bits: 0-7, Access Type: RO
    u32 VER : 8;

    // Reserved
    // Bits: 8-15, Access Type: MBZ
    u32 reserved_8_15 : 8;

    // Max LVT Entries
    // Bits: 16-23, Access Type: RO
    // Note: Stpres the Max number of Local Vector Table Entries *minus one*
    u32 MLE : 8;

    // Reserved
    // Bits: 24-30, Access Type: MBZ
    u32 reserved_24_30 : 7;

    // Extended APIC Register Space Present
    // Bit: 31, Access Type: RO
    u32 EAS : 1;
};

// Extended APIC Feature Register
struct EAFR {
    static constexpr u32 GetOffset() { return 0x400; }

    // Interrupt Enable Register Capable
    // Bit: 0, Access Type: RO
    u32 INC : 1;

    // Specific End of Interrupt Capable
    // Bit: 1, Access Type: RO
    u32 SNIC : 1;

    // Extended APIC ID Capable
    // Bit: 2, Access Type: RO
    u32 XAIDC : 1;

    // Reserved
    // Bits: 3-15, Access Type: MBZ
    u32 reserved_3_15 : 13;

    // Extended LVT Count
    // Bits: 16-23, Access Type: RO
    u32 XLC : 8;

    // Reserved
    // Bits: 24-31, Access Type: MBZ
    u32 reserved_24_31 : 8;
};

// Extended APIC Control Register
struct EACR {
    static constexpr u32 GetOffset() { return 0x410; }

    // Enable Interrupt Enable Registers
    // Bit: 0, Access Type: R/W
    u32 IERN : 1;

    // Enable SEOI Generation
    // Bit: 1, Access Type: R/W
    u32 SN : 1;

    // Extended APIC ID Enable
    // Bit: 2, Access Type: R/W
    u32 XAIDN : 1;

    // Reserved
    // Bits: 3-31, Access Type: MBZ
    u32 reserved_3_31 : 29;
};

// General Local Vector Table Register
// - Just a general format of a vetor table register -- no specific register has this format
struct LVTR {
    // Vector
    // Bits: 0-7, Access Type: R/W
    u32 VEC : 8;

    // Message Type
    // Bits: 8-10, Access Type: R/W
    u32 MT : 3;

    // Reserved
    // Bit: 11, Access Type: MBZ
    u32 reserved_11 : 1;

    // Delivery Status
    // Bit: 12, Access Type: RO
    u32 DS : 1;

    // Reserved
    // Bit: 13, Access Type: MBZ
    u32 reserved_13 : 1;

    // Remote IRR
    // Bit: 14, Access Type: RO
    u32 RIR : 1;

    // Trigger Mode
    // Bit: 15, Access Type: R/W
    u32 TGM : 1;

    // Mask
    // Bit: 16, Access Type: R/W
    u32 M : 1;

    // Timer Mode
    // Bit: 17, Access Type: R/W
    u32 TMM : 1;

    // Reserved
    // Bits: 18-31, Access Type: MBZ
    u32 reserved_18_31 : 14;
};

// APIC Timer Local Vector Table Register
struct ATLVTR {
    static constexpr u32 GetOffset() { return 0x320; }

    // Vector
    // Bits: 0-7, Access Type: R/W
    u32 VEC : 8;

    // Reserved
    // Bits: 8-11, Access Type: MBZ
    u32 reserved_8_11 : 4;

    // Delivery Status
    // Bit: 12, Access Type: RO
    u32 DS : 1;

    // Reserved
    // Bits: 13-15, Access Type: MBZ
    u32 reserved_13_15 : 3;

    // Mask
    // Bit: 16, Access Type: R/W
    u32 M : 1;

    // Timer Mode
    // Bit: 17, Access Type: R/W
    u32 TMM : 1;
};

} // namespace _Reg

// TODO: Remember more information about the present hardware

// We will map the APIC registers to this virtual address
constexpr u64 c_APIC_base_va   = 0xffff'8001'0000'1000;
constexpr u64 c_IOAPIC_base_va = 0xffff'8001'0000'3000;

// TODO: structs for different registers
template<Reg REG>
ALWAYS_INLINE GENERAL_REGS static u32 ReadReg() {
    return *reinterpret_cast<const volatile u32*>(
        c_APIC_base_va + static_cast<u64>(REG)
    );
}

template<Reg REG>
ALWAYS_INLINE GENERAL_REGS static void WriteReg(const u32 value) {
    volatile u32* const reg_addr =
        reinterpret_cast<volatile u32*>( c_APIC_base_va + static_cast<u64>(REG) );

    *reg_addr = value;
}

static bool g_x2APIC_present = false;
bool is_x2APIC_present() {
    return g_x2APIC_present;
}

void parse_madt(u64 madt_phys) {
    ACPI::MADT* madt = reinterpret_cast<ACPI::MADT*>(madt_phys);

    print("MADT LocalInterruptControllerAddress: {xh}\n", madt->LocalInterruptControllerAddress);

    // Map the LAPIC physical address to a predetermined virtual address in the higher half of the virtual address space
    map_pa_to_va(read_cr3(), madt->LocalInterruptControllerAddress, c_APIC_base_va, IsWritable::Yes, IsExecutable::No, IsUsermode::No);

    g_x2APIC_present = GetBits<21>(cpuid(1).ecx) == 1;
    print("x2APIC Supported: {}\n", g_x2APIC_present ? "Yes" : "No");

    u64 entry_addr = reinterpret_cast<u64>(madt + 1);
    while (entry_addr < madt_phys + madt->header.Length) {
        ACPI::InterruptControllerStructure* entry = reinterpret_cast<ACPI::InterruptControllerStructure*>(entry_addr);
        print("MADT entry - Length: {}, Type: \"{}\"", entry->Length, entry->type);

        switch (entry->type) {
        case ACPI::InterruptControllerStructure::Type::ProcessorLocalAPIC: {
            print(", ACPI_ProcessorUID: {}, APIC_ID: {}, En: {}, OC: {}\n",
                entry->structure.ProcessorLocalApic.ACPI_ProcessorUID,
                entry->structure.ProcessorLocalApic.APIC_ID,
                entry->structure.ProcessorLocalApic.Flags.Enabled,
                entry->structure.ProcessorLocalApic.Flags.OnlineCapable
            );
        } break;
        case ACPI::InterruptControllerStructure::Type::IO_APIC: {
            print(", IO_APIC_ID: {}, IO_APIC_Address: {xh}, GSIBase: {}\n",
                entry->structure.IO_APIC.IO_APIC_ID,
                entry->structure.IO_APIC.IO_APIC_Address,
                entry->structure.IO_APIC.GlobalSystemInterruptBase
            );
        } break;
        case ACPI::InterruptControllerStructure::Type::InterruptSourceOverride: {
            print(", Bus: {}, Source: {}, GSI: {}, Pol: {}, Trig: {}\n",
                entry->structure.InterruptSourceOverride.Bus,
                entry->structure.InterruptSourceOverride.Source,
                entry->structure.InterruptSourceOverride.GlobalSystemInterrupt,
                entry->structure.InterruptSourceOverride.Flags.Polarity,
                entry->structure.InterruptSourceOverride.Flags.TriggerMode
            );
        } break;
        case ACPI::InterruptControllerStructure::Type::LocalApicNMI: {
            print(", ACPI_ProcessorUID: {}, LocalAPIC_Lint_n: {}, Pol: {}, Trig: {}\n",
                entry->structure.LocalApicNMI.ACPI_ProcessorUID,
                entry->structure.LocalApicNMI.LocalAPIC_Lint_n,
                entry->structure.LocalApicNMI.Flags.Polarity,
                entry->structure.LocalApicNMI.Flags.TriggerMode
            );
        } break;
        default:
            print("\n");
            break;
        }

        entry_addr += entry->Length;
    }
}

void init() {
    // Enable the Local APIC, if it isn't yet
    auto ApicBaseRegister = ReadMsr<Msr::APIC_BAR>();
    if (ApicBaseRegister.AE != 1) {
        ApicBaseRegister.AE = 1;
        WriteMsr<Msr::APIC_BAR>(ApicBaseRegister);
    }

    // Enable and use the x2APIC if it is present
    if (g_x2APIC_present) {
        // TODO
    }

    // Send spurious interrupts to IRQ 255 and Enable APIC to send interrupts
    WriteReg<Reg::Spurious_Interrupt_Vector_Register>(
        0xff     | // IRQ Vector
        (1 << 8) | // APIC Enable
        (0 << 9)   // Focus CPU Core Checking
    );

    // Ignore 'none' interrupts
    WriteReg<Reg::Task_Priority_Register>(0);

    // Init the Timer interrupts on IRQ 32
    WriteReg<Reg::Timer_Local_Vector_Table_Entry>(
        32        | // IRQ Vector
        (0 << 16) | // Mask
        (1 << 17)   // Periodic timer
    );

    // TODO:
    //      Divide Configuration Register (DCR) controls the counting rate of the APIC timer by dividing the
    //      CPU core clock by a programmable amount. See Figure 16-11. For the specific details on the
    //      implementation of the APIC timer base clock rate, see the BIOS and Kernel Developer’s Guide
    //      (BKDG) or Processor Programming Reference Manual applicable to your product.

    // Decrements the Counter every X Timer tics
    WriteReg<Reg::Timer_Divide_Configuration_Register>(
        0b0011 // Divide by 16
    );

    // Starts the Timer with this initial Counter value
    WriteReg<Reg::Timer_Initial_Count_Register>(
        0x0001'0000
    );
}

// TODO: This counter should be thread/core local -- each CPU core should have its own mutable copy
// TODO: Do these even have to be marked "volatile"? Probably not always.
//       The ISRs can make a normal read-modify-write operation, and code outside of ISRs can just make a volatile read.
static volatile u64 g_timer_count = 0;

extern "C" ISR void isr32(interrupt_frame* frame) {
    (void) frame;

    g_timer_count = g_timer_count + 1;

    // Tell the Local APIC, that we handled this interrupt
    WriteReg<Reg::End_of_Interrupt_Register>(0);
}

static volatile u64 g_spurious_count = 0;
extern "C" ISR void isr255(interrupt_frame* frame) {
    (void) frame;

    g_spurious_count = g_spurious_count + 1;

    // Tell the Local APIC, that we handled this interrupt
    WriteReg<Reg::End_of_Interrupt_Register>(0);
}

void print_timer_count() {
    // TODO: Disable interrupts, then read the globals, then re-enable interrupts, and only then print the read values
    print("Timer count: {}, Spurious count: {}\n", g_timer_count, g_spurious_count);
}

u64 get_timer_count() {
    return g_timer_count;
}

} // namespace APIC
