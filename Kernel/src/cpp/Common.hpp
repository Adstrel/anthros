#pragma once

// Start of base types
#if defined(__x86_64__)

using i8  = signed char;
using i16 = short;
using i32 = int;
using i64 = long;

using u8  = unsigned char;
using u16 = unsigned short;
using u32 = unsigned int;
using u64 = unsigned long;

using c8  = char8_t;
using c16 = char16_t;
using c32 = char32_t;

using usize = unsigned long;
using isize = long;

#elif defined(__ARM64__)

#error "The ARM64 architecture is not yet supported!"

#elif defined(__RISCV64__)

#error "The RISCV64 architecture is not yet supported!"

#else

#error "Unknown or unsupported architecture!"

#endif
// End of base types

static_assert(sizeof(i8 ) == 1);
static_assert(sizeof(i16) == 2);
static_assert(sizeof(i32) == 4);
static_assert(sizeof(i64) == 8);

static_assert(sizeof(u8 ) == 1);
static_assert(sizeof(u16) == 2);
static_assert(sizeof(u32) == 4);
static_assert(sizeof(u64) == 8);

static_assert(sizeof(c8)  == 1);
static_assert(sizeof(c16) == 2);
static_assert(sizeof(c32) == 4);

static_assert(sizeof(usize) == sizeof(void*));
static_assert(sizeof(isize) == sizeof(void*));

#define _STRINGIFY(x) #x
#define  STRINGIFY(x) _STRINGIFY(x)

#define NORETURN __attribute__((noreturn))
#define ALWAYS_INLINE __attribute__((always_inline, artificial)) inline
#define GENERAL_REGS  __attribute__(( target("general-regs-only")) )
#define ALIGNED(val) __attribute__(( aligned((val)) ))
#define PACKED __attribute__(( packed ))
#define CONST __attribute__(( const ))
#define PURE __attribute__(( pure ))

#define ACCESS(access_type, arg_data_index) \
    __attribute__(( access(access_type, (arg_data_index)) ))
#define ACCESS_SIZED(access_type, arg_data_index, arg_size_index) \
    __attribute__(( access(access_type, (arg_data_index), (arg_size_index)) ))

#define RO_ARG(arg_data_index) ACCESS(read_only, (arg_data_index))
#define WO_ARG(arg_data_index) ACCESS(write_only, (arg_data_index))
#define RW_ARG(arg_data_index) ACCESS(read_write, (arg_data_index))

#define RO_ARG_SIZED(arg_data_index, arg_size_index) ACCESS_SIZED(read_only, (arg_data_index), (arg_size_index))
#define WO_ARG_SIZED(arg_data_index, arg_size_index) ACCESS_SIZED(write_only, (arg_data_index), (arg_size_index))
#define RW_ARG_SIZED(arg_data_index, arg_size_index) ACCESS_SIZED(read_write, (arg_data_index), (arg_size_index))

#define NONNULL_ARGS_ALL __attribute__(( nonnull ))
#define NONNULL_RETVAL __attribute__(( returns_nonnull ))

#define offsetof(TYPE, MEMBER) __builtin_offsetof(TYPE, MEMBER)
template<typename T, usize SIZE> ALWAYS_INLINE consteval usize countof(T (&)[SIZE]) { return SIZE; }

namespace cpp {

template<NORETURN void (* PANIC_FUNC)(const char* msg, const char* func)>
ALWAYS_INLINE constexpr void assert_impl(const bool condition, const char* msg, const char* func) {
    if (!condition) {
        PANIC_FUNC(msg, func);
    }
}

} // namespace cpp


#ifndef NDEBUG
#   define CppAssert(condition) (cpp::assert_impl<PANIC_FUNC>(static_cast<bool>(condition), __FILE__ ":" STRINGIFY(__LINE__) ": Assertion `" STRINGIFY(condition) "' failed.", __PRETTY_FUNCTION__))
#else
#   define CppAssert(condition)
#endif
