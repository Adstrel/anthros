#pragma once
#include "Types.hpp"


// 'Placement new' and 'placement delete' need these implementations
// (shamelessly taken from https://en.cppreference.com/w/cpp/header/new)
[[nodiscard]] inline void* operator new  (usize size, void* ptr) noexcept { (void) size; return ptr; }
[[nodiscard]] inline void* operator new[](usize size, void* ptr) noexcept { (void) size; return ptr; }
inline void operator delete  (void* ptr, void*) noexcept { (void) ptr; }
inline void operator delete[](void* ptr, void*) noexcept { (void) ptr; }

namespace cpp {

template<typename T>
constexpr RemoveReference<T>&& move(T&& element) {
    return static_cast<RemoveReference<T>&&>(element);
}

template<typename T>
constexpr T&& forward(RemoveReference<T>& element) {
    return static_cast<T&&>(element);
}

template<typename T>
constexpr T&& forward(RemoveReference<T>&& element) {
    return static_cast<T&&>(element);
}

template<typename T, typename... Args_t>
T* construct_at(T* ptr, Args_t&&... args) {
    return new (ptr) T(forward<Args_t>(args)...);
}

template<typename T>
constexpr void destroy_at(T* ptr) {
    if constexpr (IsArray<T>) {
        for (auto& elem : *ptr) {
            destroy_at(&elem);
        }
        return;
    }

    ptr->~T();
}

template<
    typename T,
    template<typename> typename Allocator,
    NORETURN void (* PANIC_FUNC)(const char* msg, const char* func)
>
class UniquePtr {
    Allocator<T> allocator{};
    T* ptr{nullptr};

public:
    constexpr UniquePtr() = default;
    constexpr UniquePtr(const UniquePtr& other) = delete;
    constexpr UniquePtr(UniquePtr&& other)
        : allocator(move(other.allocator)),
          ptr(other.ptr) {
        other.ptr = nullptr;
    }
    constexpr ~UniquePtr() {
        if (ptr) {
            destroy_at(ptr);
            allocator.deallocate(ptr);
        }
    }

    template<typename... Args_t>
    static constexpr UniquePtr make(Args_t&&... args) {
        UniquePtr result;
        result.ptr = construct_at(
            result.allocator.allocate(1),
            forward<Args_t>(args)...
        );
        return result;
    }

    constexpr const T& operator*() const {
        CppAssert(ptr);
        return *ptr;
    }
    constexpr T& operator*() {
        CppAssert(ptr);
        return *ptr;
    }
};

} // namespace cpp
