#pragma once
#include "Common.hpp"


namespace cpp {

template<typename T1, typename T2>
static constexpr bool IsSame = __is_same(T1, T2);

template<typename T1, typename... TypesRest_t>
static constexpr bool IsOneOf = false;

template<typename T1, typename T2, typename... TypesRest_t>
static constexpr bool IsOneOf<T1, T2, TypesRest_t...> = IsSame<T1, T2> || IsOneOf<T1, TypesRest_t...>;

template<typename T>
static constexpr bool IsInteger =
    IsSame<T, char>           ||
    IsSame<T, signed char>    ||
    IsSame<T, unsigned char>  ||
    IsSame<T, short>          ||
    IsSame<T, unsigned short> ||
    IsSame<T, int>            ||
    IsSame<T, unsigned int>   ||
    IsSame<T, long>           ||
    IsSame<T, unsigned long>  ||
    IsSame<T, long long>      ||
    IsSame<T, unsigned long long>;

template<typename T>
concept Integer = IsInteger<T>;

template<typename T> struct RemoveReference_s      { using type = T; };
template<typename T> struct RemoveReference_s<T&>  { using type = T; };
template<typename T> struct RemoveReference_s<T&&> { using type = T; };
template<typename T> using RemoveReference = typename RemoveReference_s<T>::type;

template<typename T>          static constexpr const bool IsArray       = false;
template<typename T>          static constexpr const bool IsArray<T[]>  = true;
template<typename T, usize N> static constexpr const bool IsArray<T[N]> = true;

template<Integer T> struct Signed_s;
template<> struct Signed_s<signed char>            { using type = signed char; };
template<> struct Signed_s<char>                   { using type = signed char; };
template<> struct Signed_s<unsigned char>          { using type = signed char; };
template<> struct Signed_s<short>                  { using type = signed short; };
template<> struct Signed_s<unsigned short>         { using type = signed short; };
template<> struct Signed_s<int>                    { using type = signed int; };
template<> struct Signed_s<unsigned int>           { using type = signed int; };
template<> struct Signed_s<long int>               { using type = signed long int; };
template<> struct Signed_s<unsigned long int>      { using type = signed long int; };
template<> struct Signed_s<long long int>          { using type = signed long long int; };
template<> struct Signed_s<unsigned long long int> { using type = signed long long int; };
template<Integer T> using Signed = typename Signed_s<T>::type;

template<Integer T> struct Unsigned_s;
template<> struct Unsigned_s<signed char>            { using type = unsigned char; };
template<> struct Unsigned_s<char>                   { using type = unsigned char; };
template<> struct Unsigned_s<unsigned char>          { using type = unsigned char; };
template<> struct Unsigned_s<short>                  { using type = unsigned short; };
template<> struct Unsigned_s<unsigned short>         { using type = unsigned short; };
template<> struct Unsigned_s<int>                    { using type = unsigned int; };
template<> struct Unsigned_s<unsigned int>           { using type = unsigned int; };
template<> struct Unsigned_s<long int>               { using type = unsigned long int; };
template<> struct Unsigned_s<unsigned long int>      { using type = unsigned long int; };
template<> struct Unsigned_s<long long int>          { using type = unsigned long long int; };
template<> struct Unsigned_s<unsigned long long int> { using type = unsigned long long int; };
template<Integer T> using Unsigned = typename Unsigned_s<T>::type;

} // namespace cpp
