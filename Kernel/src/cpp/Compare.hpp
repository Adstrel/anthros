#pragma once
#include "Common.hpp"

namespace std {

enum class eq : int {
    equal = 0,
    equivalent = 0
};

enum class ord : int {
    less = -1,
    greater = 1
};

class strong_ordering {
    int value{0};

    constexpr explicit strong_ordering(eq v) noexcept
        : value(static_cast<int>(v)) {}
    constexpr explicit strong_ordering(ord v) noexcept
        : value(static_cast<int>(v)) {}

public:
    static const strong_ordering less;
    static const strong_ordering equivalent;
    static const strong_ordering equal;
    static const strong_ordering greater;

    friend constexpr bool operator==(strong_ordering v, int) noexcept;
    friend constexpr bool operator==(strong_ordering v, strong_ordering w) noexcept = default;
    friend constexpr bool operator< (strong_ordering v, int) noexcept;
    friend constexpr bool operator> (strong_ordering v, int) noexcept;
    friend constexpr bool operator<=(strong_ordering v, int) noexcept;
    friend constexpr bool operator>=(strong_ordering v, int) noexcept;
    friend constexpr bool operator< (int, strong_ordering v) noexcept;
    friend constexpr bool operator> (int, strong_ordering v) noexcept;
    friend constexpr bool operator<=(int, strong_ordering v) noexcept;
    friend constexpr bool operator>=(int, strong_ordering v) noexcept;
    friend constexpr strong_ordering operator<=>(strong_ordering v, int) noexcept;
    friend constexpr strong_ordering operator<=>(int, strong_ordering v) noexcept;
};

inline constexpr strong_ordering strong_ordering::less(ord::less);
inline constexpr strong_ordering strong_ordering::equal(eq::equal);
inline constexpr strong_ordering strong_ordering::equivalent(eq::equivalent);
inline constexpr strong_ordering strong_ordering::greater(ord::greater);

constexpr bool operator==(strong_ordering v, int) noexcept { return v.value == 0; }
constexpr bool operator< (strong_ordering v, int) noexcept { return v.value < 0; }
constexpr bool operator< (int, strong_ordering v) noexcept { return v.value > 0; }
constexpr bool operator<=(strong_ordering v, int) noexcept { return v.value <= 0; }
constexpr bool operator<=(int, strong_ordering v) noexcept { return v.value >= 0; }
constexpr bool operator> (strong_ordering v, int) noexcept { return v.value > 0; }
constexpr bool operator> (int, strong_ordering v) noexcept { return v.value < 0; }
constexpr bool operator>=(strong_ordering v, int) noexcept { return v.value >= 0; }
constexpr bool operator>=(int, strong_ordering v) noexcept { return v.value <= 0; }

constexpr strong_ordering operator<=>(strong_ordering v, int) noexcept {
    return v;
}

constexpr strong_ordering operator<=>(int, strong_ordering v) noexcept {
    v.value = -v.value;
    return v;
}

} // namespace std
