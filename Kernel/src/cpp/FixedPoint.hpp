#pragma once
#include "Types.hpp"
#include "Compare.hpp"


namespace cpp {

struct Ratio {
    u64 numerator{1};
    u64 denominator{1};

    constexpr bool operator==(const Ratio& other) const noexcept = default;
};

template<typename T, Ratio RATIO = Ratio{1, 1}>
requires IsInteger<T>
struct Fixed {
    T value{};

    static constexpr Ratio c_RATIO = RATIO;

    constexpr Fixed() noexcept = default;
    constexpr Fixed(T init_value) noexcept
        : value(init_value) {}
    constexpr Fixed(const Fixed& other) noexcept
        : value(other.value) {}
    constexpr Fixed& operator=(const Fixed& other) noexcept {
        value = other.value;
        return *this;
    }

    template<Ratio RATIO_OTHER>
    constexpr Fixed<T, Ratio{RATIO.numerator, RATIO.denominator * RATIO_OTHER.denominator}> operator+(const Fixed<T, RATIO_OTHER> other) const noexcept {
        return { value * RATIO_OTHER.denominator + other.value * RATIO.denominator };
    }

    template<Ratio RATIO_OTHER>
    constexpr Fixed<T, Ratio{RATIO.numerator, RATIO.denominator * RATIO_OTHER.denominator}> operator-(const Fixed<T, RATIO_OTHER> other) const noexcept {
        return { value * RATIO_OTHER.denominator - other.value * RATIO.denominator };
    }

    template<Ratio RATIO_OTHER>
    constexpr Fixed<T, Ratio{RATIO.numerator * RATIO_OTHER.numerator, RATIO.denominator * RATIO_OTHER.denominator}> operator*(const Fixed<T, RATIO_OTHER> other) const noexcept {
        return { value * other.value };
    }

    template<Ratio RATIO_NEW>
    constexpr operator Fixed<T, RATIO_NEW>() const noexcept {
        // TODO: use 'if constexpr' to use the best order of operations possible to get the least ammount of error
        return { value * RATIO.numerator / RATIO.denominator * RATIO_NEW.denominator / RATIO_NEW.numerator };
    }

    template<typename U>
    constexpr operator Fixed<U, RATIO>() const noexcept {
        return { static_cast<U>(value) };
    }

    constexpr auto operator<=>(const Fixed<T, RATIO>& other) const noexcept = default;
};

template<Ratio RATIO = Ratio{1, 1}> using FixedU8  = Fixed<uint8_t,  RATIO>;
template<Ratio RATIO = Ratio{1, 1}> using FixedU16 = Fixed<uint16_t, RATIO>;
template<Ratio RATIO = Ratio{1, 1}> using FixedU32 = Fixed<uint32_t, RATIO>;
template<Ratio RATIO = Ratio{1, 1}> using FixedU64 = Fixed<uint64_t, RATIO>;

template<Ratio RATIO = Ratio{1, 1}> using FixedI8  = Fixed<int8_t,  RATIO>;
template<Ratio RATIO = Ratio{1, 1}> using FixedI16 = Fixed<int16_t, RATIO>;
template<Ratio RATIO = Ratio{1, 1}> using FixedI32 = Fixed<int32_t, RATIO>;
template<Ratio RATIO = Ratio{1, 1}> using FixedI64 = Fixed<int64_t, RATIO>;

// Explicit instantiation so you can look at the generated code
// template __attribute__((used)) Fixed<uint64_t, Ratio{1, 4}>::operator Fixed<uint64_t, Ratio{1, 1}>() const;

} // namespace cpp
