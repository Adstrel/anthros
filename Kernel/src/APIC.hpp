#pragma once
#include "ACPI.hpp"


namespace APIC {

void parse_madt(u64 madt_phys);

// Available after parsing the MADT ACPI table
bool is_x2APIC_present();
void init();

// Available after initialization
void print_timer_count();
u64 get_timer_count();

} // namespace APIC
