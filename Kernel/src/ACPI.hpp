#pragma once
#include "common.hpp"
#include "ACPI/Definitions.hpp"

constexpr u32 ascii_to_uint32_t(const c8 (&str)[5]) {
    // Last byte is '\0'
    return (static_cast<u32>(str[0]) << (0 * 8)) |
           (static_cast<u32>(str[1]) << (1 * 8)) |
           (static_cast<u32>(str[2]) << (2 * 8)) |
           (static_cast<u32>(str[3]) << (3 * 8));
}

constexpr u64 ascii_to_uint64_t(const c8 (&str)[9]) {
    // Last byte is '\0'
    return (static_cast<u64>(str[0]) << (0 * 8)) |
           (static_cast<u64>(str[1]) << (1 * 8)) |
           (static_cast<u64>(str[2]) << (2 * 8)) |
           (static_cast<u64>(str[3]) << (3 * 8)) |
           (static_cast<u64>(str[4]) << (4 * 8)) |
           (static_cast<u64>(str[5]) << (5 * 8)) |
           (static_cast<u64>(str[6]) << (6 * 8)) |
           (static_cast<u64>(str[7]) << (7 * 8));
}

namespace ACPI {

struct ImportantTables {
    u64 rsdp_phys{0};
    u64 xsdt_phys{0};
    u64 facp_phys{0};
    u64 madt_phys{0};
    u64 mcfg_phys{0};

    constexpr bool FoundAll() const {
        return
            rsdp_phys != 0 &&
            xsdt_phys != 0 &&
            facp_phys != 0 &&
            madt_phys != 0 &&
            mcfg_phys != 0;
    }
};

ImportantTables parse(const u64 rsdp_phys);

} // namespace ACPI
