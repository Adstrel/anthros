#include "IDT.hpp"
#include "print.hpp"


namespace IDT {

struct Entry {
    u64 Offset_15_0 : 16;
    u64 SegmentSelector : 16;
    u64 IST : 3;
    u64 Reserved0 : 5;
    u64 GateType : 4;
    u64 Zero : 1;
    u64 DPL : 2;
    u64 Present : 1;
    u64 Offset_31_16 : 16;
    u64 Offset_63_32 : 32;
    u64 Reserved1 : 32;

    constexpr Entry()
        : Offset_15_0(0),
          SegmentSelector(0),
          IST(0),
          Reserved0(0),
          GateType(0),
          Zero(0),
          DPL(0),
          Present(0),
          Offset_31_16(0),
          Offset_63_32(0),
          Reserved1(0) {}

    constexpr Entry(u64 Offset, u16 Selector, u8 IST, u8 GateType, u8 DPL)
        : Offset_15_0(Offset & 0xffff),
          SegmentSelector(Selector),
          IST(IST & 0x3),
          Reserved0(0),
          GateType(GateType & 0xf),
          Zero(0),
          DPL(DPL & 0x3),
          Present(1),
          Offset_31_16((Offset >> 16) & 0xffff),
          Offset_63_32(static_cast<u32>((Offset >> 32) & 0xffff'ffff)),
          Reserved1(0) {}
};

// TODO: Use linker script variables to generate the full IDT at link time
static constinit Entry IDT[256];

struct IDTR {
    u16 Limit;
    const Entry* Base;
} PACKED;

constexpr static ALIGNED(0x10) IDTR idtr {
    .Limit = sizeof(IDT) - 1,
    .Base = IDT
};

// TODO: Can I call 'print', if I did not mark it as 'general-regs-only'?
extern "C" ISR void isr0(interrupt_frame* frame) {
    print("Got an interrupt 0: divide by 0!\n");
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr1(interrupt_frame* frame) {
    print("Got an interrupt 1: Debug trap!\n");
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr2(interrupt_frame* frame) {
    print("Got an interrupt 2: Non-maskable Interrupt!\n");
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr3(interrupt_frame* frame) {
    print("Got an interrupt 3: Breakpoint!\n");
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr4(interrupt_frame* frame) {
    print("Got an interrupt 4: Overflow!\n");
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr5(interrupt_frame* frame) {
    print("Got an interrupt 5: Bound Range Exceeded!\n");
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr6(interrupt_frame* frame) {
    print("Got an interrupt 6: Invalid Opcode!\n");
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr7(interrupt_frame* frame) {
    print("Got an interrupt 7: Device Not Available!\n");
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr8(interrupt_frame* frame, u64 error_code) {
    print("Got an interrupt 8: Double Fault! Error code: {xh}\n", error_code);
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr9(interrupt_frame* frame) {
    print("Got an interrupt 9: Coprocessor Segment Overrun (legacy)!\n");
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr10(interrupt_frame* frame, u64 error_code) {
    print("Got an interrupt 10: Invalid TSS! Error code: {xh}\n", error_code);
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr11(interrupt_frame* frame, u64 error_code) {
    print("Got an interrupt 11: Segment Not Present! Error code: {xh}\n", error_code);
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr12(interrupt_frame* frame, u64 error_code) {
    print("Got an interrupt 12: Stack-Segment Fault! Error code: {xh}\n", error_code);
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr13(interrupt_frame* frame, u64 error_code) {
    print("Got an interrupt 13: General Protection Fault! Error code: {xh}\n", error_code);
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr14(interrupt_frame* frame, u64 error_code) {
    print("Got an interrupt 14: Page Fault! Error code: {xh}\n", error_code);
    u64 cr2;
    asm ("mov %%cr2, %0" : "=r"(cr2));
    print("CR2: {xh}\n", cr2);
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr15(interrupt_frame* frame) {
    print("Got an interrupt 15: Reserved!\n");
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr16(interrupt_frame* frame) {
    print("Got an interrupt 16: x87 Floating-Point Exception!\n");
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr17(interrupt_frame* frame, u64 error_code) {
    print("Got an interrupt 17: Alignment Check! Error code: {xh}\n", error_code);
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr18(interrupt_frame* frame) {
    print("Got an interrupt 18: Machine Check!\n");
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr19(interrupt_frame* frame) {
    print("Got an interrupt 19: SIMD Floating-Point Exception!\n");
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr20(interrupt_frame* frame) {
    print("Got an interrupt 20: Virtualization Exception!\n");
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr21(interrupt_frame* frame, u64 error_code) {
    print("Got an interrupt 21: Control Protection Exception! Error code: {xh}\n", error_code);
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr22(interrupt_frame* frame) {
    print("Got an interrupt 22: Reserved!\n");
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr23(interrupt_frame* frame) {
    print("Got an interrupt 23: Reserved!\n");
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr24(interrupt_frame* frame) {
    print("Got an interrupt 24: Reserved!\n");
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr25(interrupt_frame* frame) {
    print("Got an interrupt 25: Reserved!\n");
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr26(interrupt_frame* frame) {
    print("Got an interrupt 26: Reserved!\n");
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr27(interrupt_frame* frame) {
    print("Got an interrupt 27: Reserved!\n");
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr28(interrupt_frame* frame) {
    print("Got an interrupt 28: Hypervisor Injection Exception!\n");
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr29(interrupt_frame* frame, u64 error_code) {
    print("Got an interrupt 29: VMM Communication Exception! Error code: {xh}\n", error_code);
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr30(interrupt_frame* frame, u64 error_code) {
    print("Got an interrupt 30: Security Exception! Error code: {xh}\n", error_code);
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

extern "C" ISR void isr31(interrupt_frame* frame) {
    print("Got an interrupt 31: Reserved!\n");
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

// TODO: Get the interrupt number and print it too
extern "C" ISR void isr_other(interrupt_frame* frame) {
    print("Got an unnown interrupt!\n");
    print("rip: {xh}, cs: {xh}, rflags: {xh}, rsp: {xh}, ss: {xh}\n",
        frame->rip,
        frame->cs,
        frame->rflags,
        frame->rsp,
        frame->ss
    );

    while (true);
}

// Timer interrupt; implemented in APIC.cpp
extern "C" ISR void isr32(interrupt_frame* frame);

// APIC Spurious interrupt; implemented in APIC.cpp
extern "C" ISR void isr255(interrupt_frame* frame);

void init() {
    IDT[0]   = Entry(reinterpret_cast<u64>(isr0),   sizeof(u64) * 1, 0, 0xf, 0);
    IDT[1]   = Entry(reinterpret_cast<u64>(isr1),   sizeof(u64) * 1, 0, 0xf, 0);
    IDT[2]   = Entry(reinterpret_cast<u64>(isr2),   sizeof(u64) * 1, 0, 0xe, 0);
    IDT[3]   = Entry(reinterpret_cast<u64>(isr3),   sizeof(u64) * 1, 0, 0xf, 0);
    IDT[4]   = Entry(reinterpret_cast<u64>(isr4),   sizeof(u64) * 1, 0, 0xf, 0);
    IDT[5]   = Entry(reinterpret_cast<u64>(isr5),   sizeof(u64) * 1, 0, 0xf, 0);
    IDT[6]   = Entry(reinterpret_cast<u64>(isr6),   sizeof(u64) * 1, 0, 0xf, 0);
    IDT[7]   = Entry(reinterpret_cast<u64>(isr7),   sizeof(u64) * 1, 0, 0xf, 0);
    IDT[8]   = Entry(reinterpret_cast<u64>(isr8),   sizeof(u64) * 1, 0, 0xf, 0);
    IDT[9]   = Entry(reinterpret_cast<u64>(isr9),   sizeof(u64) * 1, 0, 0xf, 0);
    IDT[10]  = Entry(reinterpret_cast<u64>(isr10),  sizeof(u64) * 1, 0, 0xf, 0);
    IDT[11]  = Entry(reinterpret_cast<u64>(isr11),  sizeof(u64) * 1, 0, 0xf, 0);
    IDT[12]  = Entry(reinterpret_cast<u64>(isr12),  sizeof(u64) * 1, 0, 0xf, 0);
    IDT[13]  = Entry(reinterpret_cast<u64>(isr13),  sizeof(u64) * 1, 0, 0xf, 0);
    IDT[14]  = Entry(reinterpret_cast<u64>(isr14),  sizeof(u64) * 1, 0, 0xf, 0);
    IDT[15]  = Entry(reinterpret_cast<u64>(isr15),  sizeof(u64) * 1, 0, 0xf, 0);
    IDT[16]  = Entry(reinterpret_cast<u64>(isr16),  sizeof(u64) * 1, 0, 0xf, 0);
    IDT[17]  = Entry(reinterpret_cast<u64>(isr17),  sizeof(u64) * 1, 0, 0xf, 0);
    IDT[18]  = Entry(reinterpret_cast<u64>(isr18),  sizeof(u64) * 1, 0, 0xf, 0);
    IDT[19]  = Entry(reinterpret_cast<u64>(isr19),  sizeof(u64) * 1, 0, 0xf, 0);
    IDT[20]  = Entry(reinterpret_cast<u64>(isr20),  sizeof(u64) * 1, 0, 0xf, 0);
    IDT[21]  = Entry(reinterpret_cast<u64>(isr21),  sizeof(u64) * 1, 0, 0xf, 0);
    IDT[22]  = Entry(reinterpret_cast<u64>(isr22),  sizeof(u64) * 1, 0, 0xf, 0);
    IDT[23]  = Entry(reinterpret_cast<u64>(isr23),  sizeof(u64) * 1, 0, 0xf, 0);
    IDT[24]  = Entry(reinterpret_cast<u64>(isr24),  sizeof(u64) * 1, 0, 0xf, 0);
    IDT[25]  = Entry(reinterpret_cast<u64>(isr25),  sizeof(u64) * 1, 0, 0xf, 0);
    IDT[26]  = Entry(reinterpret_cast<u64>(isr26),  sizeof(u64) * 1, 0, 0xf, 0);
    IDT[27]  = Entry(reinterpret_cast<u64>(isr27),  sizeof(u64) * 1, 0, 0xf, 0);
    IDT[28]  = Entry(reinterpret_cast<u64>(isr28),  sizeof(u64) * 1, 0, 0xf, 0);
    IDT[29]  = Entry(reinterpret_cast<u64>(isr29),  sizeof(u64) * 1, 0, 0xf, 0);
    IDT[30]  = Entry(reinterpret_cast<u64>(isr30),  sizeof(u64) * 1, 0, 0xf, 0);
    IDT[31]  = Entry(reinterpret_cast<u64>(isr31),  sizeof(u64) * 1, 0, 0xf, 0);
    IDT[32]  = Entry(reinterpret_cast<u64>(isr32),  sizeof(u64) * 1, 0, 0xe, 0);

    for (usize i = 33; i < 255; i++) {
        IDT[i] = Entry(reinterpret_cast<u64>(isr_other), sizeof(u64) * 1, 0, 0xe, 0);
    }

    IDT[255] = Entry(reinterpret_cast<u64>(isr255), sizeof(u64) * 1, 0, 0xe, 0);

    asm volatile ("lidt (%0)" : : "r"(&idtr));
}

} // namespace IDT
