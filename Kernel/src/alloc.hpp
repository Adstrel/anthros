#pragma once
#include "common.hpp"
#include "cpp/Memory.hpp"
#include "print.hpp"


void kfree(void* ptr);
void* kalloc(usize needed_size, usize alignment = 16);

template<typename T>
struct KAllocator {
    T* allocate(const usize count) {
        const usize size_bytes = sizeof(T) * count;
        T* ptr = static_cast<T*>(kalloc(size_bytes));

        return ptr;
    }

    void deallocate(T* ptr) {
        kfree(ptr);
    }
};

template<typename T>
using UniquePtr = cpp::UniquePtr<T, KAllocator, panic>;
