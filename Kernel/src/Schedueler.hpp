#pragma once
#include "common.hpp"


namespace Schedueler {

struct TSS {
    u32 Reserved0;

    u32 rsp0_low;
    u32 rsp0_high;

    u32 rsp1_low;
    u32 rsp1_high;

    u32 rsp2_low;
    u32 rsp2_high;

    u32 Reserved1;
    u32 Reserved2;

    u32 isr1_low;
    u32 isr1_high;

    u32 isr2_low;
    u32 isr2_high;

    u32 isr3_low;
    u32 isr3_high;

    u32 isr4_low;
    u32 isr4_high;

    u32 isr5_low;
    u32 isr5_high;

    u32 isr6_low;
    u32 isr6_high;

    u32 isr7_low;
    u32 isr7_high;

    u32 Reserved3;
    u32 Reserved4;

    u16 Reserved5;
    u16 iopb;
};

extern TSS cpu0_tss;

NORETURN void work();

} // namespace Schedueler
