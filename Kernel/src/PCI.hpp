#pragma once
#include "ACPI.hpp"


namespace PCI {

struct CommonHeader {
    const u16 VendorID;
    const u16 DeviceID;

    volatile u16 Command;
    volatile u16 Status;

    const u8 RevisionID;
    // Programming Interface Byte
    const u8 ProgIF;
    const u8 Subclass;
    const u8 ClassCode;

    volatile u8 CacheLineSize;
    u8 LatencyTimer;
    const u8 HeaderType : 7;
    const u8 MultipleFunctions : 1;
    volatile u8 BIST;
};

struct HeaderType_0 {
    CommonHeader common;
    volatile u32 BAR0;
    volatile u32 BAR1;
    volatile u32 BAR2;
    volatile u32 BAR3;
    volatile u32 BAR4;
    volatile u32 BAR5;
    u32 CardbusCisPointer;
    const u16 SubsystemVendorID;
    const u16 SubsystemID;
    u32 ExpansionRomBaseAddress;
    u8 CapabilitiesPointer;
    const u8 Reserved[7];
    u8 InterruptLine;
    u8 InterruptPIN;
    u8 MinGrant;
    u8 MaxLatency;
};

const char* pci_vendor_id_to_string(u16 vendor_id);
const char* pci_device_to_string(u16 vendor_id, u16 device_id);

void register_mcfg_table(u64 mcfg_physical_addr);

} // namespace PCI
