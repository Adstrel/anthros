#pragma once
#include "print.hpp"
#include "x86.hpp"
#include "PageAllocator.hpp"


enum class IsWritable   : bool { No = false, Yes = true };
enum class IsExecutable : bool { No = false, Yes = true };
enum class IsUsermode   : bool { No = false, Yes = true };

inline void map_pa_to_va(const u64 page_table_phys, const u64 pa, const u64 va, const IsWritable writable, const IsExecutable executable, const IsUsermode usermode) {
    constexpr const u64 get_addr_mask = 0x000f'ffff'ffff'f000ull;
    constexpr const u64 index_mask = 0x1ff;
    const u64 index4 = (va >> 39) & index_mask;
    const u64 index3 = (va >> 30) & index_mask;
    const u64 index2 = (va >> 21) & index_mask;
    const u64 index1 = (va >> 12) & index_mask;

    u64 level4_entry = reinterpret_cast<u64*>(page_table_phys)[index4];
    if ((level4_entry & 1) == 0) {
        // Set up a level 3 page table structure
        u64 new_page = PageAllocator::get_page_zeroed();
        u64 new_entry = new_page | (1 << 0) | (1 << 1);
        reinterpret_cast<u64*>(page_table_phys)[index4] = new_entry;
        level4_entry = new_entry;
    }

    if (usermode == IsUsermode::Yes && !(level4_entry & (1 << 2))) {
        level4_entry = level4_entry | (1 << 2);
        reinterpret_cast<u64*>(page_table_phys)[index4] = level4_entry;
    }

    const u64 level3_phys_addr = level4_entry & get_addr_mask;
    u64 level3_entry = reinterpret_cast<u64*>(level3_phys_addr)[index3];
    if ((level3_entry & 1) == 0) {
        // Set up a level 2 page table structure
        u64 new_page = PageAllocator::get_page_zeroed();
        u64 new_entry = new_page | (1 << 0) | (1 << 1);
        reinterpret_cast<u64*>(level3_phys_addr)[index3] = new_entry;
        level3_entry = new_entry;
    }

    if ((level3_entry & (1 << 7)) != 0) {
        print("Early panic: Can't deal with the PageSize bit set in level3 page table -- unimplemented\n");
        while (true);
    }

    if (usermode == IsUsermode::Yes && !(level3_entry & (1 << 2))) {
        level3_entry = level3_entry | (1 << 2);
        reinterpret_cast<u64*>(level3_phys_addr)[index3] = level3_entry;
    }

    const u64 level2_phys_addr = level3_entry & get_addr_mask;
    u64 level2_entry = reinterpret_cast<u64*>(level2_phys_addr)[index2];
    if ((level2_entry & 1) == 0) {
        // Set up a level 1 page table structure
        u64 new_page = PageAllocator::get_page_zeroed();
        u64 new_entry = new_page | (1 << 0) | (1 << 1);
        reinterpret_cast<u64*>(level2_phys_addr)[index2] = new_entry;
        level2_entry = new_entry;
    }

    if ((level2_entry & (1 << 7)) != 0) {
        print("Early panic: Can't deal with the PageSize bit set in level2 page table -- unimplemented\n");
        while (true);
    }

    if (usermode == IsUsermode::Yes && !(level2_entry & (1 << 2))) {
        level2_entry = level2_entry | (1 << 2);
        reinterpret_cast<u64*>(level2_phys_addr)[index2] = level2_entry;
    }

    const u64 level1_phys_addr = level2_entry & get_addr_mask;
    reinterpret_cast<u64*>(level1_phys_addr)[index1] =
        pa |
        (1 << 0) | // Present bit
        (static_cast<u64>(writable   == IsWritable::Yes)   << 1)  |
        (static_cast<u64>(usermode   == IsUsermode::Yes)   << 2)  |
        (static_cast<u64>(executable == IsExecutable::No)  << 63);
};

// Maps a given number of fresh pages (allocated with PageAllocator) to consecutive vitural addresses starting from the given address
inline void alloc_and_map_memory(const u64 level4_phys_addr, const u64 virtual_address, const u64 pages_to_allocate, const IsWritable writable, const IsExecutable executable, const IsUsermode usermode) {
    if (virtual_address % 4096 != 0) {
        print("Panic: Allocating and mapping pages to a VA that is not aligned to 4096 bytes!\n");
        while (true);
    }

    for (u64 i = 0; i < pages_to_allocate; i++) {
        map_pa_to_va(level4_phys_addr, PageAllocator::get_page(), virtual_address + i * 4096, writable, executable, usermode);
    }
}

// Looks through the current page table and returns the physical address
// corresponding to the input virtual address
inline u64 va_to_pa(const void* const virtual_address) {
    const u64 va = reinterpret_cast<u64>(virtual_address);

    constexpr const u64 get_addr_mask = 0x000f'ffff'ffff'f000ull;
    constexpr const u64 index_mask = 0x1ff;
    const u64 index4 = (va >> 39) & index_mask;
    const u64 index3 = (va >> 30) & index_mask;
    const u64 index2 = (va >> 21) & index_mask;
    const u64 index1 = (va >> 12) & index_mask;
    const u64 offset = va & 0xfff;

    const u64 level4_phys_addr = read_cr3();

    const u64 level4_entry = reinterpret_cast<u64*>(level4_phys_addr)[index4];
    if ((level4_entry & 1) == 0) {
        return ~0ull;
    }

    const u64 level3_phys_addr = level4_entry & get_addr_mask;
    const u64 level3_entry = reinterpret_cast<u64*>(level3_phys_addr)[index3];
    if ((level3_entry & 1) == 0) {
        return ~0ull;
    }

    const u64 level2_phys_addr = level3_entry & get_addr_mask;
    const u64 level2_entry = reinterpret_cast<u64*>(level2_phys_addr)[index2];
    if ((level2_entry & 1) == 0) {
        return ~0ull;
    }

    const u64 level1_phys_addr = level2_entry & get_addr_mask;
    const u64 level1_entry = reinterpret_cast<u64*>(level1_phys_addr)[index1];
    if ((level1_entry & 1) == 0) {
        return ~0ull;
    }

    return (level1_entry & get_addr_mask) + offset;
}
