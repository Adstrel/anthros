#pragma once
#include "cpp/Common.hpp"
#include "cpp/Types.hpp"


extern "C"
WO_ARG_SIZED(1, 3)
RO_ARG_SIZED(2, 3)
NONNULL_ARGS_ALL
NONNULL_RETVAL
constexpr void* memcpy(void* dst, const void* src, usize bytes) {
    u8* _dst = static_cast<u8*>(dst);
    const u8* _src = static_cast<const u8*>(src);

    while (bytes != 0) {
        *_dst++ = *_src++;
        --bytes;
    }

    return dst;
}

extern "C"
WO_ARG_SIZED(1, 3)
NONNULL_ARGS_ALL
NONNULL_RETVAL
constexpr void* memset(void* dst, u8 value, usize bytes) {
    u8* _dst = static_cast<u8*>(dst);

    while (bytes != 0) {
        *_dst++ = value;
        --bytes;
    }

    return dst;
}

template<u32 BIT_LOWER, u32 BIT_UPPER = BIT_LOWER, cpp::Integer Int_t>
requires (
    (BIT_LOWER < sizeof(Int_t) * 8) &&
    (BIT_UPPER < sizeof(Int_t) * 8) &&
    (BIT_UPPER >= BIT_LOWER)
)
CONST
constexpr ALWAYS_INLINE cpp::Unsigned<Int_t> GetBits(Int_t value) {
    constexpr u32 BIT_SIZE = BIT_UPPER - BIT_LOWER + 1;

    const cpp::Unsigned<Int_t> unsigned_value =
        static_cast<cpp::Unsigned<Int_t>>(value);

    return
        (unsigned_value >> BIT_LOWER) &
        (~static_cast<cpp::Unsigned<Int_t>>(0) >> (32 - BIT_SIZE));
}
