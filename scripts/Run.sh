#!/bin/bash

if [ "$(basename `pwd`)" = "scripts" ]; then
    cd ..
fi

if [ "$(basename `pwd`)" != "anthros" ]; then
    echo "Use this script from the 'anthros' base project directory!"
    exit 1
fi

exec ./toolchain/root/bin/qemu-system-x86_64 \
    -m 1024 \
    -machine q35 \
    -cpu EPYC-v3 \
    -chardev stdio,id=stdout,mux=on \
    -device isa-debugcon,chardev=stdout \
    -device qemu-xhci \
    -device edu \
    -device usb-kbd \
    -device pci-serial \
    -bios toolchain/root/share/OVMF/OVMF.fd \
    -device driver=rtl8139,netdev=n0 \
    -netdev user,id=n0,tftp=pxe_root/,bootfile=prekernel.efi
