name="nasm"
version="2.16.01"
revision="2.16.01"

build_type="tar"

download_paths=(
    "https://www.nasm.us/pub/nasm/releasebuilds/$version/$name-$version.tar.xz"
)

md5sum_value="d755ba0d16f94616c2907f8cab7c748b"

compile_instructions=(
    "./configure --prefix=$root_dir --target=$target"
    "make -j $(nproc)"
)

install_instructions=(
    "make install"
)

