name="gcc"
version="13"
revision="13.2.0"

build_type="tar"

download_paths=(
    "https://ftp.gnu.org/gnu/gcc/gcc-${revision}/gcc-${revision}.tar.xz"
)

md5sum_value="e0e48554cc6e4f261d55ddee9ab69075"

compile_instructions=(
    "./contrib/download_prerequisites"
    "mkdir build"
    "cd build"
    "../configure --prefix=$root_dir --target=$target --enable-lto --enable-languages=c,c++ --disable-nls"
    "make -j $(nproc) all-gcc"
    "make -j $(nproc) all-target-libgcc"
)

install_instructions=(
    "make install-gcc"
    "make install-target-libgcc"
)

