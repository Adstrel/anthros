name="qemu"
version="8.2.0"
revision="8.2.0"

build_type="tar"

download_paths=(
    "https://download.qemu.org/qemu-${revision}.tar.xz"
)

md5sum_value="302be965cf5e54e7f8b4bd7f1248029d"

compile_instructions=(
    "mkdir build"
    "cd build"
    "../configure --prefix=$root_dir --target-list=$arch-softmmu --enable-slirp --enable-sdl"
    "ninja"
)

install_instructions=(
    "ninja install"
)

