name="binutils"
version="2.41"
revision="2.41"

build_type="tar"

download_paths=(
    "https://ftpmirror.gnu.org/binutils/binutils-${revision}.tar.xz"
)

md5sum_value="256d7e0ad998e423030c84483a7c1e30"

compile_instructions=(
    "mkdir build"
    "cd build"
    "../configure --prefix=$root_dir --target=$target --enable-lto --disable-nls --enable-targets=x86_64-unknown-pep"
    "make -j $(nproc)"
)

install_instructions=(
    "make install"
)

