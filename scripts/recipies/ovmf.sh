# OVMF drivers are part of the EDK2 package
name="edk2"
version="stable202208"
revision="stable202208"

build_type="git"

git_path="https://github.com/tianocore/edk2.git"

# TODO: Our build system does not cache this cloned repo, which is very annoying,
#       because it takes a very long time to do a recursive clone for the EDK2 repo
clone_instructions=(
    "git clone $git_path --recursive"
)

compile_instructions=(
    #"export GCC5_X64_PREFIX=$root_dir/bin/x86_64-unknown-elf-"
    "export GCC5_X64_PREFIX=x86_64-unknown-elf"
    "source edksetup.sh"
    "export GCC5_X64_PREFIX=x86_64-unknown-elf"
    "make -C BaseTools -j$(nproc)"
    "build -a X64 -t GCC5 -p OvmfPkg/OvmfPkgX64.dsc -b RELEASE"
)

install_instructions=(
    "mkdir -p $root_dir/share/OVMF/"
    "cp Build/OvmfX64/RELEASE_GCC5/FV/OVMF.fd $root_dir/share/OVMF/"
)

