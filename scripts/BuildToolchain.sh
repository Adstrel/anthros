#!/bin/bash

if [ "$(basename `pwd`)" = "scripts" ]; then
    cd ..
fi

if [ "$(basename `pwd`)" != "anthros" ]; then
    echo "Use this script from the 'anthros' base project directory!"
    exit 1
fi

mkdir -p toolchain/{root,src}

root_dir="$(pwd)/toolchain/root"
PATH="$(pwd)/toolchain/root/bin:$PATH"

target="x86_64-unknown-elf"
arch="x86_64"

for n in `cat scripts/toolchain_tools.txt`; do
    # Trim whitespace
    n=$(echo "$n" | awk '{$1=$1; print}')

    # Skip empty and commented-out lines
    if [ -z "$n" ] || [ "${n::1}" = "#" ]; then
        continue
    fi

    # Load settings for this tool
    source "scripts/recipies/$n.sh"

    case $build_type in
        tar)
            filename=`basename ${download_paths[0]}`
            wd_name=$name-$revision

            if [ ! -f "toolchain/src/$filename" ]; then
                echo "Downloading $name $revision"
                download_paths_length=${#download_paths[@]}
                random_index=$(($RANDOM % $download_paths_length))
                wget "${download_paths[$random_index]}" -O "toolchain/src/$filename" || exit 1
            else
                echo "$name $revision already downloaded"
            fi

            new_md5sum="$(md5sum toolchain/src/$filename | cut -f1 -d' ')"
            if [ "$new_md5sum" != "$md5sum_value" ]; then
                echo "[ERROR]: Mismatch in md5sum value for $filename!"
                echo "New value: $new_md5sum"
                echo "Old value: $md5sum_value"
                exit 1
            fi
            ;;
        git)
            wd_name=$name
            ;;
        *)
            echo "Unknown build type '$build_type' for project '$name'"
            exit 1
            ;;
    esac


    pushd toolchain/src
        if [ -e "$wd_name" ]; then
            echo "Removing old compile files"
            rm -rf "$wd_name"
        fi

        if [ "$build_type" = "tar" ]; then
            tar xf $filename --checkpoint=10000
        fi
        if [ "$build_type" = "git" ]; then
            echo ABCD
            for instr in "${clone_instructions[@]}"; do
                echo "$instr"
                $instr || (echo "Failed at cloning $name $revision!"; exit 1) || exit 1
            done
        fi

        pushd "$wd_name"
            echo "Compiling $name $revision"
            for instr in "${compile_instructions[@]}"; do
                $instr || (echo "Failed at compiling $name $revision!"; exit 1) || exit 1
            done

            echo "Installing $name $revision"
            for instr in "${install_instructions[@]}"; do
                $instr || (echo "Failed at installing $name $revision!"; exit 1) || exit 1
            done
        popd
        rm -rf "$wd_name"
    popd
done

