# Basic Design Plan
- PreKernel (bootloader)
    - Load the Kernel with calls to UEFI, map it, and jump into it
- EarlyKernel (part of one Kernel binary)
    - Do all initialization stuff
    - Release all memory used by EarlyKernel -- allocated memory + data and code that were put into "early" sections
    - Jump into RuntimeKernel
        - Should be in the higher half of virtual memory, if we weren't already
- RuntimeKernel (part of one Kernel binary)
    - Spin up a thread or something

# TODO plan for now
- Enumerate PCI devices
- Make a little kernel program
- Load the kernel with UEFI and give it execution

# TODO plan for after we are done with the previous one
- Serial driver for output
- Set up own segmentation table
- Set up own paging table
- Set up a page frame allocator
- Set up memory allocation
- Parse the ACPI tables
- Set up interrupts with APIC
- Spin up a thread that can call a syscall
- Revise the TODO list

# Crazy Process Design Ideas
- The `init` process *can* get full priviledges for the whole system, but can also specify a reduced set of priviledges or additional constraints on its priviledges (stored in a special table in the programs binary that the kernel reads when spawning the process). It can then spawn other processes and either give them reduced priviledges, or permanently *transfer* some of its own priviledges to it.
- When the kernel is creating a new process it:
    - Gets handed a set of priviledges by the spawning process. These form the biggest possible set of priviledges the new process can have. This set can be reduced by other mechanisms mentioned in this list.
    - Reads a table stored in the binary of the process being created, which specifies a set of priviledges the program expects to have. If there is some priviledge that the new process does not want, but the creater process allowed, then the new proces is spawned without this priviledge. If the new process wants a priviledge that the creater process did not specify, then one of the following will happen (based on what the creater process wants to happen in this situation):
        - The creation of the new process is aborted, and a descriptive error is returned to the creator process.
        - The new process is created, and when it tries to use one of the disallowed priviledges, it crashes.
        - The new process is created, and when it tries to use one of the disallowed priviledges, it is suspended (and the creator process can do whatever it wants with it).
        - The new process is created, and when it tries to use one of the disallowed priviledges, it always gets returned an error.
- Examples of priviledges I would like to have control over:
    - Can spawn new processes
    - Can spawn new threads
    - Real-time guarantees for schedueling
    - Can talk to a specific IP over network, but not any other
    - Can read files from a specific subdirectory tree (no write access whatsoever)
    - Can write to a specific pipe. Cannot write to any files.
- Why would a kernel need to be *either* a micro-kernel, *or* a monolithic kernel? Surely we can support both approaches! We should be able to use both kernel-space *and* user-space drivers!
- Kernel can check program signatures before it runs them (depending on what rules the init proces set up for this)
- Programs can store their specifications and proofs of correctness, and the kernel can check all this before it starts running the program.

# Actual stuff that would be useful to do next
- Hard Drive drivers
- Network card drivers
- GPU drivers (at least be able to display a frame buffer)
- Virtual filesystem? (or some other wierd way of writing data to disk. maybe a kernel-managed database? well... thats basically what a filesystem already is, so...)
- Basic Process structure on the kernel side, so that we can hold some state about them
- Actual usefull syscalls (read a file)
- ACPI stack?
- USB stack?
- TCP/IP (or even just UDP) stack?
- Find an easy way of writing user-space programs that the OS can execute (i.e. do not have just a hardcoded init program in the kernel)
- Write a terminal user program that can launch other programs
- Make a GUI for the OS (can be a traditional GUI, but does not need to be...)
