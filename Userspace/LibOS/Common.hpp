#pragma once
#include <stdint.h>
#include <stddef.h>

using i8  = int8_t;
using i16 = int16_t;
using i32 = int32_t;
using i64 = int64_t;

using u8  = uint8_t;
using u16 = uint16_t;
using u32 = uint32_t;
using u64 = uint64_t;

using usize = size_t;
// TODO: define `isize`

namespace OS {

namespace RawSyscall {

inline u64 Arg0(u64 sysnum) {
    u64 rax asm("rax");
    asm("syscall": "=a"(rax) : "D"(sysnum) : "rsi", "rdx", "rcx", "r8", "r9", "r10", "r11");
    return rax;
}

inline u64 Arg0_volatile(u64 sysnum) {
    u64 rax asm("rax");
    asm volatile("syscall": "=a"(rax) : "D"(sysnum) : "rsi", "rdx", "rcx", "r8", "r9", "r10", "r11");
    return rax;
}

inline u64 Arg1(u64 sysnum, u64 arg0) {
    u64 rax asm("rax");
    asm("syscall": "=a"(rax) : "D"(sysnum), "S"(arg0) : "rdx", "rcx", "r8", "r9", "r10", "r11");
    return rax;
}

inline u64 Arg1_volatile(u64 sysnum, u64 arg0) {
    u64 rax asm("rax");
    asm volatile("syscall": "=a"(rax) : "D"(sysnum), "S"(arg0) : "rdx", "rcx", "r8", "r9", "r10", "r11");
    return rax;
}

inline u64 Arg2(u64 sysnum, u64 arg0, u64 arg1) {
    u64 rax asm("rax");
    asm("syscall": "=a"(rax) : "D"(sysnum), "S"(arg0), "d"(arg1) : "rcx", "r8", "r9", "r10", "r11");
    return rax;
}

inline u64 Arg2_volatile(u64 sysnum, u64 arg0, u64 arg1) {
    u64 rax asm("rax");
    asm volatile("syscall": "=a"(rax) : "D"(sysnum), "S"(arg0), "d"(arg1) : "rcx", "r8", "r9", "r10", "r11");
    return rax;
}

} // namespace RawSyscall

namespace Syscall {

// Name: Test syscall
// Description: Makes the kernel output basic debug info into the debug console
// Syscall number: 0
// Return value: N/A
inline void Test() { RawSyscall::Arg0_volatile(0); }

inline u64 GetTimerValue() { return RawSyscall::Arg0_volatile(1); }
inline void DebugPrint(const char* data, usize size) {
    RawSyscall::Arg2_volatile(2, reinterpret_cast<u64>(data), size);
}

} // namespace Syscall

} // namespace OS
