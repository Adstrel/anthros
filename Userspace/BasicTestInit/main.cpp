#include "LibOS/Common.hpp"

//const char hello_msg[] = "Hello, World!\n";

extern "C" __attribute__((noreturn)) void __start() {
    //OS::Syscall::DebugPrint(hello_msg, sizeof(hello_msg) - 1);

    u64 timer_val = OS::Syscall::GetTimerValue();

    while (true) {
        // Invoke the Test Syscall
        OS::Syscall::Test();

        const u64 new_timer_goal = timer_val + 5000 - (timer_val % 5000);
        do {
            timer_val = OS::Syscall::GetTimerValue();
        } while (timer_val < new_timer_goal);
    }
    __builtin_unreachable();
}
