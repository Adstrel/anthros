# System Memory Map
The usable 48-bit virtual address space is divided into two halfs. The lower half is owned by the user program of the currently running process, and managed by the kernel's process subsystem. The higher half is owned by the kernel, and is further subdivided into smaller parts, each owned and managed by different kernel subsystems.

## Virtual Memory Map
| Address Range Start         | Address Range End           | Owner                       |
| --------------------------- | --------------------------- | --------------------------- |
| ```0x0000'0000'0000'0000``` | ```0x0000'7fff'ffff'ffff``` | Userspace Memory            |
| ```0xffff'8000'0000'0000``` | ```0xffff'ffff'ffff'ffff``` | Kernelspace Memory          |
|                             |                             |                             |
| ```0xffff'8000'0000'0000``` | ```0xffff'8000'ffff'ffff``` | Page Allocator's bitbuffer  |
| ```0xffff'8001'0000'1000``` | ```0xffff'8001'0000'1fff``` | Local APIC Register space   |
| ```0xffff'8010'0000'0000``` | ```0xffff'801f'ffff'ffff``` | VM Device Map Area          |
| ```0xffff'ffff'7fff'8000``` | ```0xffff'ffff'7fff'8fff``` | Kernel Stack                |
| ```0xffff'ffff'8000'0000``` | ```0xffff'ffff'ffff'ffff``` | Kernel Code and Static Data |

## Owner Description
- ### Userspace Memory
    - This memory range is free to be used by all programs.
    - Whenever the kernel is accessing userspace memory, it needs to be careful to know that the memory is valid and mapped in.
    - User programs can do whatever they want with the memory they have access to. Whenever they want to have access to more memory, they need to ask the kernel using a syscall to provide it.
- ### Kernelspace Memory
    - This memory range is used by the kernel. It is further subdivided into smaller memory ranges, which are owned by different kernel subsystems.
- ### Page Allocator's bitbuffer
    - Page Allocator is a kernel subsystem that manages the system's physical memory. The virtual address range it owns is big enough to potentially manage up to 128 TiB of physical memory.
- ### VM Device Map Area
    - This memory range is used to map PCI devices into it.
- ### Kernel Stack
    - The current process' kernel stack is only one page (4096 bytes) big. After it, and before it, there is an unmapped page, which guards against over- and underflows.
- ### Kernel Code and Static Data
    - This is the memory range to which the PreKernel / Bootloader maps the kernel image.
    - Advantage of having code and static resources in the last 2 GiB of address space is that the instructions can directly reference them with a 32-bit address operand (which just has the highest bit set to 1), instead of loading a 64-bit value into a register and then referencing the memory pointed to by the register.
