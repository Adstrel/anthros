# Kernel Subsystems
This file describes the different kernel subsystem. Each subsystem is a part of the kernel that manages some specific type of resource.

## PageAllocator
Manages the physical memory of the system, its allocation, and deallocation. It also owns a predetermined virtual address range, which is big enough to potentially manage the up to 128 TiB of physical memory.

## Schedueler
