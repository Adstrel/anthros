BASE_DIR := $(shell realpath .)
BUILD_DIR := $(BASE_DIR)/Build

.PHONY: all clean $(BUILD_DIR)/PreKernel/prekernel.efi $(BUILD_DIR)/Kernel/kernel.exe

all: pxe_root/prekernel.efi pxe_root/kernel.exe

pxe_root/prekernel.efi: $(BUILD_DIR)/PreKernel/prekernel.efi | pxe_root/
	cp $< $@

pxe_root/kernel.exe: $(BUILD_DIR)/Kernel/kernel.exe | pxe_root/
	cp $< $@

$(BUILD_DIR)/PreKernel/prekernel.efi:
	$(MAKE) -C PreKernel $@

$(BUILD_DIR)/Kernel/kernel.exe:
	$(MAKE) -C Kernel $@

clean:
	rm -rf $(BUILD_DIR) pxe_root

%/:
	mkdir -p $@
