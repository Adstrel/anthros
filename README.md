# AnthrOS
Just Another Operating System project among the many others out there.

Currently just builds a GNU cross-compilation toolchain, qemu system emulator, OVMF (UEFI drivers for qemu), and then boots as an EFI application.

For help run: `./scripts/MainScript.sh help`

# Needed packages (on an Ubuntu system)
gcc g++ make texinfo pkg-config libglib2.0-dev libpixman-1-dev libslirp-dev libsdl2-dev ninja-build

